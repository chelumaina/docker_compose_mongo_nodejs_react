import React, { useState, useEffect } from "react";
import "./filter.scss";
// import { useContext } from "react";
import {getCounties, getPositions, getCountyContituencies, getConstituencyWards, getWardPollingStations } from "../../../../src/http_util/util";
const Filter = () => {

  // const dispatch = useDispatch();
	// const widgets = useSelector(selectWidgets);
	// const projects = useSelector(selectProjects);
 

  const [position, setPosition] = useState("Select Position");
  const [positions, setPositions] = useState([]);

  const [county, setCounty] = useState("All Counties");
  const [counties, setCounties] = useState([]);

  const [constituency, setConstituency] = useState("All Constituencies");
  const [constituencies, setConstituencies] = useState([]);

  const [ward, setWard] = useState("All Wards");
  const [wards, setWards] = useState([]);

  const [station, setStation] = useState("All Polling Stations");
  const [stations, setStations] = useState([]);

  useEffect(() => {
    const getCountiesData = async () => {
      const c=await getCounties();
      setCounties(c);
    };
    getCountiesData();
  }, []);

  useEffect(() => {
    async function getPosition() {
      const c=await getPositions();
      setPositions(c);
    }
    getPosition();
  }, []);


  const onPositionChange = async (e) => {
    const positionCode = e.target.value;
    setPosition(positionCode); 
  };


  const onCountyChange = async (e) => {
    const Id = e.target.value;
    setCounty(Id);
    const c =await getCountyContituencies(Id);
    setConstituencies(c);
  };

  const onConstituencyChange = async (e) => {
    const Id = e.target.value;
    setConstituency(Id);
    const c =await getConstituencyWards(Id);
    setWards(c);
  };

  const onWardChange = async (e) => {
    const Id = e.target.value;
    setWard(Id);
    const c =await getWardPollingStations(Id);
    setStations(c);
  };

  const onStationChange = async (e) => {
    const Id = e.target.value;
    setStation(Id);

  };

 

  return (
    <div className="filters-filter">
      <hr />
      <div className="wrapper-filter">
        <div className="filter">
          <div className="form-select">
            <select onChange={onPositionChange} name="position" id="position">
              <option defaultValue="" value="">Select Position</option>
              {positions &&
                positions.map((p) => (
                  <option key={p.id} value={p.id}>
                    {p.name}
                  </option>
                ))}
            </select>
          </div>

          <div className="form-select">
          <select onChange={onCountyChange} name="county" id="county">
              <option defaultValue="" value="">Select County</option>
              {counties &&
                counties.map((p) => (
                  <option key={p.id} value={p.id}>
                    {p.name}
                  </option>
                ))}
            </select>
          </div>

          <div className="form-select">
            <select onChange={onConstituencyChange} name="constituency" id="constituency">
              <option defaultValue="" value="">Select Constituency</option>
              {constituencies &&
                constituencies.map((p) => (
                  <option key={p.id} value={p.id}>
                    {p.name}
                  </option>
                ))}
            </select>
          </div>

          <div className="form-select">
            <select onChange={onWardChange} name="ward" id="ward">
              <option defaultValue="" value="">Select Ward</option>
              {wards &&
                wards.map((p) => (
                  <option key={p.id} value={p.id}>
                    {p.name}
                  </option>
                ))}
            </select>
          </div>

          <div className="form-select">
            <select onChange={onStationChange} name="station" id="station">
              <option defaultValue="" value="">Poling Station</option>
              {stations &&
                stations.map((p) => (
                  <option key={p.id} value={p.id}>
                    {p.name}
                  </option>
                ))}
            </select>
          </div>
        </div>
      </div>
      <hr />
    </div>
  );
};

export default Filter;
