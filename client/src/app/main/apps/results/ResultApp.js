import FusePageCarded from '@fuse/core/FusePageCarded';
import withReducer from 'app/store/withReducer';
import React, { useEffect, useRef } from 'react';
import { useDispatch } from 'react-redux';
import { useParams } from 'react-router-dom';
import { useDeepCompareEffect } from '@fuse/hooks';
import reducer from './store';
import { getLabels } from './store/labelsSlice';
import { getFilters } from './store/filtersSlice';
import { getFolders } from './store/foldersSlice';
import { getResults } from './store/resultsSlice';
import ResultDialog from './ResultDialog';
import ResultHeader from './ResultHeader';
import ResultList from './ResultList';
import ResultSidebarContent from './ResultSidebarContent';
import ResultSidebarHeader from './ResultSidebarHeader';
import ResultToolbar from './ResultToolbar';

function ResultApp(props) {
	const dispatch = useDispatch();

	const pageLayout = useRef(null);
	const routeParams = useParams();

	useEffect(() => {
		dispatch(getFilters());
		dispatch(getFolders());
		dispatch(getLabels());
	}, [dispatch]);

	useDeepCompareEffect(() => {
		dispatch(getResults(routeParams));
	}, [dispatch, routeParams]);

	return (
		<>
			<FusePageCarded
				classes={{
					root: 'w-full',
					header: 'items-center min-h-72 h-72 sm:h-136 sm:min-h-136'
				}}
				header={<ResultHeader pageLayout={pageLayout} />}
				contentToolbar={<ResultToolbar />}
				content={<ResultList />}
				leftSidebarHeader={<ResultSidebarHeader />}
				leftSidebarContent={<ResultSidebarContent />}
				ref={pageLayout}
				innerScroll
			/>
			<ResultDialog />
		</>
	);
}

export default withReducer('resultApp', reducer)(ResultApp);
