import FuseAnimate from '@fuse/core/FuseAnimate';
import FuseAnimateGroup from '@fuse/core/FuseAnimateGroup';
import FuseUtils from '@fuse/utils';
import _ from '@lodash';
import List from '@material-ui/core/List';
import Typography from '@material-ui/core/Typography';
import React, { useEffect, useState } from 'react';
import { useSelector } from 'react-redux';
import { selectResults } from './store/resultsSlice';
import ResultListItem from './ResultListItem';

function ResultList(props) {
	const result = useSelector(selectResults);
	const searchText = useSelector(({ resultApp }) => resultApp.results.searchText);
	const orderBy = useSelector(({ resultApp }) => resultApp.results.orderBy);
	const orderDescending = useSelector(({ resultApp }) => resultApp.results.orderDescending);
	const [filteredData, setFilteredData] = useState(null);

	useEffect(() => {
		function getFilteredArray(entities, _searchText) {
			if (_searchText.length === 0) {
				return result;
			}
			return FuseUtils.filterArrayByString(result, _searchText);
		}

		if (result) {
			setFilteredData(
				_.orderBy(getFilteredArray(result, searchText), [orderBy], [orderDescending ? 'desc' : 'asc'])
			);
		}
	}, [result, searchText, orderBy, orderDescending]);

	if (!filteredData) {
		return null;
	}

	if (filteredData.length === 0) {
		return (
			<FuseAnimate delay={100}>
				<div className="flex flex-1 items-center justify-center h-full">
					<Typography color="textSecondary" variant="h5">
						There are no result!
					</Typography>
				</div>
			</FuseAnimate>
		);
	}

	return (
		<List className="p-0">
			<FuseAnimateGroup
				enter={{
					animation: 'transition.slideUpBigIn'
				}}
			>
				{filteredData.map(result => (
					<ResultListItem result={result} key={result.id} />
				))}
			</FuseAnimateGroup>
		</List>
	);
}

export default ResultList;
