import { useForm } from "@fuse/hooks";

import FuseUtils from "@fuse/utils";
import _ from "@lodash";
import AppBar from "@material-ui/core/AppBar";
import Avatar from "@material-ui/core/Avatar";
import Button from "@material-ui/core/Button";
import Checkbox from "@material-ui/core/Checkbox";
import Chip from "@material-ui/core/Chip";
import { amber, red } from "@material-ui/core/colors";
import Dialog from "@material-ui/core/Dialog";
import DialogActions from "@material-ui/core/DialogActions";
import DialogContent from "@material-ui/core/DialogContent";
import Divider from "@material-ui/core/Divider";
import FormControl from "@material-ui/core/FormControl";
import Icon from "@material-ui/core/Icon";
import IconButton from "@material-ui/core/IconButton";
import ListItemIcon from "@material-ui/core/ListItemIcon";
import ListItemText from "@material-ui/core/ListItemText";
import Menu from "@material-ui/core/Menu";
import MenuItem from "@material-ui/core/MenuItem";
import TextField from "@material-ui/core/TextField";
import Select from "@material-ui/core/Select";

import InputLabel from "@material-ui/core/InputLabel";
import OutlinedInput from "@material-ui/core/OutlinedInput";

import Toolbar from "@material-ui/core/Toolbar";
import Typography from "@material-ui/core/Typography";
import moment from "moment/moment";
import React, { useCallback, useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { selectLabels } from "./store/labelsSlice";
import {
  removeResult,
  addResult,
  closeNewResultDialog,
  closeEditResultDialog,
  updateResult,
} from "./store/resultsSlice";

import { makeStyles, useTheme } from "@material-ui/core/styles";
import clsx from "clsx";
import { orange } from "@material-ui/core/colors";
import { Alert } from "@material-ui/lab";

const defaultFormState = {
  id: "",
  vote_cast: "",
  spoilt_vote: "",
  notes: "",
  file: null,
  results: [],
  pollingStation: "",
  position: "",
};

const useStyles = makeStyles((theme) => ({
  productImageFeaturedStar: {
    position: "absolute",
    top: 0,
    right: 0,
    color: orange[400],
    opacity: 0,
  },
  productImageUpload: {
    transitionProperty: "box-shadow",
    transitionDuration: theme.transitions.duration.short,
    transitionTimingFunction: theme.transitions.easing.easeInOut,
  },
  productImageItem: {
    transitionProperty: "box-shadow",
    transitionDuration: theme.transitions.duration.short,
    transitionTimingFunction: theme.transitions.easing.easeInOut,
    "&:hover": {
      "& $productImageFeaturedStar": {
        opacity: 0.8,
      },
    },
    "&.featured": {
      pointerEvents: "none",
      boxShadow: theme.shadows[3],
      "& $productImageFeaturedStar": {
        opacity: 1,
      },
      "&:hover $productImageFeaturedStar": {
        opacity: 1,
      },
    },
  },
}));

const candidate = [
  {
    candidate_id: "1",
    candidate_name: "Willium Ruto",
    name: "candidate1",
    votes: "",
  },
  {
    candidate_id: "2",
    candidate_name: "Raila Odinga",
    name: "candidate1",
    votes: "",
  },
  {
    candidate_id: "3",
    candidate_name: "Wajakhoya Wajakhoya",
    name: "candidate1",
    votes: "",
  },
  {
    candidate_id: "4",
    candidate_name: "Wahiga Mwaure",
    name: "candidate1",
    votes: "",
  },
];

const pos = [
  { _id: "62972264f068dd489e054550", name: "President", __v: 0 },
  { _id: "62972264f068dd489e054552", name: "Governor", __v: 0 },
  { _id: "62972264f068dd489e054554", name: "Senator", __v: 0 },
  { _id: "62972264f068dd489e054556", name: "Woman Rep", __v: 0 },
  {
    _id: "62972264f068dd489e054558",
    name: "Member of Nationa Assembly",
    __v: 0,
  },
  { _id: "62972264f068dd489e05455a", name: "Member County Assembly", __v: 0 },
];
function ResultDialog(props) {
  const dispatch = useDispatch();
  const resultDialog = useSelector(
    ({ resultApp }) => resultApp.results.resultDialog
  );
  const labels = useSelector(selectLabels);

  const [selectedPollingStation, setSelectedPollingStation] = useState("");
  const [selectedPosition, setSelectedPosition] = useState("");

  const [pollingStation, setPollingStation] = useState([]);
  const [position, setPosition] = useState([]);

  const [voteCast, setVoteCast] = useState("");
  const [spoiltVote, setSpoiltVote] = useState("");
  const [notes, setNotes] = useState("");
  const [image, setImage] = useState({});

  // setPosition(pos);
  const classes = useStyles(props);

  const [candidates, setCandidates] = useState(candidate);

  const { form, setForm } = useForm({ ...defaultFormState });

  const initDialog = useCallback(() => {
    /**
     * Dialog type: 'edit'
     */
    if (resultDialog.type === "edit" && resultDialog.data) {
      setForm({ ...resultDialog.data });
    }

    /**
     * Dialog type: 'new'
     */
    if (resultDialog.type === "new") {
      setForm({
        ...defaultFormState,
        ...resultDialog.data,
        id: FuseUtils.generateGUID(),
      });
    }
  }, [resultDialog.data, resultDialog.type, setForm]);

  useEffect(() => {
    /**
     * After Dialog Open
     */
    if (resultDialog.props.open) {
      initDialog();
    }
    setPosition(pos);
  }, [resultDialog.props.open, initDialog, pos]);

  function closeResultDialog() {
    return resultDialog.type === "edit"
      ? dispatch(closeEditResultDialog())
      : dispatch(closeNewResultDialog());
  }

  function canBeSubmitted() {
    console.log(
      voteCast.length > 0 ,
      spoiltVote.length > 0 ,
      notes.length > 0 ,
      position.length > 0 , checkCandidateResult(candidates)
    );
    return (
      voteCast.length > 0 &&
      spoiltVote.length > 0 &&
      notes.length > 0 &&
      position.length > 0 && checkCandidateResult(candidates)
    );
    // return form.vote_cast.length > 0 && form.spoilt_vote.length > 0 ;
  }

  function handleChange(event) {
    console.log(event.target.name);
    switch (event.target.name) {
      case "vote_cast":
        setVoteCast(event.target.value);
        break;
      case "spoilt_vote":
        setSpoiltVote(event.target.value);
        break;
      case "notes":
        setNotes(event.target.value);
        break;
      default:
        break;
    }
    console.log("notes", notes, event.target.value);
    // setSelectedPollingStation(event.target.value);
  }
  function handleSelectedPollingStation(event) {
    setSelectedPollingStation(event.target.value);
  }

  function handleSelectedPosition(event) {
    setSelectedPosition(event.target.value);
  }

  function handleUploadChange(e) {
    const file = e.target.files[0];
    if (!file) {
      return;
    }
    const reader = new FileReader();
    reader.readAsBinaryString(file);

    reader.onload = () => {
      setImage({
        id: FuseUtils.generateGUID(),
        url: `data:${file.type};base64,${btoa(reader.result)}`,
        type: "image",
      });
      console.log(image);
      // setForm(
      //   _.set({ ...form }, `images`, [
      //     {
      //       id: FuseUtils.generateGUID(),
      //       url: `data:${file.type};base64,${btoa(reader.result)}`,
      //       type: "image",
      //     },
      //     ...form.images,
      //   ])
      // );
    };

    reader.onerror = () => {
      console.log("error on load image");
    };
  }

  const validate = candidates.reduce(function (previousValue, currentValue) {
    return previousValue + currentValue.votes;
  }, 0);

  const checkCandidateResult = (c) => {
    const invalid = c.filter((person) => parseInt(person.votes) <= 0);

    const sum_for_each = candidates.reduce(function (previousValue,currentValue) {return parseInt(previousValue) + parseInt(currentValue.votes);},0);

    console.log("fullAge", invalid.length, sum_for_each, parseInt(voteCast));
    return invalid.length == 0 || sum_for_each === parseInt(voteCast);
  };

  function handleChangeCandidateVotes(i, event) {
    let candidate_votes = [...candidates];
    candidate_votes[i]["votes"] = event.target.value;
    setCandidates(candidate_votes);
    checkCandidateResult(candidates);
  }
  return (
    <Dialog
      {...resultDialog.props}
      onClose={closeResultDialog}
      fullWidth
      maxWidth="sm"
      classes={{
        paper: "rounded-8",
      }}
    >
      <AppBar position="static" className="shadow-md">
        <Toolbar className="flex w-full">
          <Typography variant="subtitle1" color="inherit">
            {resultDialog.type === "new" ? "New Result" : "Edit Result"}
          </Typography>
        </Toolbar>
      </AppBar>

      <DialogContent classes={{ root: "p-0" }}>
        <div className="px-16 sm:px-24">
          <FormControl className="mt-8 mb-16" required fullWidth>
            <InputLabel htmlFor="pollingStation-label-placeholder">
              {" "}
              Polling Station{" "}
            </InputLabel>
            <Select
              value={selectedPosition}
              onChange={handleSelectedPollingStation}
              input={
                <OutlinedInput
                  labelWidth={"pollingStation".length * 9}
                  name="pollingStation"
                  id="pollingStation-label-placeholder"
                />
              }
            >
              <MenuItem value="all">
                <em> Select Polling Station </em>
              </MenuItem>
              {pollingStation &&
                pollingStation.map((p) => (
                  <MenuItem value={p.value} key={p.id}>
                    {p.label}
                  </MenuItem>
                ))}
            </Select>
          </FormControl>

          <div className="flex -mx-4">
            <FormControl className="mt-8 mb-16 mx-4" required fullWidth>
              <InputLabel htmlFor="position-label-placeholder">
                {"      "} Select Position {"   "}
              </InputLabel>
              <Select
                value={selectedPosition}
                onChange={handleSelectedPosition}
                input={
                  <OutlinedInput
                    labelWidth={"position".length * 9}
                    name="position"
                    id="position-label-placeholder"
                  />
                }
              >
                <MenuItem className="mt-8 mb-16 mx-4" value="all">
                  <em> Select Elective Post </em>
                </MenuItem>
                {position &&
                  position.map((p) => (
                    <MenuItem value={p._id} key={p._id}>
                      {p.name}
                    </MenuItem>
                  ))}
              </Select>
            </FormControl>
            <FormControl className="mt-8 mb-16 mx-4" required fullWidth>
              <TextField
                label="Vote Case"
                autoFocus
                name="vote_cast"
                placeholder="eg. 0"
                value={voteCast}
                onChange={handleChange}
                required
                variant="outlined"
                fullWidth
              />
            </FormControl>

            <FormControl className="mt-8 mb-16 mx-4" required fullWidth>
              <TextField
                label="Spoilt Votes"
                autoFocus
                name="spoilt_vote"
                placeholder="eg. 0"
                value={spoiltVote}
                onChange={handleChange}
                required
                variant="outlined"
                fullWidth
              />
            </FormControl>
          </div>
          <Divider className="mx-24" />

          <hr />
          <div className="grid grid-cols-3 gap-3">
            {candidates.map((input, index) => {
              console.log("input", input.candidate_name);
              return (
                <FormControl
                  key={index}
                  className="mt-8 mb-16 mx-4"
                  required
                  fullWidth
                >
                  <TextField
                    label={input.candidate_name}
                    autoFocus
                    type="number"
                    name={input.name}
                    value={input.votes || ""}
                    onChange={handleChangeCandidateVotes.bind(this, index)}
                    required
                    variant="outlined"
                    fullWidth
                  />
                </FormControl>
              );
            })}
          </div>
          <div className="px-16 sm:px-24 mt-5 mb-5">
              <Alert severity="error">
                Sum of Votes for each candidate must be equal to the Valid Votes Cast
              </Alert>
          </div>
          <hr />

          <FormControl className="mt-8 mb-16" required fullWidth>
            <TextField
              autoFocus
              required
              fullWidth
              label="Notes"
              name="notes"
              multiline
              rows="3"
              value={notes}
              onChange={handleChange}
              variant="outlined"
            />
          </FormControl>

          <div>
            <div className="flex justify-center sm:justify-start flex-wrap -mx-8">
              <label
                htmlFor="button-file"
                className={clsx(
                  classes.productImageUpload,
                  "flex items-center justify-center relative w-128 h-128 rounded-8 mx-8 mb-16 overflow-hidden cursor-pointer shadow hover:shadow-lg"
                )}
              >
                <input
                  accept="image/*"
                  className="hidden"
                  id="button-file"
                  type="file"
                  onChange={handleUploadChange}
                />
                <Icon fontSize="large" color="action">
                  cloud_upload
                </Icon>
              </label>
              {image && (
                <div
                  role="button"
                  tabIndex={0}
                  className={clsx(
                    classes.productImageUpload,
                    "flex items-center justify-center relative w-128 h-128 rounded-8 mx-8 mb-16 overflow-hidden cursor-pointer outline-none shadow hover:shadow-lg",
                    "featured"
                  )}
                  key={image.id}
                >
                  <img className="max-w-none w-auto h-full" src={image.url} />
                </div>
              )}
            </div>
            
          </div>
 
        </div>
      </DialogContent>

      {resultDialog.type === "new" ? (
        <DialogActions className="justify-between p-8">
          <div className="px-16">
            <Button
              variant="contained"
              color="primary"
              // onClick={() => {
              //   dispatch(addResult(form)).then(() => {
              //     closeResultDialog();
              //   });
              // }}
              disabled={!canBeSubmitted()}
            >
              Confirm and Send Results
            </Button>
          </div>
        </DialogActions>
      ) : (
        <DialogActions className="justify-between p-8">
          <div className="px-16">
            <Button
              variant="contained"
              color="primary"
              // onClick={() => {
              //   dispatch(updateResult(form)).then(() => {
              //     closeResultDialog();
              //   });
              // }}
              disabled={!canBeSubmitted()}
            >
              Confirm and Send Results
            </Button>
          </div>
          <IconButton
            className="min-w-auto"
            onClick={() => {
              dispatch(removeResult(form.id)).then(() => {
                closeResultDialog();
              });
            }}
          >
            <Icon>delete</Icon>
          </IconButton>
        </DialogActions>
      )}
    </Dialog>
  );
}

export default ResultDialog;
