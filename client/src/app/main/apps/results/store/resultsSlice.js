import { createSlice, createAsyncThunk, createEntityAdapter } from '@reduxjs/toolkit';
import axios from 'axios';
import {process_results } from '../../../../../http_util/util';
 

export const getResults2 = createAsyncThunk('resultApp/filters/getFilters', async () => {
	const response = await process_results();
	return response;
});


export const getResults = createAsyncThunk('resultApp/results/getResults', async (routeParams, { getState }) => {
	routeParams = routeParams || getState().resultApp.results.routeParams;
	// const response = await axios.get('/api/todo-app/todos', {
	// 	params: routeParams
	// });
	const data = await process_results();
	// const data = await response.data;
	return { data, routeParams };
});
 
 

export const addResult = createAsyncThunk('resultApp/results/addResult', async (results, { dispatch, getState }) => {
	// const response = await axios.post('/api/results-app/new-results', results);
	const data =[];// await response.data;

	dispatch(getResults());

	return data;
});

export const updateResult = createAsyncThunk('resultApp/results/updateResult', async (results, { dispatch, getState }) => {
	// const response = await axios.post('/api/results-app/update-results', results);
	const data = [];//await response.data;

	dispatch(getResults());

	return data;
});

export const removeResult = createAsyncThunk('resultApp/results/removeResult', async (resultId, { dispatch, getState }) => {
	// const response = await axios.post('/api/results-app/remove-results', resultId);
	const data = [];//await response.data;

	dispatch(getResults());

	return data;
});

const resultsAdapter = createEntityAdapter({});

export const { selectAll: selectResults, selectById: selectResultsById } = resultsAdapter.getSelectors(
	state => state.resultApp.results
);

const resultsSlice = createSlice({
	name: 'resultApp/results',
	initialState: resultsAdapter.getInitialState({
		searchText: '',
		orderBy: '',
		orderDescending: false,
		routeParams: {},
		resultDialog: {
			type: 'new',
			props: {
				open: false
			},
			data: null
		}
	}),
	reducers: {
		setResultsSearchText: {
			reducer: (state, action) => {
				state.searchText = action.payload;
			},
			prepare: event => ({ payload: event.target.value || '' })
		},
		toggleOrderDescending: (state, action) => {
			state.orderDescending = !state.orderDescending;
		},
		changeOrder: (state, action) => {
			state.orderBy = action.payload;
		},
		openNewResultDialog: (state, action) => {
			console.log("openNewResultDialog", action.payload);

			state.resultDialog = {
				type: 'new',
				props: {
					open: true
				},
				data: null
			};
		},
		closeNewResultDialog: (state, action) => {
			state.resultDialog = {
				type: 'new',
				props: {
					open: false
				},
				data: null
			};
		},
		openEditResultDialog: (state, action) => {
			state.resultDialog = {
				type: 'edit',
				props: {
					open: true
				},
				data: action.payload
			};
		},
		closeEditResultDialog: (state, action) => {
			state.resultDialog = {
				type: 'edit',
				props: {
					open: false
				},
				data: null
			};
		}
	},
	extraReducers: {
		[updateResult.fulfilled]: resultsAdapter.upsertOne,
		[addResult.fulfilled]: resultsAdapter.addOne,
		[getResults.fulfilled]: (state, action) => {
			const { data, routeParams } = action.payload;
			resultsAdapter.setAll(state, data);
			state.routeParams = routeParams;
			state.searchText = '';
		}
	}
});

export const {
	setResultsSearchText,
	toggleOrderDescending,
	changeOrder,
	openNewResultDialog,
	closeNewResultDialog,
	openEditResultDialog,
	closeEditResultDialog
} = resultsSlice.actions;

export default resultsSlice.reducer;
