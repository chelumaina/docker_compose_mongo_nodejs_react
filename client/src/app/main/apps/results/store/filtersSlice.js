import { createSlice, createAsyncThunk, createEntityAdapter } from '@reduxjs/toolkit';
import axios from 'axios';
import {getPositions } from '../../../../../http_util/util';
 

export const getFilters = createAsyncThunk('resultApp/filters/getFilters', async () => {
	const response = await getPositions();
	console.log(response)
	return response;
	console.log(response)

});
 

const filtersAdapter = createEntityAdapter({});

export const { selectAll: selectFilters, selectById: selectFilterById } = filtersAdapter.getSelectors(
	state => state.resultApp.filters
);

const filtersSlice = createSlice({
	name: 'resultApp/filters',
	initialState: filtersAdapter.getInitialState({}),
	reducers: {},
	extraReducers: {
		[getFilters.fulfilled]: filtersAdapter.setAll
	}
});

export default filtersSlice.reducer;
