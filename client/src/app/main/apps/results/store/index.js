import { combineReducers } from '@reduxjs/toolkit';
import filters from './filtersSlice';
import folders from './foldersSlice';
import labels from './labelsSlice';
import results from './resultsSlice';

const reducer = combineReducers({
	results,
	folders,
	labels,
	filters
});

export default reducer;
