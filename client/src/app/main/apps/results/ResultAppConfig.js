import React from 'react';
import { Redirect } from 'react-router-dom';

const ResultAppConfig = {
	settings: {
		layout: {}
	},
	routes: [
		{
			path: [
				'/apps/results/label/:labelHandle/:resultsId?',
				'/apps/results/filter/:filterHandle/:resultsId?',
				'/apps/results/:folderHandle/:resultsId?'
			],
			component: React.lazy(() => import('./ResultApp'))
		},
		{
			path: '/apps/results',
			component: () => <Redirect to="/apps/results/all" />
		}
	]
};

export default ResultAppConfig;
