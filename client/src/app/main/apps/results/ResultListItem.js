import _ from '@lodash';
import Checkbox from '@material-ui/core/Checkbox';
import { amber, red } from '@material-ui/core/colors';
import Icon from '@material-ui/core/Icon';
import IconButton from '@material-ui/core/IconButton';
import ListItem from '@material-ui/core/ListItem';
import { makeStyles } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import clsx from 'clsx';
import React from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { selectLabelsEntities } from './store/labelsSlice';
import { updateResult, openEditResultDialog } from './store/resultsSlice';

import ResultChip from './ResultChip';

const useStyles = makeStyles({
	resultItem: {
		'&.completed': {
			background: 'rgba(0,0,0,0.03)',
			'& .result-title, & .result-notes': {
				//textDecoration: 'line-through'
			}
		}
	}
});

function ResultListItem(props) {
	const dispatch = useDispatch();
	const labels = useSelector(selectLabelsEntities);

	const classes = useStyles(props);
	return (
		<ListItem
			className={clsx(
				classes.resultItem,
				{ completed: props.result.form_name },
				'border-solid border-b-1 py-16 px-0 sm:px-8'
			)}
			onClick={ev => {
				ev.preventDefault();
				dispatch(openEditResultDialog(props.result));
			}}
			dense
			button
		>
			<Checkbox
				tabIndex={-1}
				disableRipple
				checked
				onChange={() =>
					dispatch(
						updateResult({
							...props.result,
							completed: false
						})
					)
				}
				onClick={ev => ev.stopPropagation()}
			/>

			<div className="flex flex-1 flex-col relative px-8">
				<Typography
					variant="subtitle1"
					className="result-title"
					color={props.result.form_name ? 'textSecondary' : 'inherit'}
				>
					{props.result.form_name} - {props.result.position_id.name}
				</Typography>



				

				<div className={clsx(classes.labels, 'flex -mx-2')}>
					{props.result.results.map(label => (
						<ResultChip
							className="mx-2 mt-4"
							title={label.candidate_name+' - '+label.votes}
							color="color"
							key={label.candidate_uuid}
						/>
					))}
				</div>
			</div>

			<div className="flex flex-1 flex-col relative px-8">
			<Typography color="textSecondary6" className="result-notes">
					{props.result.polling_station_id.polling_station_code} - 
					{props.result.polling_station_id.polling_station_name}
				</Typography>
	 
			<Typography color="textSecondary5" className="result-notes">
					{props.result.position_id.name}
				</Typography>
			</div>

			<div className="px-8">
				<IconButton
					onClick={ev => {
						ev.preventDefault();
						ev.stopPropagation();
						dispatch(
							updateResult({
								...props.result,
								important: !props.result.form_name
							})
						);
					}}
				>
					{props.result.form_name ? <Icon style={{ color: red[500] }}>error</Icon> : <Icon>error_outline</Icon>}
				</IconButton>
				<IconButton
					onClick={ev => {
						ev.preventDefault();
						ev.stopPropagation();
						dispatch(
							updateResult({
								...props.result,
								starred: !props.result.form_name
							})
						);
					}}
				>
					{props.result.form_name ? <Icon style={{ color: amber[500] }}>star</Icon> : <Icon>star_outline</Icon>}
				</IconButton>
			</div>
		</ListItem>
	);
}

export default ResultListItem;
