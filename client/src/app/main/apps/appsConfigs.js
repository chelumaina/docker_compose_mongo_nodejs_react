import AcademyAppConfig from './academy/AcademyAppConfig';
import CalendarAppConfig from './calendar/CalendarAppConfig';
import ChatAppConfig from './chat/ChatAppConfig';
import ContactsAppConfig from './contacts/ContactsAppConfig';
import AgentsAppConfig from './agents/AgentsAppConfig';
import CandidatesAppConfig from './candidates/CandidatesAppConfig';


import AnalyticsDashboardAppConfig from './dashboards/analytics/AnalyticsDashboardAppConfig';
import ProjectDashboardAppConfig from './dashboards/project/ProjectDashboardAppConfig';
import ECommerceAppConfig from './e-commerce/ECommerceAppConfig';
import ElectiveAreaAppConfig from './elective-area/ElectiveAreaAppConfig';
import FileManagerAppConfig from './file-manager/FileManagerAppConfig';
import MailAppConfig from './mail/MailAppConfig';
import NotesAppConfig from './notes/NotesAppConfig';
import ScrumboardAppConfig from './scrumboard/ScrumboardAppConfig';
import TodoAppConfig from './todo/TodoAppConfig';
import ResultAppConfig from './results/ResultAppConfig';


const appsConfigs = [
	AnalyticsDashboardAppConfig,
	ProjectDashboardAppConfig,
	MailAppConfig,
	TodoAppConfig,
	ResultAppConfig,
	FileManagerAppConfig,
	ContactsAppConfig,
	AgentsAppConfig,
	CandidatesAppConfig,
	CalendarAppConfig,
	ChatAppConfig,
	ECommerceAppConfig,
	ElectiveAreaAppConfig,
	ScrumboardAppConfig,
	AcademyAppConfig,
	NotesAppConfig
];

export default appsConfigs;
