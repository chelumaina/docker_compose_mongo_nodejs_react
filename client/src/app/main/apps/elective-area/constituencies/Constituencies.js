import FusePageCarded from '@fuse/core/FusePageCarded';
import withReducer from 'app/store/withReducer';
import React from 'react';
import reducer from '../store';
import ConstituenciesHeader from './ConstituenciesHeader';
import ConstituenciesTable from './ConstituenciesTable';

function Constituencies() {
	return (
		<FusePageCarded
			classes={{
				content: 'flex',
				contentCard: 'overflow-hidden',
				header: 'min-h-72 h-72 sm:h-136 sm:min-h-136'
			}}
			header={<ConstituenciesHeader />}
			content={<ConstituenciesTable />}
			innerScroll
		/>
	);
}

export default withReducer('ElectiveAreaApp', reducer)(Constituencies);
