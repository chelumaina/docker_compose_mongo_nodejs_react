import FuseScrollbars from '@fuse/core/FuseScrollbars';
import _ from '@lodash';
import Checkbox from '@material-ui/core/Checkbox';
import Icon from '@material-ui/core/Icon';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TablePagination from '@material-ui/core/TablePagination';
import TableRow from '@material-ui/core/TableRow';
import Typography from '@material-ui/core/Typography';
import clsx from 'clsx';
import React, { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { withRouter } from 'react-router-dom';
import FuseLoading from '@fuse/core/FuseLoading';
import FuseAnimate from '@fuse/core/FuseAnimate/FuseAnimate';
import { getAllPollingStations, selectPollingStations } from '../store/pollingStationsSlice';

import PollingStationsTableHead from './PollingStationsTableHead';
import PollingStationStatus from './../pollingStation/PollingStationStatus';

function PollingStationsTable(props) {
	const dispatch = useDispatch();
	const pollingStations = useSelector(selectPollingStations);
	const searchText = useSelector(({ ElectiveAreaApp }) => ElectiveAreaApp.pollingStations.searchText);
 
	const [loading, setLoading] = useState(true);
	const [selected, setSelected] = useState([]);
	const [data, setData] = useState([]);
	// const [pollingStations, setPollingStations] = useState([]);
	const [page, setPage] = useState(0);
	const [wardId, setWardId] = useState(props.wardId);

	
	const [rowsPerPage, setRowsPerPage] = useState(10);
	const [PollingStation, setPollingStation] = useState({
		direction: 'asc',
		id: null
	});

	
	useEffect(() => {
		dispatch(getAllPollingStations(wardId)).then(() => setLoading(false));
	}, [wardId, dispatch]);

	useEffect(() => {
		if (searchText.length !== 0) {
			setData(_.filter(pollingStations, item => item.name.toLowerCase().includes(searchText.toLowerCase())));
			setPage(0);
		} else {
			setData(pollingStations);
		}
	}, [pollingStations, searchText]);

	function handleRequestSort(event, property) {
		const id = property;
		let direction = 'desc';

		if (pollingStations.id === property && pollingStations.direction === 'desc') {
			direction = 'asc';
		}

		setPollingStation({
			direction,
			id
		});
	}

	function handleSelectAllClick(event) {
		if (event.target.checked) {
			setSelected(data.map(n => n.id));
			return;
		}
		setSelected([]);
	}

	function handleDeselect() {
		setSelected([]);
	}

	function handleClick(item) {
		///apps/elective-area/pollingStation/stationResults/Station.js
		props.history.push(`/apps/elective-area/pollingStation/${item.id}`);
		// props.history.push(`/apps/elective-area/pollingStation/${item.id}/${item.name}`);
	}

	function handleCheck(event, id) {
		const selectedIndex = selected.indexOf(id);
		let newSelected = [];

		if (selectedIndex === -1) {
			newSelected = newSelected.concat(selected, id);
		} else if (selectedIndex === 0) {
			newSelected = newSelected.concat(selected.slice(1));
		} else if (selectedIndex === selected.length - 1) {
			newSelected = newSelected.concat(selected.slice(0, -1));
		} else if (selectedIndex > 0) {
			newSelected = newSelected.concat(selected.slice(0, selectedIndex), selected.slice(selectedIndex + 1));
		}

		setSelected(newSelected);
	}

	function handleChangePage(event, value) {
		setPage(value);
	}

	function handleChangeRowsPerPage(event) {
		setRowsPerPage(event.target.value);
	}

	if (loading) {
		return <FuseLoading />;
	}

	if (data.length === 0) {
		return (
			<FuseAnimate delay={100}>
				<div className="flex flex-1 items-center justify-center h-full">
					<Typography color="textSecondary" variant="h5">
						There are no polling Stations!
					</Typography>
				</div>
			</FuseAnimate>
		);
	} 

	return (
		<div className="w-full flex flex-col">
			<FuseScrollbars className="flex-grow overflow-x-auto">
			<Table stickyHeader className="min-w-xl" aria-labelledby="tableTitle">
					<PollingStationsTableHead
						selectedPollingStationIds={selected}
						order={pollingStations}
						onSelectAllClick={handleSelectAllClick}
						onRequestSort={handleRequestSort}
						rowCount={data.length}
						onMenuItemClick={handleDeselect}
					/>

					<TableBody>
						{_.orderBy(
							data,
							[
								o => {
									switch (pollingStations.id) {
										case 'id': {
											return parseInt(o.id, 10);
										}
										case 'customer': {
											return o.customer.firstName;
										}
										case 'payment': {
											return o.payment.method;
										}
										case 'status': {
											return o.status[0].name;
										}
										default: {
											return o[pollingStations.id];
										}
									}
								}
							],
							[pollingStations.direction]
						)
							.slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
							.map(n => {
								const isSelected = selected.indexOf(n.id) !== -1;
								return (
									<TableRow
										className="h-64 cursor-pointer"
										hover
										role="checkbox"
										aria-checked={isSelected}
										tabIndex={-1}
										key={n.id}
										selected={isSelected}
										onClick={event => handleClick(n)}
									>
										<TableCell className="w-40 md:w-64 text-center" padding="none">
											<Checkbox
												checked={isSelected}
												onClick={event => event.stopPropagation()}
												onChange={event => handleCheck(event, n.id)}
											/>
										</TableCell>

										<TableCell className="p-4 md:p-16" component="th" scope="row">
											{n.id}
										</TableCell>

										<TableCell className="p-4 md:p-16" component="th" scope="row">
											{n.polling_station_code}
										</TableCell>

										<TableCell className="p-4 md:p-16 truncate" component="th" scope="row">
											{`${n.name} ${n.name}`}
										</TableCell>

										<TableCell className="p-4 md:p-16" component="th" scope="row" align="right">
											<span>$</span>
											{n.polling_station_code}
										</TableCell>

										<TableCell className="p-4 md:p-16" component="th" scope="row">
											{n.polling_station_code}
										</TableCell>

										<TableCell className="p-4 md:p-16" component="th" scope="row">
											<PollingStationStatus name={n.polling_station_code} color="red" />
										</TableCell>

										<TableCell className="p-4 md:p-16" component="th" scope="row">
											{n.polling_station_code}
										</TableCell>
									</TableRow>
								);
							})}
					</TableBody>
				</Table>
			</FuseScrollbars>

			<TablePagination
				className="flex-shrink-0 border-t-1"
				component="div"
				count={data.length}
				rowsPerPage={rowsPerPage}
				page={page}
				backIconButtonProps={{
					'aria-label': 'Previous Page'
				}}
				nextIconButtonProps={{
					'aria-label': 'Next Page'
				}}
				onChangePage={handleChangePage}
				onChangeRowsPerPage={handleChangeRowsPerPage}
			/>
		</div>
	);
}

export default withRouter(PollingStationsTable);
