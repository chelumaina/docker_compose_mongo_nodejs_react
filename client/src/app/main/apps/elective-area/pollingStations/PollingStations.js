import FusePageCarded from '@fuse/core/FusePageCarded';
import withReducer from 'app/store/withReducer';
import React from 'react';
import reducer from '../store';
import PollingStationsHeader from './PollingStationsHeader';
import PollingStationsTable from './PollingStationsTable';

function PollingStations() {
	return (
		<FusePageCarded
			classes={{
				content: 'flex',
				contentCard: 'overflow-hidden',
				header: 'min-h-72 h-72 sm:h-136 sm:min-h-136'
			}}
			header={<PollingStationsHeader />}
			content={<PollingStationsTable />}
			innerScroll
		/>
	);
}

export default withReducer('ElectiveAreaApp', reducer)(PollingStations);
