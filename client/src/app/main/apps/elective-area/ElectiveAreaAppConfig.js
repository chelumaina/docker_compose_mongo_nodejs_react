import React from 'react';
import { Redirect } from 'react-router-dom';

const ElectiveAreaAppConfig = {
	settings: {
		layout: {}
	},
	routes: [
		{
			path: '/apps/elective-area/counties/:countyId/:countyHandle?',
			component: React.lazy(() => import('./county/County'))
		},
		{
			path: '/apps/elective-area/counties',
			component: React.lazy(() => import('./counties/Counties'))
		},
		{
			path: '/apps/elective-area/constituencies/:constituencyId/:constituencyHandle?',
			component: React.lazy(() => import('./constituency/Constituency'))
		},
		{
			path: '/apps/elective-area/constituencies',
			component: React.lazy(() => import('./constituencies/Constituencies'))
		},

		{
			path: '/apps/elective-area/wards/:wardId/:wardHandle?',
			component: React.lazy(() => import('./ward/Ward'))
		},
		{
			path: '/apps/elective-area/wards',
			component: React.lazy(() => import('./wards/Wards'))
		},
		{
			path: '/apps/elective-area/pollingStations/:pollingStationId/:pollingStationHandle?',
			component: React.lazy(() => import('./pollingStation/PollingStation'))
		},
		{
			path: '/apps/elective-area/pollingStation/:pollingStationId/:pollingStationHandle?',
			component: React.lazy(() => import('./pollingStation/stationResults/Station'))
		},
		{
			path: '/apps/elective-area/pollingStation/stationResults/Station/:pollingStationId/:pollingStationHandle?',
			component: React.lazy(() => import('./pollingStation/stationResults/Station'))
		},

		{
			path: '/apps/elective-area/pollingStations',
			component: React.lazy(() => import('./pollingStations/PollingStations'))
		},
		// {
		// 	path: '/apps/elective-area/orders/:orderId',
		// 	component: React.lazy(() => import('./order/Order'))
		// },
		// {
		// 	path: '/apps/elective-area/orders',
		// 	component: React.lazy(() => import('./orders/Orders'))
		// },
		{
			path: '/apps/elective-area',
			component: () => <Redirect to="/apps/elective-area/counties" />
		}
	]
};

export default ElectiveAreaAppConfig;
