import FuseAnimate from '@fuse/core/FuseAnimate';
// import FuseChipSelect from '@fuse/core/FuseChipSelect';
import FuseLoading from '@fuse/core/FuseLoading';
import FusePageCarded from '@fuse/core/FusePageCarded';
import { useForm, useDeepCompareEffect } from '@fuse/hooks';
import FuseUtils from '@fuse/utils';
import _ from '@lodash';
import Button from '@material-ui/core/Button';
import { orange } from '@material-ui/core/colors';
import Icon from '@material-ui/core/Icon';
// import InputAdornment from '@material-ui/core/InputAdornment';
import { makeStyles, useTheme } from '@material-ui/core/styles';
import Tab from '@material-ui/core/Tab';
import Tabs from '@material-ui/core/Tabs';
import TextField from '@material-ui/core/TextField';
import Typography from '@material-ui/core/Typography';
import withReducer from 'app/store/withReducer';
import clsx from 'clsx';
import React, { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { Link, useParams } from 'react-router-dom';
import { resetCounty, saveCounty, newCounty, getCounty } from '../store/countySlice';

import reducer from '../store';  
import ConstituenciesHeader from '../constituencies/ConstituenciesHeader';
import ConstituenciesTable from '../constituencies/ConstituenciesTable';

const useStyles = makeStyles(theme => ({
	countyImageFeaturedStar: {
		position: 'absolute',
		top: 0,
		right: 0,
		color: orange[400],
		opacity: 0
	},
	countyImageUpload: {
		transitionProperty: 'box-shadow',
		transitionDuration: theme.transitions.duration.short,
		transitionTimingFunction: theme.transitions.easing.easeInOut
	},
	countyImageItem: {
		transitionProperty: 'box-shadow',
		transitionDuration: theme.transitions.duration.short,
		transitionTimingFunction: theme.transitions.easing.easeInOut,
		'&:hover': {
			'& $countyImageFeaturedStar': {
				opacity: 0.8
			}
		},
		'&.featured': {
			pointerEvents: 'none',
			boxShadow: theme.shadows[3],
			'& $countyImageFeaturedStar': {
				opacity: 1
			},
			'&:hover $countyImageFeaturedStar': {
				opacity: 1
			}
		}
	}
}));

function County(props) {
	const dispatch = useDispatch();
	const county = useSelector(({ ElectiveAreaApp }) =>{return ElectiveAreaApp.county});
	const theme = useTheme();

	const classes = useStyles(props);
	const [tabValue, setTabValue] = useState(0);
	const [noCounty, setNoCounty] = useState(false);
	const [ountyImages, setCountyImages] = useState([]);


	
	const { form, handleChange, setForm, setInForm } = useForm(null);
	const routeParams = useParams();

	useDeepCompareEffect(() => {
		function updateCountyState() {
			const { countyId } = routeParams;

			if (countyId === 'new') 
			{
				dispatch(newCounty());
			} else {
				dispatch(getCounty(routeParams)).then(action => {
					if (!action.payload) {
						setNoCounty(true);
					}
				});
			}
		}

		updateCountyState();
	}, [dispatch, routeParams]);

	useEffect(() => { 
		if ((county && !form) || (county && form && county._id !== form.id)) {
			setForm(county);
		}
	}, [form, county, setForm]);

	useEffect(() => {
		return () => {
			dispatch(resetCounty());
			setNoCounty(false);
		};
	}, [dispatch]);

	function handleChangeTab(event, value) {
		setTabValue(value);
	}

	function handleUploadChange(e) {
		const file = e.target.files[0];
		if (!file) {
			return;
		}
		const reader = new FileReader();
		reader.readAsBinaryString(file);

		reader.onload = () => {
			setForm(
				_.set({ ...form }, `images`, [
					{
						id: FuseUtils.generateGUID(),
						url: `data:${file.type};base64,${btoa(reader.result)}`,
						type: 'image'
					},
					...form.county_images
				])
			);
		};

		reader.onerror = () => {
			console.log('error on load image');
		};
	}

	function canBeSubmitted() { 
		return form.county_name.length > 0 && !_.isEqual(county, form);
	} 

	if (noCounty) {
		return (
			<FuseAnimate delay={100}>
				<div className="flex flex-col flex-1 items-center justify-center h-full">
					<Typography color="textSecondary" variant="h5">
						There is no such county!
					</Typography>
					<Button
						className="mt-24"
						component={Link}
						variant="outlined"
						to="/apps/elective-area/counties"
						color="inherit"
					>
						Go to Counties Page
					</Button>
				</div>
			</FuseAnimate>
		);
	}


	if ((!county || (county && routeParams.countyId !== county._id)) && routeParams.countyId !== 'new') {
		return <FuseLoading />;
	}


	
	return (
		<FusePageCarded
			classes={{
				toolbar: 'p-0',
				header: 'min-h-72 h-72 sm:h-136 sm:min-h-136'
			}}
			header={
				form && (
					<div className="flex flex-1 w-full items-center justify-between">
						<div className="flex flex-col items-start max-w-full">
							<FuseAnimate animation="transition.slideRightIn" delay={300}>
								<Typography
									className="flex items-center sm:mb-12"
									component={Link}
									role="button"
									to="/apps/e-commerce/products"
									color="inherit"
								>
									<Icon className="text-20">
										{theme.direction === 'ltr' ? 'arrow_back' : 'arrow_forward'}
									</Icon>
									<span className="mx-4">Counties</span>
								</Typography>
							</FuseAnimate>

							<div className="flex items-center max-w-full">
								<FuseAnimate animation="transition.expandIn" delay={300}>
									{form.county_images.length > 0 && form.featuredImageId ? (
										<img
											className="w-32 sm:w-48 rounded"
											src={_.find(form.county_images, { id: form.featuredImageId }).url}
											alt={form.county_name}
										/>
									) : (
										<img
											className="w-32 sm:w-48 rounded"
											src="assets/images/ecommerce/product-image-placeholder.png"
											alt={form.county_name}
										/>
									)}
								</FuseAnimate>
								<div className="flex flex-col min-w-0 mx-8 sm:mc-16">
									<FuseAnimate animation="transition.slideLeftIn" delay={300}>
										<Typography className="text-16 sm:text-20 truncate">
											{form.county_name ? form.county_name : form.county_name}
										</Typography>
									</FuseAnimate>
									<FuseAnimate animation="transition.slideLeftIn" delay={300}>
										<Typography variant="caption">County Detail</Typography>
									</FuseAnimate>
								</div>
							</div>
						</div>
						{/* <FuseAnimate animation="transition.slideRightIn" delay={300}>
							<Button
								className="whitespace-nowrap"
								variant="contained"
								color="secondary"
								disabled={!canBeSubmitted()}
								onClick={() => dispatch(saveCounty(form))}
							>
								Save
							</Button>
						</FuseAnimate> */}
					</div>
				)
			}
			contentToolbar={
				<Tabs
					value={tabValue}
					onChange={handleChangeTab}
					indicatorColor="primary"
					textColor="primary"
					variant="scrollable"
					scrollButtons="auto"
					classes={{ root: 'w-full h-64' }}
				>
					<Tab className="h-64" label="Basic Info" />
					<Tab className="h-64" label="County Images" />
					<Tab className="h-64" label="Constituency" />
					{/* <Tab className="h-64" label="Inventory" />
					<Tab className="h-64" label="Shipping" /> */}
				</Tabs>
			}
			content={
				form && (
					<div className="p-16 sm:p-24 max-w-2xl">
						{tabValue === 0 && (
							<div>
								<TextField
									className="mt-8 mb-16"
									error={form.county_code === ''}
									required
									label="County Code"
									autoFocus
									id="county_code"
									name="county_code"
									value={form.county_code}
									onChange={handleChange}
									variant="outlined"
									fullWidth
								/>


								<TextField
									className="mt-8 mb-16"
									id="county_name"
									name="county_name"
									onChange={handleChange}
									label="County Name"
									type="text"
									value={form.county_name}  
									variant="outlined"
									fullWidth
								/>

								<TextField
									className="mt-8 mb-16"
									id="voter_registered"
									name="voter_registered"
									onChange={handleChange}
									label="Registered Voters"
									type="text"
									value={form.voter_registered}  
									variant="outlined"
									fullWidth
								/>

								<TextField
									className="mt-8 mb-8"
									id="total_polling_stations"
									name="total_polling_stations"
									onChange={handleChange}
									label="Total Polling Stations"
									type="text"
									value={form.total_polling_stations}  
									variant="outlined" 
									fullWidth
								/>
			

 
							</div>
						)}
						{tabValue === 1 && (
							<div>
								<div className="flex justify-center sm:justify-start flex-wrap -mx-8">
									<label
										htmlFor="button-file"
										className={clsx(
											classes.countyImageUpload,
											'flex items-center justify-center relative w-128 h-128 rounded-8 mx-8 mb-16 overflow-hidden cursor-pointer shadow hover:shadow-lg'
										)}
									>
										<input
											accept="image/*"
											className="hidden"
											id="button-file"
											type="file"
											onChange={handleUploadChange}
										/>
										<Icon fontSize="large" color="action">
											cloud_upload
										</Icon>
									</label>
									{ountyImages && ountyImages.map(media => (
										<div
											onClick={() => setInForm('featuredImageId', media.id)}
											onKeyDown={() => setInForm('featuredImageId', media.id)}
											role="button"
											tabIndex={0}
											className={clsx(
												classes.countyImageItem,
												'flex items-center justify-center relative w-128 h-128 rounded-8 mx-8 mb-16 overflow-hidden cursor-pointer outline-none shadow hover:shadow-lg',
												media.id === form.featuredImageId && 'featured'
											)}
											key={media.id}
										>
											<Icon className={classes.countyImageFeaturedStar}>star</Icon>
											<img className="max-w-none w-auto h-full" src={media.url} alt="county" />
										</div>
									))}
								</div>
							</div>
						)}
						{tabValue === 2 && (
							<div>
							<FusePageCarded
								classes={{
									content: 'flex',
									contentCard: 'overflow-hidden',
									header: 'min-h-72 h-72 sm:h-136 sm:min-h-136'
								}}
								header={<ConstituenciesHeader countyId={routeParams.countyId} />}
								content={<ConstituenciesTable countyId={routeParams.countyId} />}
								innerScroll
							/>
							</div>
						)}
						
					</div>
				)
			}
			innerScroll
		/>
	);
}

export default withReducer('ElectiveAreaApp', reducer)(County);
