import _ from '@lodash';
import clsx from 'clsx';
import React from 'react';
 
function StationStatus(props) {
	return (
		<div className={clsx('inline text-12 p-4 rounded truncate', props.color)}>
			{props.name}
		</div>
	);
}

export default StationStatus;
