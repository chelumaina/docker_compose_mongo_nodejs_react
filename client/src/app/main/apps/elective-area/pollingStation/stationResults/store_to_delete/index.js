import { combineReducers } from '@reduxjs/toolkit';

import station from './stationSlice';
import stations from './stationsSlice';

const reducer = combineReducers({
	station,
	stations
});

export default reducer;
