import { createSlice, createAsyncThunk } from '@reduxjs/toolkit';
import axios from 'axios';

export const getStation = createAsyncThunk('ElectiveAreaApp/order/getStation', async params => {
	const response = await axios.get('/api/e-commerce-app/order', { params });
	const data = await response.data;

	return data === undefined ? null : data;
});

export const saveStation = createAsyncThunk('ElectiveAreaApp/order/saveStation', async order => {
	const response = await axios.post('/api/e-commerce-app/order/save', order);
	const data = await response.data;
	return data;
});

const orderSlice = createSlice({
	name: 'ElectiveAreaApp/order',
	initialState: null,
	reducers: {
		resetStation: () => null
	},
	extraReducers: {
		[getStation.fulfilled]: (state, action) => action.payload,
		[saveStation.fulfilled]: (state, action) => action.payload
	}
});

export const { resetStation } = orderSlice.actions;

export default orderSlice.reducer;
