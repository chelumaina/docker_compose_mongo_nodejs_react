import FuseAnimate from '@fuse/core/FuseAnimate';
import FusePageCarded from '@fuse/core/FusePageCarded';
import Avatar from '@material-ui/core/Avatar';
import Accordion from '@material-ui/core/Accordion';
import AccordionDetails from '@material-ui/core/AccordionDetails';
import AccordionSummary from '@material-ui/core/AccordionSummary';
import Button from '@material-ui/core/Button';
import Icon from '@material-ui/core/Icon';
import { useTheme } from '@material-ui/core/styles';
import Tab from '@material-ui/core/Tab';
import Tabs from '@material-ui/core/Tabs';
import Tooltip from '@material-ui/core/Tooltip';
import Typography from '@material-ui/core/Typography';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import withReducer from 'app/store/withReducer';
import GoogleMap from 'google-map-react';
import React, { useEffect, useState } from 'react';

import { useDispatch, useSelector } from 'react-redux';
import { Link, useParams } from 'react-router-dom';
import { useDeepCompareEffect } from '@fuse/hooks';
import reducer from '../../store';
import { resetPollingStation, getPollingStation } from '../../store/pollingStationSlice';
import StationInvoice from './StationInvoice';
import StationsStatus from './StationsStatus';

function Marker(props) {
	return (
		<Tooltip title={props.text} placement="top">
			<Icon className="text-red">place</Icon>
		</Tooltip>
	);
}


function Station(props) {
	const dispatch = useDispatch();
	const pollingStation = useSelector(({ ElectiveAreaApp }) => ElectiveAreaApp.pollingStation);
	const theme = useTheme();
	const routeParams = useParams();

	console.log("pollingStation", pollingStation);
	console.log("routeParams", routeParams);

	const [tabValue, setTabValue] = useState(0);
	const [noStation, setNoStation] = useState(false);
	const [map, setMap] = useState('shipping');

	useDeepCompareEffect(() => {
		dispatch(getPollingStation(routeParams)).then(action => {
			if (!action.payload) {
				setNoStation(true);
			}
		});
	}, [dispatch, routeParams]);

	useEffect(() => {
		return () => {
			dispatch(resetPollingStation());
			setNoStation(false);
		};
	}, [dispatch]);

	function handleChangeTab(event, value) {
		setTabValue(value);
	}

	if (noStation) {
		return (
			<FuseAnimate delay={100}>
				<div className="flex flex-col flex-1 items-center justify-center h-full">
					<Typography color="textSecondary" variant="h5">
						There is no such pollingStation!
					</Typography>
					<Button
						className="mt-24"
						component={Link}
						variant="outlined"
						to="/apps/e-commerce/stations"
						color="inherit"
					>
						Go to Stations Page
					</Button>
				</div>
			</FuseAnimate>
		);
	}

	return (
		<FusePageCarded
			classes={{
				content: 'flex',
				header: 'min-h-72 h-72 sm:h-136 sm:min-h-136'
			}}
			header={
				pollingStation && (
					<div className="flex flex-1 w-full items-center justify-between">
						<div className="flex flex-1 flex-col items-center sm:items-start">
							<FuseAnimate animation="transition.slideRightIn" delay={300}>
								<Typography
									className="flex items-center sm:mb-12"
									component={Link}
									role="button"
									to="/apps/e-commerce/stations"
									color="inherit"
								>
									<Icon className="text-20">
										{theme.direction === 'ltr' ? 'arrow_back' : 'arrow_forward'}
									</Icon>
									<span className="mx-4">Stations</span>
								</Typography>
							</FuseAnimate>

							<div className="flex flex-col min-w-0 items-center sm:items-start">
								<FuseAnimate animation="transition.slideLeftIn" delay={300}>
									<Typography className="text-16 sm:text-20 truncate">
										{`Station ${pollingStation.polling_station_name}`}
									</Typography>
								</FuseAnimate>

								<FuseAnimate animation="transition.slideLeftIn" delay={300}>
									<Typography variant="caption">
										{`From ${pollingStation.polling_station_name} ${pollingStation.polling_station_name}`}
									</Typography>
								</FuseAnimate>
							</div>
						</div>
					</div>
				)
			}
			contentToolbar={
				<Tabs
					value={tabValue}
					onChange={handleChangeTab}
					indicatorColor="primary"
					textColor="primary"
					variant="scrollable"
					scrollButtons="auto"
					classes={{ root: 'w-full h-64' }}
				>
					<Tab className="h-64" label="Station Details" />
					<Tab className="h-64" label="Products" />
					<Tab className="h-64" label="Invoice" />
				</Tabs>
			}
			content={
				pollingStation && (
					<div className="p-16 sm:p-24 max-w-2xl w-full">
						{/* Station Details */}
						{tabValue === 0 && (
							<div>
								<div className="pb-48">
									<div className="pb-16 flex items-center">
										<Icon color="action">account_circle</Icon>
										<Typography className="h2 mx-16" color="textSecondary">
											Customer
										</Typography>
									</div>

									<div className="mb-24">
										<div className="table-responsive mb-48">
											<table className="simple">
												<thead>
													<tr>
														<th>Name</th>
														<th>Email</th>
														<th>Phone</th>
														<th>Company</th>
													</tr>
												</thead>
												<tbody>
													<tr>
														<td>
															<div className="flex items-center">
																<Avatar src={pollingStation.polling_station_name} />
																<Typography className="truncate mx-8">
																	{`${pollingStation.polling_station_name} ${pollingStation.polling_station_name}`}
																</Typography>
															</div>
														</td>
														<td>
															<Typography className="truncate">
																{pollingStation.polling_station_name}
															</Typography>
														</td>
														<td>
															<Typography className="truncate">
																{pollingStation.polling_station_name}
															</Typography>
														</td>
														<td>
															<span className="truncate">{pollingStation.polling_station_name}</span>
														</td>
													</tr>
												</tbody>
											</table>
										</div>

										<Accordion
											className="shadow"
											expanded={map === 'shipping'}
											onChange={() => setMap(map !== 'shipping' ? 'shipping' : false)}
										>
											<AccordionSummary expandIcon={<ExpandMoreIcon />}>
												<Typography className="font-600">Shipping Address</Typography>
											</AccordionSummary>
											<AccordionDetails className="flex flex-col md:flex-row">
												<Typography className="w-full md:max-w-256 mb-16 md:mb-0">
													{pollingStation.polling_station_name}
												</Typography>
												<div className="w-full h-320">
													{/* <GoogleMap
														bootstrapURLKeys={{
															key: process.env.REACT_APP_MAP_KEY
														}}
														defaultZoom={15}
														defaultCenter={[
															pollingStation.polling_station_name,
															pollingStation.polling_station_name
														]}
													>
														<Marker
															text={pollingStation.polling_station_name}
															lat={pollingStation.polling_station_name}
															lng={pollingStation.polling_station_name}
														/>
													</GoogleMap> */}
												</div>
											</AccordionDetails>
										</Accordion>

										<Accordion
											className="shadow"
											expanded={map === 'invoice'}
											onChange={() => setMap(map !== 'invoice' ? 'invoice' : false)}
										>
											<AccordionSummary expandIcon={<ExpandMoreIcon />}>
												<Typography className="font-600">Invoice Address</Typography>
											</AccordionSummary>
											<AccordionDetails className="flex flex-col md:flex-row">
												<Typography className="w-full md:max-w-256 mb-16 md:mb-0">
													{pollingStation.polling_station_name}
												</Typography>
												<div className="w-full h-320">
													{/* <GoogleMap
														bootstrapURLKeys={{
															key: process.env.REACT_APP_MAP_KEY
														}}
														defaultZoom={15}
														defaultCenter={[
															pollingStation.polling_station_name,
															pollingStation.polling_station_name
														]}
													>
														<Marker
															text={pollingStation.polling_station_name}
															lat={pollingStation.polling_station_name}
															lng={pollingStation.polling_station_name}
														/>
													</GoogleMap> */}
												</div>
											</AccordionDetails>
										</Accordion>
									</div>
								</div>

								<div className="pb-48">
									<div className="pb-16 flex items-center">
										<Icon color="action">access_time</Icon>
										<Typography className="h2 mx-16" color="textSecondary">
											Station Status
										</Typography>
									</div>

									<div className="table-responsive">
										<table className="simple">
											<thead>
												<tr>
													<th>Status</th>
													<th>Updated On</th>
												</tr>
											</thead>
											<tbody>
												{/* {pollingStation.map(status => ( */}
													<tr key={pollingStation._id}>
														<td>
															<StationsStatus name={pollingStation.polling_station_name} color="red" />
														</td>
														<td>{pollingStation.polling_station_name}</td>
													</tr>
												{/* ))} */}
											</tbody>
										</table>
									</div>
								</div>

								<div className="pb-48">
									<div className="pb-16 flex items-center">
										<Icon color="action">attach_money</Icon>
										<Typography className="h2 mx-16" color="textSecondary">
											Payment
										</Typography>
									</div>

									<div className="table-responsive">
										<table className="simple">
											<thead>
												<tr>
													<th>TransactionID</th>
													<th>Payment Method</th>
													<th>Amount</th>
													<th>Date</th>
												</tr>
											</thead>
											<tbody>
												<tr>
													<td>
														<span className="truncate">{pollingStation.polling_station_name}</span>
													</td>
													<td>
														<span className="truncate">{pollingStation.polling_station_name}</span>
													</td>
													<td>
														<span className="truncate">{pollingStation.polling_station_name}</span>
													</td>
													<td>
														<span className="truncate">{pollingStation.polling_station_name}</span>
													</td>
												</tr>
											</tbody>
										</table>
									</div>
								</div>

								<div className="pb-48">
									<div className="pb-16 flex items-center">
										<Icon color="action">local_shipping</Icon>
										<Typography className="h2 mx-12" color="textSecondary">
											Shipping
										</Typography>
									</div>

									<div className="table-responsive">
										<table className="simple">
											<thead>
												<tr>
													<th>Tracking Code</th>
													<th>Carrier</th>
													<th>Weight</th>
													<th>Fee</th>
													<th>Date</th>
												</tr>
											</thead>
											<tbody>
												{/* {pollingStation.map(shipping => ( */}
													<tr key={pollingStation._id}>
														<td>
															<span className="truncate">{pollingStation.polling_station_name}</span>
														</td>
														<td>
															<span className="truncate">{pollingStation.polling_station_name}</span>
														</td>
														<td>
															<span className="truncate">{pollingStation.polling_station_name}</span>
														</td>
														<td>
															<span className="truncate">{pollingStation.polling_station_name}</span>
														</td>
														<td>
															<span className="truncate">{pollingStation.polling_station_name}</span>
														</td>
													</tr>
												{/* ))} */}
											</tbody>
										</table>
									</div>
								</div>
							</div>
						)}
						{/* Products */}
						{tabValue === 1 && (
							<div className="table-responsive">
								<table className="simple">
									<thead>
										<tr>
											<th>ID</th>
											<th>Image</th>
											<th>Name</th>
											<th>Price</th>
											<th>Quantity</th>
										</tr>
									</thead>
									<tbody>
										{/* {pollingStation.map(product => ( */}
											<tr key={pollingStation._id}>
												<td className="w-64">{pollingStation._id}</td>
												<td className="w-80">
													<img className="product-image" src={pollingStation.polling_station_name} alt="product" />
												</td>
												<td>
													<Typography
														component={Link}
														to={`/apps/e-commerce/products/${pollingStation._id}`}
														className="truncate"
														style={{
															color: 'inherit',
															textDecoration: 'underline'
														}}
													>
														{pollingStation.polling_station_name}
													</Typography>
												</td>
												<td className="w-64 text-right">
													<span className="truncate">${pollingStation.polling_station_name}</span>
												</td>
												<td className="w-64 text-right">
													<span className="truncate">{pollingStation.polling_station_name}</span>
												</td>
											</tr>
										{/* ))} */}
									</tbody>
								</table>
							</div>
						)}
						{/* Invoice */}
						{tabValue === 2 && <StationInvoice pollingStation={pollingStation} />}
					</div>
				)
			}
			innerScroll
		/>
	);
}

export default withReducer('ElectiveAreaApp', reducer)(Station);
