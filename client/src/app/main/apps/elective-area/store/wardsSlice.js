import { createSlice, createAsyncThunk, createEntityAdapter } from '@reduxjs/toolkit';
import axios from 'axios';
import {getConstituencyWards } from '../../../../../http_util/util';
 

export const getAllWards = createAsyncThunk('ElectiveArea/wards/getAllWards', async (constituencyId) => {
	const response = await getConstituencyWards(constituencyId); 
	return response;
});

export const removeWards = createAsyncThunk(
	'ElectiveArea/wards/removeWards',
	async (wardIds, { dispatch, getState }) => {
		await axios.post('/api/e-commerce-app/remove-wards', { wardIds });

		return wardIds;
	}
);

const wardsAdapter = createEntityAdapter({});

export const { selectAll: selectWards, selectById: selectCountyById } = wardsAdapter.getSelectors(
	state => state.ElectiveAreaApp.wards 
);


export const state = wardsAdapter.getSelectors(
	state => state.ElectiveAreaApp.wards 
);

const wardsSlice = createSlice({
	name: 'ElectiveArea/wards',
	initialState: wardsAdapter.getInitialState({
		searchText: ''
	}),
	reducers: {
		setWardsSearchText: {
			reducer: (state, action) => {
				state.searchText = action.payload;
			},
			prepare: event => ({ payload: event.target.value || '' })
		}
	},
	extraReducers: {
		[getAllWards.fulfilled]: wardsAdapter.setAll,
		[removeWards.fulfilled]: (state, action) => wardsAdapter.removeMany(state, action.payload)
	}
});

export const { setWardsSearchText } = wardsSlice.actions;

export default wardsSlice.reducer;
