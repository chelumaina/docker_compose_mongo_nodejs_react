import { createSlice, createAsyncThunk, createEntityAdapter } from '@reduxjs/toolkit';
import axios from 'axios';
import {
	// getConstituencies, 
	// getPositions, 
	getCountyContituencies, 
	// getConstituencyContituenciesWards, 
	// getConstituencyContituenciesWardsPollingStations 
} from '../../../../../http_util/util';
 

export const getAllConstituencies = createAsyncThunk('ElectiveArea/constituencies/getAllConstituencies', async (countyId) => {
	const response = await getCountyContituencies(countyId); 
	return response;
});

export const removeConstituencies = createAsyncThunk(
	'ElectiveArea/constituencies/removeConstituencies',
	async (constituencyIds, { dispatch, getState }) => {
		await axios.post('/api/e-commerce-app/remove-constituencies', { constituencyIds });

		return constituencyIds;
	}
);

const constituenciesAdapter = createEntityAdapter({});

export const { selectAll: selectConstituencies, selectById: selectConstituencyById } = constituenciesAdapter.getSelectors(
	state => state.ElectiveAreaApp.constituencies 
);


export const state = constituenciesAdapter.getSelectors(
	state => state.ElectiveAreaApp.constituencies 
);
 
const constituenciesSlice = createSlice({
	name: 'ElectiveArea/constituencies',
	initialState: constituenciesAdapter.getInitialState({
		searchText: ''
	}),
	reducers: {
		setConstituenciesSearchText: {
			reducer: (state, action) => {
				state.searchText = action.payload;
			},
			prepare: event => ({ payload: event.target.value || '' })
		}
	},
	extraReducers: {
		[getAllConstituencies.fulfilled]: constituenciesAdapter.setAll,
		[removeConstituencies.fulfilled]: (state, action) => constituenciesAdapter.removeMany(state, action.payload)
	}
});

export const { setConstituenciesSearchText } = constituenciesSlice.actions;

export default constituenciesSlice.reducer;
