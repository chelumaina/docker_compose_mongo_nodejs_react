import { combineReducers } from '@reduxjs/toolkit';
import constituencies from './constituenciesSlice';
import constituency from './constituencySlice';
import county from './countySlice';
import counties from './countiesSlice';
import ward from './wardSlice';
import wards from './wardsSlice';
import pollingStation from './pollingStationSlice';
import pollingStations from './pollingStationsSlice';

const reducer = combineReducers({
	counties,
	county,
	constituencies,
	constituency,
	wards,
	ward,
	pollingStations,
	pollingStation
});

export default reducer;
