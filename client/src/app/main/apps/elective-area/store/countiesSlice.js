import { createSlice, createAsyncThunk, createEntityAdapter } from '@reduxjs/toolkit';
import axios from 'axios';
import {
	getCounties, 
	// getPositions, 
	// getCountyContituencies, 
	// getCountyContituenciesWards, 
	// getCountyContituenciesWardsPollingStations 
} from '../../../../../http_util/util';
 

export const getAllCounties = createAsyncThunk('ElectiveArea/counties/getAllCounties', async () => {
	const response = await getCounties(); 
	return response;
});

export const removeCounties = createAsyncThunk(
	'ElectiveArea/counties/removeCounties',
	async (countyIds, { dispatch, getState }) => {
		await axios.post('/api/e-commerce-app/remove-counties', { countyIds });

		return countyIds;
	}
);

const countiesAdapter = createEntityAdapter({});

export const { selectAll: selectCounties, selectById: selectCountyById } = countiesAdapter.getSelectors(
	state => state.ElectiveAreaApp.counties 
);


export const state = countiesAdapter.getSelectors(
	state => state.ElectiveAreaApp.counties 
);

const countiesSlice = createSlice({
	name: 'ElectiveArea/counties',
	initialState: countiesAdapter.getInitialState({
		searchText: ''
	}),
	reducers: {
		setCountiesSearchText: {
			reducer: (state, action) => {
				state.searchText = action.payload;
			},
			prepare: event => ({ payload: event.target.value || '' })
		}
	},
	extraReducers: {
		[getAllCounties.fulfilled]: countiesAdapter.setAll,
		[removeCounties.fulfilled]: (state, action) => countiesAdapter.removeMany(state, action.payload)
	}
});

export const { setCountiesSearchText } = countiesSlice.actions;

export default countiesSlice.reducer;
