import { createSlice, createAsyncThunk } from '@reduxjs/toolkit';
import axios from 'axios';
import FuseUtils from '@fuse/utils';
import {getOnePollingStation} from '../../../../../http_util/util';

export const  getPollingStation = createAsyncThunk('ElectiveAreaApp/pollingStation/getPollingStation', async params => {
	const data =await getOnePollingStation(params.pollingStationId); 
	return data === undefined ? null : data;
});

export const savePollingStation = createAsyncThunk('ElectiveAreaApp/pollingStation/savePollingStation', async pollingStation => {
	const response = await axios.post('/api/e-commerce-app/pollingStation/save', pollingStation);
	const data = await response.data;
	return data;
});

const pollingStationSlice = createSlice({
	name: 'ElectiveAreaApp/pollingStation',
	initialState: null,
	reducers: {
		resetPollingStation: () => null,
		newPollingStation: {
			reducer: (state, action) => action.payload,
			prepare: event => ({
				payload: {
					id: FuseUtils.generateGUID(),
					name: '',
					handle: '',
					description: '',
					categories: [],
					tags: [],
					images: [],
					priceTaxExcl: 0,
					priceTaxIncl: 0,
					taxRate: 0,
					comparedPrice: 0,
					quantity: 0,
					sku: '',
					width: '',
					height: '',
					depth: '',
					weight: '',
					extraShippingFee: 0,
					active: true
				}
			})
		}
	},
	extraReducers: {
		[getPollingStation.fulfilled]: (state, action) => action.payload,
		[savePollingStation.fulfilled]: (state, action) => action.payload
	}
});

export const { newPollingStation, resetPollingStation } = pollingStationSlice.actions;
 
export default pollingStationSlice.reducer;
