import { createSlice, createAsyncThunk } from '@reduxjs/toolkit';
import axios from 'axios';
import FuseUtils from '@fuse/utils';
import {getOneConstituency} from '../../../../../http_util/util';

export const  getConstituency = createAsyncThunk('ElectiveAreaApp/constituency/getConstituency', async params => {
	const data =await getOneConstituency(params.constituencyId); 
	return data === undefined ? null : data;
});

export const saveConstituency = createAsyncThunk('ElectiveAreaApp/constituency/saveConstituency', async constituency => {
	const response = await axios.post('/api/e-commerce-app/constituency/save', constituency);
	const data = await response.data;
	return data;
});

const constituencySlice = createSlice({
	name: 'ElectiveAreaApp/constituency',
	initialState: null,
	reducers: {
		resetConstituency: () => null,
		newConstituency: {
			reducer: (state, action) => action.payload,
			prepare: event => ({
				payload: {
					id: FuseUtils.generateGUID(),
					name: '',
					handle: '',
					description: '',
					categories: [],
					tags: [],
					images: [],
					priceTaxExcl: 0,
					priceTaxIncl: 0,
					taxRate: 0,
					comparedPrice: 0,
					quantity: 0,
					sku: '',
					width: '',
					height: '',
					depth: '',
					weight: '',
					extraShippingFee: 0,
					active: true
				}
			})
		}
	},
	extraReducers: {
		[getConstituency.fulfilled]: (state, action) => action.payload,
		[saveConstituency.fulfilled]: (state, action) => action.payload
	}
});

export const { newConstituency, resetConstituency } = constituencySlice.actions;

export default constituencySlice.reducer;
