import { createSlice, createAsyncThunk, createEntityAdapter } from '@reduxjs/toolkit';
import axios from 'axios';
import {getWardPollingStations} from '../../../../../http_util/util';
 

export const getAllPollingStations = createAsyncThunk('ElectiveArea/pollingStations/getAllPollingStations', async (wardId) => {
	const response = await getWardPollingStations(wardId);
	
	return response;
});

export const removePollingStations = createAsyncThunk(
	'ElectiveArea/pollingStations/removePollingStations',
	async (wardIds, { dispatch, getState }) => {
		await axios.post('/api/e-commerce-app/remove-pollingStations', { wardIds });

		return wardIds;
	}
);

const pollingStationsAdapter = createEntityAdapter({});

export const { selectAll: selectPollingStations, selectById: selectPollingStationById } = pollingStationsAdapter.getSelectors(
	state => state.ElectiveAreaApp.pollingStations 
);


export const state = pollingStationsAdapter.getSelectors(
	state => state.ElectiveAreaApp.pollingStations 
);

const pollingStationsSlice = createSlice({
	name: 'ElectiveArea/pollingStations',
	initialState: pollingStationsAdapter.getInitialState({
		searchText: ''
	}),
	reducers: {
		setPollingStationsSearchText: {
			reducer: (state, action) => {
				state.searchText = action.payload;
			},
			prepare: event => ({ payload: event.target.value || '' })
		}
	},
	extraReducers: {
		[getAllPollingStations.fulfilled]: pollingStationsAdapter.setAll,
		[removePollingStations.fulfilled]: (state, action) => pollingStationsAdapter.removeMany(state, action.payload)
	}
});

export const { setPollingStationsSearchText } = pollingStationsSlice.actions;

export default pollingStationsSlice.reducer;
