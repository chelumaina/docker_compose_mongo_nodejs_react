import { createSlice, createAsyncThunk } from '@reduxjs/toolkit';
import axios from 'axios';
import FuseUtils from '@fuse/utils';
import {getOneWard} from '../../../../../http_util/util';

export const  getWard = createAsyncThunk('ElectiveAreaApp/ward/getWard', async params => {
	const data =await getOneWard(params.wardId); 
	return data === undefined ? null : data;
});

export const saveWard = createAsyncThunk('ElectiveAreaApp/ward/saveWard', async ward => {
	const response = await axios.post('/api/e-commerce-app/ward/save', ward);
	const data = await response.data;
	return data;
});

const wardSlice = createSlice({
	name: 'ElectiveAreaApp/ward',
	initialState: null,
	reducers: {
		resetWard: () => null,
		newWard: {
			reducer: (state, action) => action.payload,
			prepare: event => ({
				payload: {
					id: FuseUtils.generateGUID(),
					name: '',
					handle: '',
					description: '',
					categories: [],
					tags: [],
					images: [],
					priceTaxExcl: 0,
					priceTaxIncl: 0,
					taxRate: 0,
					comparedPrice: 0,
					quantity: 0,
					sku: '',
					width: '',
					height: '',
					depth: '',
					weight: '',
					extraShippingFee: 0,
					active: true
				}
			})
		}
	},
	extraReducers: {
		[getWard.fulfilled]: (state, action) => action.payload,
		[saveWard.fulfilled]: (state, action) => action.payload
	}
});

export const { newWard, resetWard } = wardSlice.actions;

export default wardSlice.reducer;
