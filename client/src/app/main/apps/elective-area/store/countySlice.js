import { createSlice, createAsyncThunk } from '@reduxjs/toolkit';
// import axios from 'axios';
import FuseUtils from '@fuse/utils';
import {
	// getCounties, 
	getOneCounties, 
	createCounty,
	// getPositions, 
	// getCountyContituencies, 
	// getCountyContituenciesWards, 
	// getCountyContituenciesWardsPollingStations 
} from '../../../../../http_util/util';

export const  getCounty = createAsyncThunk('ElectiveAreaApp/county/getCounty', async params => {
	const data =await getOneCounties(params.countyId); 
	return data === undefined ? null : data;
});

export const saveCounty = createAsyncThunk('ElectiveAreaApp/county/saveCounty', async county => {
	// const response = await axios.post('/api/e-commerce-app/county/save', county);
	const response =await createCounty(county);  
	const data = await response.data.message;
	return data;
});

const countySlice = createSlice({
	name: 'ElectiveAreaApp/county',
	initialState: null,
	reducers: {
		resetCounty: () => null,
		newCounty: {
			reducer: (state, action) => action.payload,
			prepare: event => ({
				payload: { 
					county_name: '', 
					county_code: ''
				}
			})
		}
	},
	extraReducers: {
		[getCounty.fulfilled]: (state, action) => action.payload,
		[saveCounty.fulfilled]: (state, action) => action.payload
	}
});

export const { newCounty, resetCounty } = countySlice.actions;

export default countySlice.reducer;
