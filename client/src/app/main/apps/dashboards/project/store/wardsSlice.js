import { createEntityAdapter, createSlice, createAsyncThunk } from '@reduxjs/toolkit';
import JwtService from 'app/services/jwtService';
import axios from 'axios';
 

const headers = {headers:{
	'Content-Type': 'application/json',
	'x-access-token': JwtService.getAccessToken()
  }
};


export const getCountyContituenciesWards = createAsyncThunk('projectDashboardApp/wards/getCountyContituenciesWards', async (constituency_id) => {
	const response = await axios.get(`${process.env.REACT_APP_API_URL}/api/v1/wards/constituencyWards/${constituency_id}?pageNo=1&size=100`,headers);
	const c=response.data.message.map(d=>({id:d._id, name:d.ward_code+"  -  "+d.ward_name}));
	return c.sort((a, b) => a.name.toLowerCase() > b.name.toLowerCase() ? 1 : -1);;
});

const wardsAdapter = createEntityAdapter({});

export const {
	selectAll: selectWards,
	selectEntities: selectWardsEntities,
	selectById: selectWardById
} = wardsAdapter.getSelectors(state => state.projectDashboardApp.wards);

const wardsSlice = createSlice({
	name: 'projectDashboardApp/wards',
	initialState: wardsAdapter.getInitialState(),
	reducers: {},
	extraReducers: {
		[getCountyContituenciesWards.fulfilled]: wardsAdapter.setAll
	}
});

export default wardsSlice.reducer;
