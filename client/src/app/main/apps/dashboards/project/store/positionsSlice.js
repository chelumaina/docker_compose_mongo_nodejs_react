import { createEntityAdapter, createSlice, createAsyncThunk } from '@reduxjs/toolkit';
import JwtService from 'app/services/jwtService';
import axios from 'axios';
// import _ from 'lodash';

const headers = {headers:{
	'Content-Type': 'application/json',
	'x-access-token': JwtService.getAccessToken()
  }
};


 
export const getPositions = createAsyncThunk('projectDashboardApp/positions/getPositions', async () => {

	const response = await axios.get(`${process.env.REACT_APP_API_URL}/api/v1/position?pageNo=1&size=100`,headers);
	const c=response.data.message.map(d=>({id:d._id, name:d.name}));

	return c;
});

const positionsAdapter = createEntityAdapter({});

export const {
	selectAll: selectPositions,
	selectEntities: selectPositionsEntities,
	selectById: selectPositionById
} = positionsAdapter.getSelectors(state => state.projectDashboardApp.positions);

const positionsSlice = createSlice({
	name: 'projectDashboardApp/positions',
	initialState: positionsAdapter.getInitialState(),
	reducers: {},
	extraReducers: {
		[getPositions.fulfilled]: positionsAdapter.setAll
	}
});

export default positionsSlice.reducer;
