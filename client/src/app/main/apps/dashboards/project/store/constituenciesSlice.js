import { createEntityAdapter, createSlice, createAsyncThunk } from '@reduxjs/toolkit';
import JwtService from 'app/services/jwtService';
import axios from 'axios';
 

const headers = {headers:{
	'Content-Type': 'application/json',
	'x-access-token': JwtService.getAccessToken()
  }
};


export const getCountyContituencies = createAsyncThunk('projectDashboardApp/constituencies/getCountyContituencies', async (county_id) => {
	const response = await axios.get(`${process.env.REACT_APP_API_URL}/api/v1/constituency/get_county_constituencies/${county_id}?pageNo=1&size=100`,headers);
	const c=response.data.message.map(d=>({id:d._id, name:d.constituency_code+" - "+d.constituency_name}));
	return c.sort((a, b) => a.name.toLowerCase() > b.name.toLowerCase() ? 1 : -1);
});

const constituenciesAdapter = createEntityAdapter({});

export const {
	selectAll: selectConstituencies,
	selectEntities: selectConstituenciesEntities,
	selectById: selectConstituencyById
} = constituenciesAdapter.getSelectors(state => state.projectDashboardApp.constituencies);

const constituenciesSlice = createSlice({
	name: 'projectDashboardApp/constituencies',
	initialState: constituenciesAdapter.getInitialState(),
	reducers: {},
	extraReducers: {
		[getCountyContituencies.fulfilled]: constituenciesAdapter.setAll 

	}
});

export default constituenciesSlice.reducer;
