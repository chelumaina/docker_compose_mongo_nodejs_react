import { createEntityAdapter, createSlice, createAsyncThunk } from '@reduxjs/toolkit';
import JwtService from 'app/services/jwtService';
import axios from 'axios';
 

const headers = {headers:{
	'Content-Type': 'application/json',
	'x-access-token': JwtService.getAccessToken()
  }
};


export const getCountyContituenciesWardsPollingStations = createAsyncThunk('projectDashboardApp/pollingStation/getCountyContituenciesWardsPollingStations', async (constituency_id) => {
	const response = await axios.get(`${process.env.REACT_APP_API_URL}/api/v1/pollingStation/ward_stations/${constituency_id}?pageNo=1&size=100`,headers);
	const c=response.data.message.map(d=>({id:d._id, name:d.polling_station_code+" - "+d.polling_station_name}));
	return c.sort((a, b) => a.name.toLowerCase() > b.name.toLowerCase() ? 1 : -1);;
});

const pollingStationsAdapter = createEntityAdapter({});

export const {
	selectAll: selectPollingStations,
	selectEntities: selectPollingStationsEntities,
	selectById: selectStationById
} = pollingStationsAdapter.getSelectors(state => state.projectDashboardApp.pollingStations);

const pollingStationsSlice = createSlice({
	name: 'projectDashboardApp/pollingStations',
	initialState: pollingStationsAdapter.getInitialState(),
	reducers: {},
	extraReducers: {
		[getCountyContituenciesWardsPollingStations.fulfilled]: pollingStationsAdapter.setAll
	}
});

export default pollingStationsSlice.reducer;
