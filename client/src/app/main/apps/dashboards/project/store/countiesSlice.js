import { createEntityAdapter, createSlice, createAsyncThunk } from '@reduxjs/toolkit';
import JwtService from 'app/services/jwtService';
import axios from 'axios';

const headers = {headers:{
	'Content-Type': 'application/json',
	'x-access-token': JwtService.getAccessToken()
  }
};


export const getCounties = createAsyncThunk('projectDashboardApp/counties/getCounties', async () => {
	const response = await axios.get("http://localhost:3030/api/v1/county",headers);
	const c=response.data.message.map(d=>({id:d._id, name:d.county_name}));
	return c.sort((a, b) => a.name.toLowerCase() > b.name.toLowerCase() ? 1 : -1);;
});

 

const countiesAdapter = createEntityAdapter({});

export const {
	selectAll: selectCounties,
	selectEntities: selectCountiesEntities,
	selectById: selectCountyById
} = countiesAdapter.getSelectors(state => state.projectDashboardApp.counties);

const countiesSlice = createSlice({
	name: 'projectDashboardApp/counties',
	initialState: countiesAdapter.getInitialState(),
	reducers: {},
	extraReducers: {
		[getCounties.fulfilled]: countiesAdapter.setAll
	}
});

export default countiesSlice.reducer;
