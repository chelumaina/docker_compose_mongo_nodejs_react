import FusePageSimple from '@fuse/core/FusePageSimple';
import withReducer from 'app/store/withReducer';
import React, { useRef } from 'react';
import { useDispatch } from 'react-redux';
import { useParams } from 'react-router-dom';
import { useDeepCompareEffect } from '@fuse/hooks';
import CandidateDialog from './CandidateDialog';
import CandidatesHeader from './CandidatesHeader';
import CandidatesList from './CandidatesList';
import CandidatesSidebarContent from './CandidatesSidebarContent';
import reducer from './store';
import { getCandidates } from './store/candidatesSlice';
import { getUserData } from './store/userSlice';

function CandidatesApp(props) {
	const dispatch = useDispatch();

	const pageLayout = useRef(null);
	const routeParams = useParams();

	useDeepCompareEffect(() => {
		dispatch(getCandidates(routeParams));
		dispatch(getUserData());
	}, [dispatch, routeParams]);

	return (
		<>
			<FusePageSimple
				classes={{
					contentWrapper: 'p-0 sm:p-24 h-full',
					content: 'flex flex-col h-full',
					leftSidebar: 'w-256 border-0',
					header: 'min-h-72 h-72 sm:h-136 sm:min-h-136',
					wrapper: 'min-h-0'
				}}
				header={<CandidatesHeader pageLayout={pageLayout} />}
				content={<CandidatesList />}
				leftSidebarContent={<CandidatesSidebarContent />}
				sidebarInner
				ref={pageLayout}
				innerScroll
			/>
			<CandidateDialog />
		</>
	);
}

export default withReducer('candidatesApp', reducer)(CandidatesApp);
