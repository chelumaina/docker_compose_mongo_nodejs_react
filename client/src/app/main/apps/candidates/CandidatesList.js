import FuseAnimate from '@fuse/core/FuseAnimate';
import FuseUtils from '@fuse/utils';
import Avatar from '@material-ui/core/Avatar';
import Icon from '@material-ui/core/Icon';
import IconButton from '@material-ui/core/IconButton';
import Typography from '@material-ui/core/Typography';
import React, { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import CandidatesMultiSelectMenu from './CandidatesMultiSelectMenu';
import CandidatesTable from './CandidatesTable';
import { openEditCandidateDialog, removeCandidate, toggleStarredCandidate, selectCandidates } from './store/candidatesSlice';

function CandidatesList(props) {
	const dispatch = useDispatch();
	const candidates = useSelector(selectCandidates);
	const searchText = useSelector(({ candidatesApp }) => candidatesApp.candidates.searchText);
	const user = useSelector(({ candidatesApp }) => candidatesApp.user);

	const [filteredData, setFilteredData] = useState(null);

	const columns = React.useMemo(
		() => [
			{
				Header: ({ selectedFlatRows }) => {
					const selectedRowIds = selectedFlatRows.map(row => row.original.id);

					return (
						selectedFlatRows.length > 0 && <CandidatesMultiSelectMenu selectedCandidateIds={selectedRowIds} />
					);
				},
				accessor: 'avatar',
				Cell: ({ row }) => {
					return <Avatar className="mx-8" alt={row.original.name} src={row.original.avatar} />;
				},
				className: 'justify-center',
				width: 64,
				sortable: false
			},
			{
				Header: 'Candidate Name',
				accessor: 'candidate_name',
				className: 'font-bold',
				sortable: true
			},
			{
				Header: 'Party Name',
				accessor: 'party_name',
				className: 'font-bold',
				sortable: true
			},
			{
				Header: 'Position',
				accessor: 'position_name',
				sortable: true
			},
			// {
			// 	Header: 'Job Title',
			// 	accessor: 'jobTitle',
			// 	sortable: true
			// },
			// {
			// 	Header: 'Email',
			// 	accessor: 'email',
			// 	sortable: true
			// },
			// {
			// 	Header: 'Phone',
			// 	accessor: 'phone',
			// 	sortable: true
			// },
			{
				id: 'action',
				width: 128,
				sortable: false,
				Cell: ({ row }) => (
					<div className="flex items-center">
						<IconButton
							onClick={ev => {
								ev.stopPropagation();
								dispatch(toggleStarredCandidate(row.original.id));
							}}
						>
							{user.starred && user.starred.includes(row.original.id) ? (
								<Icon>star</Icon>
							) : (
								<Icon>star_border</Icon>
							)}
						</IconButton>
						<IconButton
							onClick={ev => {
								ev.stopPropagation();
								dispatch(removeCandidate(row.original.id));
							}}
						>
							<Icon>delete</Icon>
						</IconButton>
					</div>
				)
			}
		],
		[dispatch, user.starred]
	);

	useEffect(() => {
		function getFilteredArray(entities, _searchText) {
			if (_searchText.length === 0) {
				return candidates;
			}
			return FuseUtils.filterArrayByString(candidates, _searchText);
		}

		if (candidates) {
			setFilteredData(getFilteredArray(candidates, searchText));
		}
	}, [candidates, searchText]);

	if (!filteredData) {
		return null;
	}

	 

	if (filteredData.length === 0) {
		return (
			<div className="flex flex-1 items-center justify-center h-full">
				<Typography color="textSecondary" variant="h5">
					There are no candidates!
				</Typography>
			</div>
		);
	}



	return (
		<FuseAnimate animation="transition.slideUpIn" delay={300}>
			<CandidatesTable
				columns={columns}
				data={filteredData}
				onRowClick={(ev, row) => {
					if (row) {
						dispatch(openEditCandidateDialog(row.original));
					}
				}}
			/>
		</FuseAnimate>
	);
}

export default CandidatesList;
