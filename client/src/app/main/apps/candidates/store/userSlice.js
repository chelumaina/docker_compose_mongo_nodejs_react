import { createSlice, createAsyncThunk } from '@reduxjs/toolkit';
import {getCandidates} from '../../../../../http_util/util';

export const getUserData = createAsyncThunk('candidatesApp/user/getUserData', async () => {
	const response = await getCandidates();//await axios.get('/api/candidates-app/user');
	const data = await response.data.message;
	return data;
});



const userSlice = createSlice({
	name: 'candidatesApp/user',
	initialState: {},
	reducers: {},
	extraReducers: {
		[getUserData.fulfilled]: (state, action) => action.payload
	}
});

export default userSlice.reducer;
