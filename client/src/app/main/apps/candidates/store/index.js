import { combineReducers } from '@reduxjs/toolkit';
import candidates from './candidatesSlice';
import user from './userSlice';

const reducer = combineReducers({
	candidates,
	user
});

export default reducer;
