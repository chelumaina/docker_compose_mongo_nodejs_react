import { createSlice, createAsyncThunk, createEntityAdapter } from '@reduxjs/toolkit';
import axios from 'axios';
import { getUserData } from './userSlice';
import {getAllCandidates} from '../../../../../http_util/util';



export const getCandidates = createAsyncThunk('candidatesApp/candidates/getCandidates', async (routeParams, { getState }) => {
	routeParams = routeParams || getState().candidatesApp.candidates.routeParams;
	
	const data= await getAllCandidates();

	return { data, routeParams };
});


export const addCandidate = createAsyncThunk(
	'candidatesApp/candidates/addCandidate',
	async (candidate, { dispatch, getState }) => {
		const response = await axios.post('/api/candidates-app/add-candidate', { candidate });
		const data = await response.data;

		dispatch(getCandidates());

		return data;
	}
);

export const updateCandidate = createAsyncThunk(
	'candidatesApp/candidates/updateCandidate',
	async (candidate, { dispatch, getState }) => {
		const response = await axios.post('/api/candidates-app/update-candidate', { candidate });
		const data = await response.data;

		dispatch(getCandidates());

		return data;
	}
);

export const removeCandidate = createAsyncThunk(
	'candidatesApp/candidates/removeCandidate',
	async (candidateId, { dispatch, getState }) => {
		await axios.post('/api/candidates-app/remove-candidate', { candidateId });

		return candidateId;
	}
);

export const removeCandidates = createAsyncThunk(
	'candidatesApp/candidates/removeCandidates',
	async (candidateIds, { dispatch, getState }) => {
		await axios.post('/api/candidates-app/remove-candidates', { candidateIds });

		return candidateIds;
	}
);

export const toggleStarredCandidate = createAsyncThunk(
	'candidatesApp/candidates/toggleStarredCandidate',
	async (candidateId, { dispatch, getState }) => {
		const response = await axios.post('/api/candidates-app/toggle-starred-candidate', { candidateId });
		const data = await response.data;

		dispatch(getUserData());

		dispatch(getCandidates());

		return data;
	}
);

export const toggleStarredCandidates = createAsyncThunk(
	'candidatesApp/candidates/toggleStarredCandidates',
	async (candidateIds, { dispatch, getState }) => {
		const response = await axios.post('/api/candidates-app/toggle-starred-candidates', { candidateIds });
		const data = await response.data;

		dispatch(getUserData());

		dispatch(getCandidates());

		return data;
	}
);

export const setCandidatesStarred = createAsyncThunk(
	'candidatesApp/candidates/setCandidatesStarred',
	async (candidateIds, { dispatch, getState }) => {
		const response = await axios.post('/api/candidates-app/set-candidates-starred', { candidateIds });
		const data = await response.data;

		dispatch(getUserData());

		dispatch(getCandidates());

		return data;
	}
);

export const setCandidatesUnstarred = createAsyncThunk(
	'candidatesApp/candidates/setCandidatesUnstarred',
	async (candidateIds, { dispatch, getState }) => {
		const response = await axios.post('/api/candidates-app/set-candidates-unstarred', { candidateIds });
		const data = await response.data;

		dispatch(getUserData());

		dispatch(getCandidates());

		return data;
	}
);

const candidatesAdapter = createEntityAdapter({});

export const { selectAll: selectCandidates, selectById: selectCandidatesById } = candidatesAdapter.getSelectors(
	state => {
		return state.candidatesApp.candidates}
);



const candidatesSlice = createSlice({
	name: 'candidatesApp/candidates',
	initialState: candidatesAdapter.getInitialState({
		searchText: '',
		routeParams: {},
		candidateDialog: {
			type: 'new',
			props: {
				open: false
			},
			data: null
		}
	}),
	reducers: {
		setCandidatesSearchText: {
			reducer: (state, action) => {
				state.searchText = action.payload;
			},
			prepare: event => ({ payload: event.target.value || '' })
		},
		openNewCandidateDialog: (state, action) => {
			state.candidateDialog = {
				type: 'new',
				props: {
					open: true
				},
				data: null
			};
		},
		closeNewCandidateDialog: (state, action) => {
			state.candidateDialog = {
				type: 'new',
				props: {
					open: false
				},
				data: null
			};
		},
		openEditCandidateDialog: (state, action) => {
			state.candidateDialog = {
				type: 'edit',
				props: {
					open: true
				},
				data: action.payload
			};
		},
		closeEditCandidateDialog: (state, action) => {
			state.candidateDialog = {
				type: 'edit',
				props: {
					open: false
				},
				data: null
			};
		}
	},
	extraReducers: {
		[updateCandidate.fulfilled]: candidatesAdapter.upsertOne,
		[addCandidate.fulfilled]: candidatesAdapter.addOne,
		[removeCandidates.fulfilled]: (state, action) => candidatesAdapter.removeMany(state, action.payload),
		[removeCandidate.fulfilled]: (state, action) => candidatesAdapter.removeOne(state, action.payload),
		[getCandidates.fulfilled]: (state, action) => {


			
			const { data, routeParams } = action.payload;
			candidatesAdapter.setAll(state, data);
			state.routeParams = routeParams;
			state.searchText = '';
		}
	}
});

export const {
	setCandidatesSearchText,
	openNewCandidateDialog,
	closeNewCandidateDialog,
	openEditCandidateDialog,
	closeEditCandidateDialog
} = candidatesSlice.actions;

export default candidatesSlice.reducer;
