import React from 'react';
import { Redirect } from 'react-router-dom';

const CandidatesAppConfig = {
	settings: {
		layout: {
			config: {}
		}
	},
	routes: [
		{
			path: '/apps/candidates/:id',
			component: React.lazy(() => import('./CandidatesApp'))
		},
		{
			path: '/apps/candidates',
			component: () => <Redirect to="/apps/candidates/all" />
		}
	]
};

export default CandidatesAppConfig;
