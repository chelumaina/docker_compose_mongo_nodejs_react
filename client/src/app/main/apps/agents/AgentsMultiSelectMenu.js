import Icon from '@material-ui/core/Icon';
import IconButton from '@material-ui/core/IconButton';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import Menu from '@material-ui/core/Menu';
import MenuItem from '@material-ui/core/MenuItem';
import MenuList from '@material-ui/core/MenuList';
import React, { useState } from 'react';
import { useDispatch } from 'react-redux';
import { setAgentsUnstarred, setAgentsStarred, removeAgents } from './store/agentsSlice';

function AgentsMultiSelectMenu(props) {
	const dispatch = useDispatch();
	const { selectedAgentIds } = props;

	const [anchorEl, setAnchorEl] = useState(null);

	function openSelectedAgentMenu(event) {
		setAnchorEl(event.currentTarget);
	}

	function closeSelectedAgentsMenu() {
		setAnchorEl(null);
	}

	return (
		<>
			<IconButton
				className="p-0"
				aria-owns={anchorEl ? 'selectedAgentsMenu' : null}
				aria-haspopup="true"
				onClick={openSelectedAgentMenu}
			>
				<Icon>more_horiz</Icon>
			</IconButton>
			<Menu
				id="selectedAgentsMenu"
				anchorEl={anchorEl}
				open={Boolean(anchorEl)}
				onClose={closeSelectedAgentsMenu}
			>
				<MenuList>
					<MenuItem
						onClick={() => {
							dispatch(removeAgents(selectedAgentIds));
							closeSelectedAgentsMenu();
						}}
					>
						<ListItemIcon className="min-w-40">
							<Icon>delete</Icon>
						</ListItemIcon>
						<ListItemText primary="Remove" />
					</MenuItem>
					<MenuItem
						onClick={() => {
							dispatch(setAgentsStarred(selectedAgentIds));
							closeSelectedAgentsMenu();
						}}
					>
						<ListItemIcon className="min-w-40">
							<Icon>star</Icon>
						</ListItemIcon>
						<ListItemText primary="Starred" />
					</MenuItem>
					<MenuItem
						onClick={() => {
							dispatch(setAgentsUnstarred(selectedAgentIds));
							closeSelectedAgentsMenu();
						}}
					>
						<ListItemIcon className="min-w-40">
							<Icon>star_border</Icon>
						</ListItemIcon>
						<ListItemText primary="Unstarred" />
					</MenuItem>
				</MenuList>
			</Menu>
		</>
	);
}

export default AgentsMultiSelectMenu;
