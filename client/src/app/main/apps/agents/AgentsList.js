import FuseAnimate from '@fuse/core/FuseAnimate';
import FuseUtils from '@fuse/utils';
import Avatar from '@material-ui/core/Avatar';
import Icon from '@material-ui/core/Icon';
import IconButton from '@material-ui/core/IconButton';
import Typography from '@material-ui/core/Typography';
import React, { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import AgentsMultiSelectMenu from './AgentsMultiSelectMenu';
import AgentsTable from './AgentsTable';
import { openEditAgentDialog, removeAgent, toggleStarredAgent, selectAgents } from './store/agentsSlice';

function AgentsList(props) {

	const dispatch = useDispatch();
	const agents = useSelector(selectAgents);
	const searchText = useSelector(({ agentsApp }) => agentsApp.agents.searchText);
	const user = useSelector(({ agentsApp }) => agentsApp.user);


	const [filteredData, setFilteredData] = useState(null);

	const columns = React.useMemo(
		() => [
			{
				Header: ({ selectedFlatRows }) => {
					const selectedRowIds = selectedFlatRows.map(row => row.original.id);

					return (
						selectedFlatRows.length > 0 && <AgentsMultiSelectMenu selectedAgentIds={selectedRowIds} />
					);
				},
				accessor: 'avatar',
				Cell: ({ row }) => {
					return <Avatar className="mx-8" alt={row.original.name} src={row.original.avatar} />;
				},
				className: 'justify-center',
				width: 64,
				sortable: false
			},
			{
				Header: 'Agent Name',
				accessor: 'party_name',
				className: 'font-bold',
				sortable: true
			},
			{
				Header: 'Role',
				accessor: 'role',
				sortable: true
			},
			{
				Header: 'Email',
				accessor: 'email',
				sortable: true
			},
			{
				Header: 'Username',
				accessor: 'username',
				sortable: true
			},
			{
				Header: 'County',
				accessor: 'county_name',
				sortable: true
			},
			{
				Header: 'Constituency',
				accessor: 'constituency_name',
				sortable: true
			},
			{
				Header: 'Ward',
				accessor: 'ward_name',
				sortable: true
			},
			{
				Header: 'Polling Station',
				accessor: 'polling_station_id',
				sortable: true
			},
			{
				id: 'action',
				width: 128,
				sortable: false,
				Cell: ({ row }) => (
					<div className="flex items-center">
						<IconButton
							onClick={ev => {
								ev.stopPropagation();
								dispatch(toggleStarredAgent(row.original.id));
							}}
						>
							{user.starred && user.starred.includes(row.original.id) ? (
								<Icon>star</Icon>
							) : (
								<Icon>star_border</Icon>
							)}
						</IconButton>
						<IconButton
							onClick={ev => {
								ev.stopPropagation();
								dispatch(removeAgent(row.original.id));
							}}
						>
							<Icon>delete</Icon>
						</IconButton>
					</div>
				)
			}
		],
		[dispatch, user.starred]
	);

	useEffect(() => {
		function getFilteredArray(entities, _searchText) {
			if (_searchText.length === 0) {
				return agents;
			}
			return FuseUtils.filterArrayByString(agents, _searchText);
		}

		if (agents) {
			setFilteredData(getFilteredArray(agents, searchText));
		}
	}, [agents, searchText]);

	if (!filteredData) {
		return null;
	}

	 

	if (filteredData.length === 0) {
		return (
			<div className="flex flex-1 items-center justify-center h-full">
				<Typography color="textSecondary" variant="h5">
					There are no agents!
				</Typography>
			</div>
		);
	}



	return (
		<FuseAnimate animation="transition.slideUpIn" delay={300}>
			<AgentsTable
				columns={columns}
				data={filteredData}
				onRowClick={(ev, row) => {
					if (row) {
						dispatch(openEditAgentDialog(row.original));
					}
				}}
			/>
		</FuseAnimate>
	);
}

export default AgentsList;