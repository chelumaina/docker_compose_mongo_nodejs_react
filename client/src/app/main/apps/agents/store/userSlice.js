import { createSlice, createAsyncThunk } from '@reduxjs/toolkit';
import axios from 'axios';
import {getUsers} from '../../../../../http_util/util';

export const getUserData2 = createAsyncThunk('contactsApp/user/getUserData', async () => {
	const response = await axios.get('/api/contacts-app/user');
	const data = await response.data;
	return data;
});

export const getUserData = createAsyncThunk('agentsApp/agents/getAgents', async (routeParams, { getState }) => {
	routeParams = routeParams || getState().agentsApp.agents.routeParams;
	const response = await getUsers();
	const data = await response.data.message;
	return { data, routeParams };
});


const userSlice = createSlice({
	name: 'contactsApp/user',
	initialState: {},
	reducers: {},
	extraReducers: {
		[getUserData.fulfilled]: (state, action) => action.payload
	}
});

export default userSlice.reducer;
