import { createSlice, createAsyncThunk, createEntityAdapter } from '@reduxjs/toolkit';
import axios from 'axios';
import { getUserData } from './userSlice';
import {getAllAgents} from '../../../../../http_util/util';



export const getAgents = createAsyncThunk('agentsApp/agents/getAgents', async (routeParams, { getState }) => {
	routeParams = routeParams || getState().agentsApp.agents.routeParams;
	
	const data= await getAllAgents();

	return { data, routeParams };
});


export const addAgent = createAsyncThunk(
	'agentsApp/agents/addAgent',
	async (agent, { dispatch, getState }) => {
		const response = await axios.post('/api/agents-app/add-agent', { agent });
		const data = await response.data;

		dispatch(getAgents());

		return data;
	}
);

export const updateAgent = createAsyncThunk(
	'agentsApp/agents/updateAgent',
	async (agent, { dispatch, getState }) => {
		const response = await axios.post('/api/agents-app/update-agent', { agent });
		const data = await response.data;

		dispatch(getAgents());

		return data;
	}
);

export const removeAgent = createAsyncThunk(
	'agentsApp/agents/removeAgent',
	async (agentId, { dispatch, getState }) => {
		await axios.post('/api/agents-app/remove-agent', { agentId });

		return agentId;
	}
);

export const removeAgents = createAsyncThunk(
	'agentsApp/agents/removeAgents',
	async (agentIds, { dispatch, getState }) => {
		await axios.post('/api/agents-app/remove-agents', { agentIds });

		return agentIds;
	}
);

export const toggleStarredAgent = createAsyncThunk(
	'agentsApp/agents/toggleStarredAgent',
	async (agentId, { dispatch, getState }) => {
		const response = await axios.post('/api/agents-app/toggle-starred-agent', { agentId });
		const data = await response.data;

		dispatch(getUserData());

		dispatch(getAgents());

		return data;
	}
);

export const toggleStarredAgents = createAsyncThunk(
	'agentsApp/agents/toggleStarredAgents',
	async (agentIds, { dispatch, getState }) => {
		const response = await axios.post('/api/agents-app/toggle-starred-agents', { agentIds });
		const data = await response.data;

		dispatch(getUserData());

		dispatch(getAgents());

		return data;
	}
);

export const setAgentsStarred = createAsyncThunk(
	'agentsApp/agents/setAgentsStarred',
	async (agentIds, { dispatch, getState }) => {
		const response = await axios.post('/api/agents-app/set-agents-starred', { agentIds });
		const data = await response.data;

		dispatch(getUserData());

		dispatch(getAgents());

		return data;
	}
);

export const setAgentsUnstarred = createAsyncThunk(
	'agentsApp/agents/setAgentsUnstarred',
	async (agentIds, { dispatch, getState }) => {
		const response = await axios.post('/api/agents-app/set-agents-unstarred', { agentIds });
		const data = await response.data;

		dispatch(getUserData());

		dispatch(getAgents());

		return data;
	}
);

const agentsAdapter = createEntityAdapter({});

export const { selectAll: selectAgents, selectById: selectAgentsById } = agentsAdapter.getSelectors(
	state => {
		return state.agentsApp.agents}
);



const agentsSlice = createSlice({
	name: 'agentsApp/agents',
	initialState: agentsAdapter.getInitialState({
		searchText: '',
		routeParams: {},
		agentDialog: {
			type: 'new',
			props: {
				open: false
			},
			data: null
		}
	}),
	reducers: {
		setAgentsSearchText: {
			reducer: (state, action) => {
				state.searchText = action.payload;
			},
			prepare: event => ({ payload: event.target.value || '' })
		},
		openNewAgentDialog: (state, action) => {
			state.agentDialog = {
				type: 'new',
				props: {
					open: true
				},
				data: null
			};
		},
		closeNewAgentDialog: (state, action) => {
			state.agentDialog = {
				type: 'new',
				props: {
					open: false
				},
				data: null
			};
		},
		openEditAgentDialog: (state, action) => {
			state.agentDialog = {
				type: 'edit',
				props: {
					open: true
				},
				data: action.payload
			};
		},
		closeEditAgentDialog: (state, action) => {
			state.agentDialog = {
				type: 'edit',
				props: {
					open: false
				},
				data: null
			};
		}
	},
	extraReducers: {
		[updateAgent.fulfilled]: agentsAdapter.upsertOne,
		[addAgent.fulfilled]: agentsAdapter.addOne,
		[removeAgents.fulfilled]: (state, action) => agentsAdapter.removeMany(state, action.payload),
		[removeAgent.fulfilled]: (state, action) => agentsAdapter.removeOne(state, action.payload),
		[getAgents.fulfilled]: (state, action) => {


			
			const { data, routeParams } = action.payload;
			agentsAdapter.setAll(state, data);
			state.routeParams = routeParams;
			state.searchText = '';
		}
	}
});

export const {
	setAgentsSearchText,
	openNewAgentDialog,
	closeNewAgentDialog,
	openEditAgentDialog,
	closeEditAgentDialog
} = agentsSlice.actions;

export default agentsSlice.reducer;
