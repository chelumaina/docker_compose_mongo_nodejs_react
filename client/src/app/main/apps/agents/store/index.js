import { combineReducers } from '@reduxjs/toolkit';
import agents from './agentsSlice';
import user from './userSlice';

const reducer = combineReducers({
	agents,
	user
});

export default reducer;
