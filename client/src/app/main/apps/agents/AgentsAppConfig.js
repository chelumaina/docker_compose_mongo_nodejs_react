import React from 'react';
import { Redirect } from 'react-router-dom';

const AgentsAppConfig = {
	settings: {
		layout: {
			config: {}
		}
	},
	routes: [
		{
			path: '/apps/agents/:id',
			component: React.lazy(() => import('./AgentsApp'))
		},
		{
			path: '/apps/agents',
			component: () => <Redirect to="/apps/agents/all" />
		}
	]
};

export default AgentsAppConfig;
