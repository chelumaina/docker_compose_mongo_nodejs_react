import axios from "axios";
import jwtService from "../app/services/jwtService/jwtService";

export const requestOptions = {
  method: "POST",
  headers: {
    accept: "application/json",
    "Content-Type": "application/json",
    "x-access-token": jwtService.getAccessToken(),
  },
  body: JSON.stringify({}),
};

export const headers = {
  headers: {
    "Content-Type": "application/json",
    "x-access-token": jwtService.getAccessToken(),
  },
};

export const getPositions = async () => {
  const response = await axios.get(`${process.env.REACT_APP_API_URL}/api/v1/position?pageNo=1&size=100`,headers);
  const c = response.data.message.map((d) => ({ id: d._id, name: d.name }));
  return c;
};



/*
* Get All Counties 
*/
export const getCounties = async () => {
  const response = await axios.get(
    `${process.env.REACT_APP_API_URL}/api/v1/county?pageNo=1&size=50`,
    headers
  );
  const c = response.data.message.map((d) => ({ id: d._id, name: d.county_name, county_code: d.county_code, voter_registered: d.voter_registered, total_polling_stations: d.total_polling_stations }));
  return c.sort((a, b) =>
    a.name.toLowerCase() > b.name.toLowerCase() ? 1 : -1
  );
};
/*
* Get one Counties 
*/
export const getOneCounties = async (county_id) => {
  const response = await axios.get(`${process.env.REACT_APP_API_URL}/api/v1/county/${county_id}?pageNo=1&size=100`,headers);
  return response.data.message;
};


/*
* Save County
*/
export const createCounty = async (county) => {
  const response = await axios.post(`${process.env.REACT_APP_API_URL}/api/v1/county?pageNo=1&size=100`,county,headers);
  return response.data.message;
};



/*
* Get All Constituency within a County 
*/
export const getCountyContituencies = async (county_id) => {
  const response = await axios.get(`${process.env.REACT_APP_API_URL}/api/v1/constituency/get_county_constituencies/${county_id}?pageNo=1&size=100`,headers);
  const c = response.data.message.map((d) => ({id: d._id,name: d.constituency_name,constituency_code: d.constituency_code, voter_registered: d.voter_registered, total_polling_stations: d.total_polling_stations }));
  return c.sort((a, b) =>a.name.toLowerCase() > b.name.toLowerCase() ? 1 : -1);
};

/*
* Get one Constituency 
*/
export const getOneConstituency = async (constituency_id) => {
  const response = await axios.get(`${process.env.REACT_APP_API_URL}/api/v1/constituency/${constituency_id}?pageNo=1&size=100`,headers);
  return response.data.message;
};


/*
* Get All Wards within a constituency 
*/
export const getConstituencyWards = async (constituency_id) => {
  const response = await axios.get(
    `${process.env.REACT_APP_API_URL}/api/v1/wards/constituencyWards/${constituency_id}?pageNo=1&size=100`,
    headers
  );
  const c = response.data.message.map((d) => ({
    id: d._id,
    name:  d.ward_name,
    ward_code: d.ward_code, voter_registered: d.voter_registered, total_polling_stations: d.total_polling_stations
  }));
  return c.sort((a, b) =>
    a.name.toLowerCase() > b.name.toLowerCase() ? 1 : -1
  );
};


/*
* Get one Ward 
*/
export const getOneWard = async (ward_id) => {
  const response = await axios.get(`${process.env.REACT_APP_API_URL}/api/v1/wards/${ward_id}?pageNo=1&size=100`,headers);
  return response.data.message;
};



/*
* Get All Stations within a Ward 
*/
export const getWardPollingStations = async (ward_id) => {
  const response = await axios.get(`${process.env.REACT_APP_API_URL}/api/v1/pollingStation/ward_stations/${ward_id}?pageNo=1&size=100`,headers);
  const c = response.data.message.map((d) => ({
    id: d._id,
    polling_station_code: d.polling_station_code,
    polling_station_registered_voters: d.polling_station_registered_voters,
    name: d.polling_station_name
  }));
  return c.sort((a, b) =>
    a.name.toLowerCase() > b.name.toLowerCase() ? 1 : -1
  );
};


/*
* Get one Ward 
*/
export const getOnePollingStation = async (polling_station_id) => {
  const response = await axios.get(`${process.env.REACT_APP_API_URL}/api/v1/pollingStation/${polling_station_id}?pageNo=1&size=100`,headers);
  return response.data.message;
};




/*
* Get All Candidates 
*/
export const getCandidates = async () => {
  return await axios.get(`${process.env.REACT_APP_API_URL}/api/v1/candidate?pageNo=1&size=100`,headers);
};

export const getAllCandidates = async () => {
  const response= await axios.get(`${process.env.REACT_APP_API_URL}/api/v1/candidate?pageNo=1&size=100`,headers);
  const da=await response.data.message;
  
  const c = da.map((d) =>  ({id: d._id,name: d.candidate_name,candidate_name: d.candidate_name,constituency_code: d.constituency_code,county_code: d.county_code,party_name: d.party_name,position_name: d.position_name}));
  
  const res=c.sort((a, b) =>
    a.candidate_name.toLowerCase() > b.candidate_name.toLowerCase() ? 1 : -1
  );

  return res;
};






/*
* Get one Ward 
*/
export const getOneCandidate = async (candidate_id) => {
  const response = await axios.get(`${process.env.REACT_APP_API_URL}/api/v1/candidate/${candidate_id}?pageNo=1&size=100`,headers);
  const c = response.map((d) => ({
    id: d._id,
    candidate_name: d.candidate_name,
    constituency_code: d.constituency_code,
    county_code: d.county_code,
    party_name: d.party_name,
    position_name: d.position_name
  }));
  return c.sort((a, b) =>
    a.candidate_name.toLowerCase() > b.candidate_name.toLowerCase() ? 1 : -1
  );
};




/*
* Get All users 
*/
export const getUsers = async () => {
  // return await axios.get(`${process.env.REACT_APP_API_URL}/api/v1/user?pageNo=1&size=100`,headers);
  const response = await axios.get(`${process.env.REACT_APP_API_URL}/api/v1/user?pageNo=1&size=100?pageNo=1&size=100`,headers);
  console.log(response.data.message)
  return response.data.message;
};


/*
* Get one User 
*/
export const getOneUser = async (user_id) => {
  const response = await axios.get(`${process.env.REACT_APP_API_URL}/api/v1/user/${user_id}?pageNo=1&size=100`,headers);
  return response.data.message;
};


export const process_results = async (position, county, constituencies, wards, PollingStation) => {
  const response = await axios.get(`${process.env.REACT_APP_API_URL}/api/v1/votes?pageNo=1&size=100`,headers);
  const c = response.data.message.map((d) =>  ({
          constituency_id: d.constituency_id,
          county_id:  d.county_id,
          form_name: d.form_name,
          polling_station_code:  d.polling_station_code,
          polling_station_id:  d.polling_station_id,
          polling_station_name:  d.polling_station_name,
          position_id: d.position_id,
          position_name: d.position_name,
          results:  d.results,
          ward_id: d.ward_id,
          id:  d._id
        }));
  return c;
};


// module.exports = [getCountiesData, getCounties];



export const getAllAgents = async () => {
  const response= await axios.get(`${process.env.REACT_APP_API_URL}/api/v1/user?pageNo=1&size=100`,headers);
  const da=await response.data.message;
  
  const c = da.map((d) =>  ({
    id: d._id,
    username: d.username,
    email: d.email,
    name: d.email,
    role: d.role[0].name,
    party_name: d.data.displayName,
    avatar: d.data.photoURL,
    county_name: d.county_id[0].county_name,
    constituency_name: d.constituency_id[0].constituency_name,
    ward_name: d.ward_id[0].ward_name,
    polling_station_id: d.polling_station_id[0].polling_station_name
  }));
  
  const res=c.sort((a, b) =>
    a.party_name.toLowerCase() > b.party_name.toLowerCase() ? 1 : -1
  );

  return res;
};
