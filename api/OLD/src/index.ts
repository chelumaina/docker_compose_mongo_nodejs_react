import dotenv from 'dotenv'
import express from 'express'
import mongoose from 'mongoose'
import cors from 'cors'
import morgan from 'morgan'
import routes from './routes'
// Redis
import RedisClient from './lib/redis'

dotenv.config()
const app = express()

// middlware
app.use(cors())
app.use(morgan('dev'))
app.use(express.json())
app.use(express.urlencoded()) 
// // Datatbase
const URI = "mongodb://root:root@mongo/restAPI";//process.env.MONGODB_URL; 


//Set up default mongoose connection
var mongoDB = 'mongodb://mongodb/restAPI';
mongoose.connect(mongoDB);

//Get the default connection
var db = mongoose.connection;

//Bind connection to error event (to get notification of connection errors)
db.on('error', console.error.bind(console, 'MongoDB connection error:'));


 
// mongoose.connect(URI, {
//   autoIndex: false
// }, (err) => {
//   if(err) throw err;
// })




// Routes
app.get('/', async (req, res) => {
  // const value = await RedisClient.get('key');
  res.json({"TEstETST":"Test T"})
})

app.use('/api', routes)



// Start server listening
const port = 5000;

app.listen(port, () => {
  console.log(`Express is listening on port ${port}`)
})