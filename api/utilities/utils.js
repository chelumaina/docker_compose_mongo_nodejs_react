
var mysql = require('mysql');
const { faker } = require("@faker-js/faker");

const db = require("../models");
const array = require("joi/lib/types/array");
const PoliticalParty = require('../models/politicalParty.model');
const Role = db.role;
const Position = db.position;

const County = db.county;
const Constituency = db.constituency;
const Ward = db.ward;
const Stations = db.pollingStation;
const Candidate = db.candidate;
const Vote = db.vote;
var json = [];
var i = 0,j = 0,k = 0;


const mysql_db_con = mysql.createConnection({
  host: process.env.DB_HOST,
  user: process.env.DB_USER,
  password: process.env.DB_PASSWORD,
  database: process.env.DB_NAME,
  port: process.env.DB_PORT
});

const mysqlUpdateCounties = async (con, mongo_id, code) => {
  var sql = `UPDATE station SET mongo_county_id = '${mongo_id}' WHERE COUNTY_CODE ='${code}'`;
  await con.query(sql, function (err, result) {
    if (err) throw err;
  });
};

const mysqlUpdateConstituencies = async (con, mongo_id, code) => {
  var sql = `UPDATE station SET mongo_constituency_id = '${mongo_id}' WHERE CONSTITUENCY_CODE ='${code}'`;
  await con.query(sql, function (err, result) {
    if (err) throw err; 
  });
};

const mysqlUpdateWards = async(con, mongo_id, code) => {
  var sql = `UPDATE station SET mongo_ward_id = '${mongo_id}' WHERE CAW_CODE ='${code}'`;
  await con.query(sql, function (err, result) {
    if (err) throw err;
  });
};

const mysqlUpdatePollingStations = async (con, mongo_id, code) => {
  var sql = `UPDATE station SET mongo_polling_station_id = '${mongo_id}' WHERE POLLING_STATION_CODE ='${code}'`;
  await con.query(sql, function (err, result) {
    if (err) throw err; 
  });
};


const create_wards_mongo = async (con,county,constituency, ward, mongo_county_id, mongo_constituency_id) => {
  //if(constituency.mongo_county_id)
  {
    const d = {
      ward_code: ward.CAW_CODE,
      ward_name: ward.CAW_NAME,
      county_id: mongo_county_id,
      constituency_id: mongo_constituency_id,
    };

    const d2 = {
      ward_code: ward.CAW_CODE,
      ward_name: ward.CAW_NAME,
    };

    await Ward.findOne(d2).then(async (res, err) => {
      if (err) throw err;
      else if (res) {
        await mysqlUpdateWards(con, res._id, ward.CAW_CODE);
        await process_polling_station(con,ward,constituency,county,mongo_county_id,mongo_constituency_id,res._id).then((err, res) => {});
        } else {
        await new Ward(d).save().then(async (res1, err) => {
          await mysqlUpdateWards(con, res1._id, ward.CAW_CODE);
          await process_polling_station(con,ward,constituency,county,mongo_county_id,mongo_constituency_id,res1._id).then((err, res) => {});
        });
      }
    });
  }
};

 
const create_county_mongo = async (mysql_db_con, county) => {
  const d = {
    county_code: county.COUNTY_CODE,
    county_name: county.COUNTY_NAME,
  };

  const d2 = {
    county_code: county.COUNTY_CODE,
  };

  await County.findOne(d2).then(async(res, err)=>{
 
    if (err) throw err;
      
    else if (res) 
    {
      await mysqlUpdateCounties(mysql_db_con, res._id, county.COUNTY_CODE);
      await get_constituency(mysql_db_con, county, county.COUNTY_CODE, res._id);   
    } 
    else 
    {
      const res=await new County(d).save().then(async (res1, err) => {
        await mysqlUpdateCounties(mysql_db_con, res1._id, county.COUNTY_CODE);
        await get_constituency(mysql_db_con, county, county.COUNTY_CODE, res1._id);
      });
    }
  });
  
};

const process_candidate=async (pos, party)=>
{ 

  let json={};

  switch(pos.name) {
    case "President":
        json={
          candidate_name:faker.name.lastName()+" "+faker.name.firstName(),//pos.name+" - "+party.party_name,
          county_id: null,
          constituency_id: null,
          ward_id: null,
          polling_station_id:null ,
          position_id:pos.id ,
          party_id: party._id,
          img:faker.image.image(),
        };
      break;   
    case "Governor": case "Senator": case "Woman Rep":
      const county=await County.find().limit(1).skip(Math.floor(Math.random()*49))
       json={
        candidate_name:faker.name.lastName()+" "+faker.name.firstName(),//pos.name+" - "+party.party_name,
        county_id: county[0]._id,
        constituency_id: null,
        ward_id: null,
        polling_station_id:null ,
        position_id:pos.id ,
        party_id: party._id,
        img:faker.image.image(),
      };
      break;   
    case "Member of Nationa Assembly":
      const constituency=await Constituency.find().limit(1).skip(Math.floor(Math.random()*49))
       json={
        candidate_name:faker.name.lastName()+" "+faker.name.firstName(),//pos.name+" - "+party.party_name,
        county_id: constituency[0].county_id,
        constituency_id: constituency[0]._id,
        ward_id: null,
        polling_station_id:null ,
        position_id:pos.id ,
        party_id: party._id,
        img:faker.image.image(),
      };
      break;
    case "Member County Assembly":
      const ward=await Ward.find().limit(1).skip(Math.floor(Math.random()*49))
       json={
        candidate_name:faker.name.lastName()+" "+faker.name.firstName(),//pos.name+" - "+party.party_name,
        county_id: ward[0].county_id,
        constituency_id: ward[0].constituency_id,
        ward_id: ward[0]._id,
        polling_station_id:null ,
        position_id:pos.id ,
        party_id: party._id,
        img:faker.image.image(),
      };
      break;   
    default:
      json={
        candidate_name:faker.name.lastName()+" "+faker.name.firstName(),//pos.name+" - "+party.party_name,
        county_id: null,
        constituency_id: null,
        ward_id: null,
        polling_station_id:null ,
        position_id:pos.id ,
        party_id: party._id,
        img:faker.image.image(),
      };
      break;   
  }
  return json;
}
const create_dummy_candidate = async () => {

  await PoliticalParty.find().then(async(res, err)=>{
    await res.map(async (party)=>{

const positions=[
  {"id":"62972264f068dd489e054550", "name":"President" },
  {"id":"62972264f068dd489e054552", "name":"Governor" },
  {"id":"62972264f068dd489e054554", "name":"Senator" },
  {"id":"62972264f068dd489e054556", "name":"Woman Rep" },
  {"id":"62972264f068dd489e054558", "name":"Member of Nationa Assembly" },
  {"id":"62972264f068dd489e05455a", "name":"Member County Assembly" }, 
]
   await positions.forEach(async (pos)=>{

    const json=await process_candidate(pos, party);
    // await new Stations(d).save().then(async (res1, err)
    await new Candidate(json).save().then(async(res, err)=>{});
   })
   
    })
  });


  // const d = {
  //   county_code: county.COUNTY_CODE,
  //   county_name: county.COUNTY_NAME,
  // };

  // const d2 = {
  //   county_code: county.COUNTY_CODE,
  // };

  // await County.findOne(d2).then(async(res, err)=>{
 
  //   if (err) throw err;
      
  //   else if (res) 
  //   {
  //     await mysqlUpdateCounties(mysql_db_con, res._id, county.COUNTY_CODE);
  //     await get_constituency(mysql_db_con, county, county.COUNTY_CODE, res._id);   
  //   } 
  //   else 
  //   {
  //     const res=await new County(d).save().then(async (res1, err) => {
  //       await mysqlUpdateCounties(mysql_db_con, res1._id, county.COUNTY_CODE);
  //       await get_constituency(mysql_db_con, county, county.COUNTY_CODE, res1._id);
  //     });
  //   }
  // });
  
};

const filter=async (r)=>{
      const str=await r.map((c)=> ( {votes:Math.floor(Math.random()*100),candidate_uuid:c._id,candidate_name:c.candidate_name }));
      return str;
}

const process_result=async (pos, polling_station)=>
{ 

  let json={};

  switch(pos.name) {
    case "President":
        await Candidate.find({position_id:pos.id}).then(async (res, err)=>{
        
        // let r1= await res.map((c)=> ( {votes:Math.floor(Math.random()),candidate_uuid:c._id }));
        json= {
          form_name: pos.form_name+"-"+pos.name,
          county_id: polling_station.county_id,
          constituency_id: polling_station.constituency_id,
          ward_id: polling_station.ward_id,
          polling_station_id: polling_station._id,
          polling_station_name: polling_station.polling_station_name,
          polling_station_code: polling_station.polling_station_code,
          position_id: pos.id,
          position_name: pos.name,
          results: await filter(res),
          img:faker.image.image(),
          vote_cast:Math.floor(Math.random()*100),
          spoiled_votes:Math.floor(Math.random()*100),
        };
      });
      break;   
    case "Governor": case "Senator": case "Woman Rep":
      const candidate2=await Candidate.find({position_id:pos.id, county_id:polling_station.county_id}).then(async (res, err)=>{
      // const r2=await res.map(async (c)=>({votes: Math.floor(Math.random()),candidate_uuid:c._id }));
        
      json={
        form_name: pos.form_name+"-"+pos.name,
          county_id: polling_station.county_id,
          constituency_id: polling_station.constituency_id,
          ward_id: polling_station.ward_id,
          polling_station_id: polling_station._id,
          polling_station_name: polling_station.polling_station_name,
          polling_station_code: polling_station.polling_station_code,
          position_id: pos.id,
          position_name: pos.name,
          results: await filter(res),
          img:faker.image.image()   
        };


      });
      break;   
    case "Member of Nationa Assembly":
      const candidate3=await Candidate.find({position_id:pos.id, constituency_id:polling_station.constituency_id}).then(async (res, err)=>{

      // const r3=await res.map(async (c)=>( {votes:Math.floor(Math.random()),candidate_uuid:c._id }));
        
      json={
        form_name: pos.form_name+"-"+pos.name,
          county_id: polling_station.county_id,
          constituency_id: polling_station.constituency_id,
          ward_id: polling_station.ward_id,
          polling_station_id: polling_station._id,
          polling_station_name: polling_station.polling_station_name,
          polling_station_code: polling_station.polling_station_code,
          position_id: pos.id,
          position_name: pos.name,
          results:await filter(res),
          img:faker.image.image()   
        };
        
      });
      break;
    case "Member County Assembly":
      const candidate4= await Candidate.find({position_id:pos.id, ward_id:polling_station.ward_id}).then(async (res, err)=>{

      // const r4=await res.map(async (c)=>({votes:Math.floor(Math.random()),candidate_uuid:c._id }));
        
      json={
        form_name: pos.form_name+"-"+pos.name,
          county_id: polling_station.county_id,
          constituency_id: polling_station.constituency_id,
          ward_id: polling_station.ward_id,
          polling_station_id: polling_station._id,
          polling_station_name: polling_station.polling_station_name,
          polling_station_code: polling_station.polling_station_code,
          position_id: pos.id,
          position_name: pos.name,
          results:await filter(res),
          img:faker.image.image()   
        };
  
      })
      break;  
    default:
      const candidate5= await Candidate.find({position_id:pos.id, ward_id:polling_station.ward_id}).then(async (res, err)=>{

      // let r5=await res.map(async (c)=>({votes:Math.floor(Math.random()),candidate_uuid:c._id }));
      json={
        form_name: pos.form_name+"-"+pos.name,
          county_id: polling_station.county_id,
          constituency_id: polling_station.constituency_id,
          ward_id: polling_station.ward_id,
          polling_station_id: polling_station._id,
          polling_station_name: polling_station.polling_station_name,
          polling_station_code: polling_station.polling_station_code,
          position_id: pos.id,
          position_name: pos.name,
          results:await filter(res),
          img:faker.image.image()   
        };
      
      });
      break;   
  }
  return json;
}
const create_dummy_votes = async () => {

  const positions=[
    {"id":"62972264f068dd489e054550", "name":"President", "form_name":"Form 30" },
    {"id":"62972264f068dd489e054552", "name":"Governor", "form_name":"Form 31" },
    {"id":"62972264f068dd489e054554", "name":"Senator", "form_name":"Form 32" },
    {"id":"62972264f068dd489e054556", "name":"Woman Rep", "form_name":"Form 33" },
    {"id":"62972264f068dd489e054558", "name":"Member of Nationa Assembly", "form_name":"Form 34" },
    {"id":"62972264f068dd489e05455a", "name":"Member County Assembly", "form_name":"Form 35" }, 
  ];

 
     await positions.forEach(async (pos)=>{
       
        const poll=await Stations.find({county_id:'6296fe598f6d2020fa867f20'});
        poll.map(async (p)=>{
          const v= await process_result(pos, p)
          await new Vote(v).save().then(async (res1, err) => {});
        });
        
     })

  
};


const create_constituency_mongo = async (mysql_db_con, county, constituency, county_mongo_id) => {
  const d = {
    constituency_code: constituency.CONSTITUENCY_CODE,
    constituency_name: constituency.CONSTITUENCY_NAME,
    county_id: county_mongo_id,
  };
  
  const d2 = {
    constituency_code: constituency.CONSTITUENCY_CODE,
  };
  let c=await Constituency.findOne(d2).then(async (res, err) => {

      if (err) throw err;
      else if (res) {
        await mysqlUpdateConstituencies(mysql_db_con, res._id, constituency.CONSTITUENCY_CODE);
          await process_wards(mysql_db_con,county,constituency,county_mongo_id,res._id).then((res, err) => {});
      }
      else {
        
        await new Constituency(d).save().then(async (res1, err) => {
          await mysqlUpdateConstituencies(mysql_db_con, res1._id, constituency.CONSTITUENCY_CODE);
          await process_wards(mysql_db_con,county,constituency,county_mongo_id,res1._id).then((res, err) => {});
        });
      }

  });
  // if (!c) 
  // {
  //   c= await new Constituency(d2).save();
  // }
 
  // await Constituency.findOneAndUpdate(d2, d, {new:true}, async (res, err) => {
  //   if (err) throw err;
  //   else if (res) {
  //     // await mysqlUpdateConstituencies(mysql_db_con, res._id, constituency.CONSTITUENCY_CODE);
  //     // await process_wards(mysql_db_con,county,constituency,county_mongo_id,res._id).then((res, err) => {});
  //   } else {

  //     await new Constituency(d2).save().then(async (res1, err) => {
  //         // await mysqlUpdateConstituencies(mysql_db_con, county_mongo_id, res._id, constituency.CONSTITUENCY_CODEE);
  //         // await process_wards(mysql_db_con,county,constituency,county_mongo_id,res1._id).then((err, res) => {});
  //       });
  //   }
  // });
};


const create_station_mongo = async (mysql_db_con, station,ward,constituency,county,county_mongo_id,constituency_mongo_id,ward_mongo_id) => {
  const d = {
    polling_station_code: station.POLLING_STATION_CODE,
    polling_station_name: station.POLLING_STATION_NAME,
    polling_station_registered_voters: (station.VOTERS_PER_POLLING_STATION).replace(/(\r\n|\n|\r)/gm,""),
    county_id: county_mongo_id,
    constituency_id: constituency_mongo_id,
    ward_id:ward_mongo_id,
  };

  const d2 = { 
    polling_station_code: station.POLLING_STATION_CODE,
    polling_station_name: station.POLLING_STATION_NAME
   };
 
   
  await Stations.findOne(d2).then( async (res, err) => {
    
    if (err) throw err; 
    else if (res) {
      await mysqlUpdatePollingStations(mysql_db_con, res._id, station.POLLING_STATION_CODE);
    } else {
      await new Stations(d).save().then(async (res1, err) => {
          await mysqlUpdatePollingStations(mysql_db_con, res1._id, station.POLLING_STATION_CODE);
        });
    }
  });
};



const get_constituency=async (mysql_db_con, county, county_code, county_mongo_id)=>{
  try
  {
    await mysql_db_con.query(`SELECT CONSTITUENCY_CODE, CONSTITUENCY_NAME, mongo_county_id, mongo_constituency_id FROM station WHERE COUNTY_CODE='${county_code}' and CONSTITUENCY_CODE is not null and CONSTITUENCY_NAME is not null GROUP BY  CONSTITUENCY_CODE, CONSTITUENCY_NAME ORDER BY CONSTITUENCY_CODE`, async function (err, rows, fields) {
      if (err) throw err; 
      return await rows.forEach( async(constituency) => { 
        
        var id =await create_constituency_mongo(mysql_db_con, county, constituency, county_mongo_id);
      }); 
    });
  }
  catch(err) {console.error("Connection error", err);}
}
 



const get_ward=async (mysql_db_con,county,constituency,constituency_code,county_code, county_mongo_id, constituency_mongo_id)=>{
  try{
  // mysql_db_con.query("SELECT COUNTY_CODE, COUNTY_NAME, mongo_county_id FROM station GROUP BY  COUNTY_CODE, COUNTY_NAME ORDER BY COUNTY_CODE", async function (err, rows, fields) {
  await mysql_db_con.query(`SELECT CAW_CODE, CAW_NAME, mongo_county_id, mongo_constituency_id, mongo_ward_id FROM station WHERE CONSTITUENCY_CODE='${constituency_code}' and CAW_CODE is not null and CAW_NAME is not null GROUP BY  CAW_CODE, CAW_NAME ORDER BY CAW_CODE`, async function (err, rows, fields) {
    if (err) throw err; 

    return await rows.forEach( async(wards) => { 

      var id =await create_wards_mongo(mysql_db_con,county,constituency, wards, county_mongo_id, constituency_mongo_id);

  }); 
  });
}
catch(err) {
  console.error("Connection error", err); 
}
} 



const get_polling_station_ward=async (mysql_db_con, ward,constituency,county,caw_code, county_mongo_id, constituency_mongo_id, ward_mongo_id)=>{
  try{
  // mysql_db_con.query("SELECT COUNTY_CODE, COUNTY_NAME, mongo_county_id FROM station GROUP BY  COUNTY_CODE, COUNTY_NAME ORDER BY COUNTY_CODE", async function (err, rows, fields) {
  await mysql_db_con.query(`SELECT POLLING_STATION_CODE, POLLING_STATION_NAME, VOTERS_PER_POLLING_STATION, mongo_county_id, mongo_constituency_id, mongo_ward_id FROM station WHERE CAW_CODE='${caw_code}' and POLLING_STATION_CODE is not null and POLLING_STATION_NAME is not null GROUP BY  POLLING_STATION_CODE, POLLING_STATION_NAME ORDER BY POLLING_STATION_CODE`, async function (err, rows, fields) {
    if (err) throw err; 

    return await rows.forEach( async(polling_station) => { 

      var id=await create_station_mongo(mysql_db_con, polling_station,ward,constituency,county, county_mongo_id,constituency_mongo_id,ward_mongo_id);
  }); 
  });
}
catch(err) {
  console.error("Connection error", err); 
}
}
 
 

const process_wards = async (mysql_db_con,county,constituency, county_mongo_id,constituency_mongo_id) => {
  j++;
  let wards = await get_ward(mysql_db_con,county,constituency, constituency.CONSTITUENCY_CODE,county.COUNTY_CODE, county_mongo_id,constituency_mongo_id);
};

const process_polling_station = async (mysql_db_con,ward,constituency,county,county_mongo_id,constituency_mongo_id,ward_mongo_id) => {
   let polling_stations = await get_polling_station_ward(mysql_db_con, ward,constituency,county,ward.CAW_CODE, county_mongo_id,constituency_mongo_id,ward_mongo_id);
};




const initial = async () => {
  await Role.estimatedDocumentCount().then( async (res, err) => {

    if (err) throw err;
    else if (res <= 0 ) {
      await new Role({ name: "user" }).save().then((err) => {
        if (err) {
        }
      });
      await new Role({ name: "moderator" }).save().then((err) => {
        if (err) {
        }
      });
      await new Role({ name: "admin" }).save().then((err) => {
        if (err) {
        }
      });
    }
  });



  await Position.estimatedDocumentCount().then(async (res, err) => {
   
    if (err) throw err;
    else if (res <= 0 ) {
      await new Position({
        name: "President",
      }).save().then((err) => {
        if (err) {
        }
      });
      await new Position({
        name: "Governor",
      }).save().then((err) => {
        if (err) {
        }
      });
      await new Position({
        name: "Senator",
      }).save().then((err) => {
        if (err) {
          
        }
        
      });

      await new Position({
        name: "Woman Rep",
      }).save().then((err) => {
        if (err) {
          
        }
        
      });

      await new Position({
        name: "Member of Nationa Assembly",
      }).save().then((err) => {
        if (err) {
          
        }
        
      });

      await new Position({
        name: "Member County Assembly",
      }).save().then((err) => {
        if (err) {
          
        }
        
      });
    }
  });

  await County.estimatedDocumentCount().then(async (res, err) => {

    if (err) throw err;
    else if (res <= 0 ) {
      await create_county();
    }
  });


  await Candidate.estimatedDocumentCount().then(async (res, err) => {

    if (err) throw err;
    else if (res <= 0 ) {
      await create_dummy_candidate();
    }
  });


  await Vote.estimatedDocumentCount().then(async (res, err) => {

    if (err) throw err;
    else if (res <= 0 ) {
      await create_dummy_votes();
    }
  });
};





const shuffle = (array) => {
  return array.sort(() => Math.random() - 0.5);
};

const create_county=async ()=>{
  try{
  await mysql_db_con.query("SELECT COUNTY_CODE, COUNTY_NAME, mongo_county_id FROM station WHERE COUNTY_CODE is not null AND  COUNTY_NAME is not null  GROUP BY  COUNTY_CODE, COUNTY_NAME ORDER BY COUNTY_CODE", async function (err, rows, fields) {
    if (err) throw err;
    return await rows.forEach( async(row) => {
        var id= await create_county_mongo(mysql_db_con, row);
    }); 
  });
}
catch(err) {
  console.error("Connection error", err); 
}
}


module.exports = {
  initial
};
