require("dotenv").config();
const cors = require("cors");
const express = require("express");
var bodyParser = require('body-parser'); 
// var mysql = require('mysql'); 

// const redoc = require('redoc-express');
// const swaggerJsDoc = require('swagger-jsdoc')
const swaggerUi = require('swagger-ui-express')
const swaggerDocument = require('./swagger.json');
 
const db = require("./models");
const {initial}= require('./utilities/utils')



console.log(`connect Mongos`);

const database = db.mongoose.connect(process.env.DB_CONNECT, {
    // Bug here on connect is not a function
    useNewUrlParser: true,
    useUnifiedTopology: true,
  })
  .then(() => {
    console.log("Connected to the MongoDB database!");
  })
  .then(() => {
    console.log("Successfully connect to MongoDB.");
    ///initial(); 
  })
  .catch(err => {
    console.error("Connection error", err);
    process.exit();
  })
  .catch((err) => {
    console.log("Cannot connect to the database!", err);
    process.exit();
  });

 
/*
  const mysql_db_con = mysql.createConnection({
    host: process.env.DB_HOST,
    user: process.env.DB_USER,
    password: process.env.DB_PASSWORD,
    database: process.env.DB_NAME,
    port: process.env.DB_PORT
  });

// const mysql_db = require("./models/mysql/dbConnect");
// mysql_db.sequelize.sync();
mysql_db_con.connect(async function(err) {
  if (err) throw err;

  const polling_station=async (CAW_CODE)=>{
    mysql_db_con.query(`SELECT POLLING_STATION_CODE, POLLING_STATION_NAME, VOTERS_PER_POLLING_STATION, mongo_county_id, mongo_constituency_id, mongo_ward_id, mongo_polling_station_id FROM station WHERE CAW_CODE=${CAW_CODE}  ORDER BY POLLING_STATION_CODE`, function (err, rows, fields) {
      if (err) throw err;
      rows.forEach( async (row) => {
        if(!row.mongo_polling_station_id)
          // var id= await create_stations_mongo(mysql_db_con, row);
      });
    });
  } 


  const wards=async (CONSTITUENCY_CODE)=>{
    mysql_db_con.query(`SELECT CAW_CODE, CAW_NAME, mongo_county_id, mongo_constituency_id, mongo_ward_id FROM station WHERE CONSTITUENCY_CODE=${CONSTITUENCY_CODE} GROUP BY CAW_CODE, CAW_NAME ORDER BY CAW_CODE`, function (err, rows, fields) {

      if (err) throw err;
      rows.forEach( async(row) => {

        if(!row.mongo_ward_id)
          var id= await create_wards_mongo(mysql_db_con, row);
        await polling_station(row.CAW_CODE);

      });
    });
  }

  const constituencies=async (COUNTY_CODE)=>{
    mysql_db_con.query(`SELECT CONSTITUENCY_CODE, CONSTITUENCY_NAME, mongo_county_id, mongo_constituency_id FROM station WHERE COUNTY_CODE=${COUNTY_CODE} GROUP BY CONSTITUENCY_CODE, CONSTITUENCY_NAME, mongo_county_id, mongo_constituency_id ORDER BY CONSTITUENCY_CODE`, function (err, rows, fields) {
    if (err) throw err;
      rows.forEach( async(row) => {
        if(!row.mongo_constituency_id)
          var id= create_constituencies_mongo(mysql_db_con, row);
        
        await wards(row.CONSTITUENCY_CODE);

      });
    });
  }


  
  const counties=async ()=>{
    try{
    mysql_db_con.query("SELECT COUNTY_CODE, COUNTY_NAME, mongo_county_id FROM station GROUP BY  COUNTY_CODE, COUNTY_NAME ORDER BY COUNTY_CODE", async function (err, rows, fields) {
      if (err) throw err;
      return await rows.forEach( async(row) => {
          var id= await create_county_mongo(mysql_db_con, row);
      }); 
    });
  }
  catch(err) {
    console.error("Connection error", err); 
  }
  }

  await counties();
});
*/

const app = express();
app.use(cors());

app.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header('Access-Control-Allow-Methods', 'DELETE, PUT, GET, POST');
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  next();
});

app.use(express.json());

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));



// simple route
app.get("/", (req, res) => {
  res.json({ message: "Welcome to bezkoder application." });
});

const {authRoute, userRoute,roleRoute, pollingStationRoute, candidateRoute, votesRoute, CountyRoute,ConstituencyRoute,WardsRoute,StationRoute,PositionRoute, PoliticalParty} = require("./routes");

// app.use('/', require('./routes/auth.routes'));
// app.use('/api/users', require('./routes/routes'))


app.use("/api/v1/auth/", authRoute);
app.use("/api/v1/user", userRoute);
app.use("/api/v1/polling_station", pollingStationRoute);
app.use("/api/v1/roles", roleRoute); 
app.use("/api/v1/candidate", candidateRoute); 
app.use("/api/v1/votes", votesRoute); 
app.use("/api/v1/county", CountyRoute); 
app.use("/api/v1/constituency", ConstituencyRoute); 
app.use("/api/v1/wards", WardsRoute); 
app.use("/api/v1/pollingStation", StationRoute);
app.use("/api/v1/position", PositionRoute);  
app.use("/api/v1/politicalParties", PoliticalParty);  


app.use(
  '/api-docs',
  swaggerUi.serve, 
  swaggerUi.setup(swaggerDocument)
);

// set port, listen for requests
const PORT = process.env.PORT || 3000;

app.listen(PORT, () => {
  console.log(`Server is running on port ${PORT}.`);
});

