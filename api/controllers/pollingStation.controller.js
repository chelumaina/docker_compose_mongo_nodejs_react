const db = require("../models");
var fs = require("fs");

// const User = db.user;
// const Role = db.role;
const PollingStation = db.pollingStation;

const { RegisterValidate, LoginValidate } = require("../validate");

// router.post("/getPollingStation", [authJwt.verifyToken, authJwt.isModerator], getPollingStation);
// router.post("/createPollingStation", [authJwt.verifyToken, authJwt.isModerator], createPollingStation);
// router.post("/updatePollingStation", [authJwt.verifyToken, authJwt.isModerator], updatePollingStation);
// router.post("/deletePollingStation", [authJwt.verifyToken, authJwt.isModerator], deletePollingStation);

const getPollingStation = async (req, res) => {
  try {

    var pageNo = parseInt(req.query.pageNo)
    var size = parseInt(req.query.size)
    var query = {}
    if(pageNo < 0 || pageNo === 0 || pageNo == null || isNaN(pageNo) ) {
      response = {"error" : true,"message" : "invalid page number, should start with 1"};
          return res.json(response)
    }

    size=isNaN(size)?10:size;
    query.skip = size * (pageNo - 1);
    query.limit = size;

    var filter={}

    PollingStation.countDocuments(filter,function(err,totalCount) {
      if(err) {
        response = {"error" : true,"message" : "Error fetching data"}
      }
 
      PollingStation.find(filter,{},query, async function(err,data) {
            // Mongo command to fetch all data from collection.
          if(err) {
              response = {"error" : true,"message" : "Error fetching data"};
          } else {

              var totalPages = Math.ceil(totalCount / size)
              var track = [];
               const c= await Promise.all(data.map(async (post, i) =>{ 
                  const r = await get_station_stats(post, i).then((res, err)=>{return res;}); 
                  track.push(r);
                }));
              response = {"pages": totalPages,"current_page": pageNo,"size": size, "error" : false,"message" : track};
          }
          res.json(response);
        });
    })

    // //Check if User email exist
    // const model = await PollingStation.find();
    // res.json(model);
  } catch (error) {
    res.status(400).json({ message: error.message });
  }
};


const getPollingStationOne = async (req, res) => {
  try {
    
    var pageNo = parseInt(req.query.pageNo)
    var size = parseInt(req.query.size)
    var query = {}
    if(pageNo < 0 || pageNo === 0 || pageNo == null || isNaN(pageNo) ) {
      response = {"error" : true,"message" : "invalid page number, should start with 1"};
          return res.json(response)
    }

    size=isNaN(size)?10:size;
    query.skip = size * (pageNo - 1);
    query.limit = size;

    var filter={polling_station_code:req.params.id}

    PollingStation.countDocuments(filter,function(err,totalCount) {
      if(err) {
        response = {"error" : true,"message" : "Error fetching data"}
      }
 
      PollingStation.find(filter,{},query, async function(err,data) {
            // Mongo command to fetch all data from collection.
          if(err) {
              response = {"error" : true,"message" : "Error fetching data"};
          } else {

              var totalPages = Math.ceil(totalCount / size)
              var track = [];
               const c= await Promise.all(data.map(async (post, i) =>{ 
                  const r = await get_station_stats(post, i).then((res, err)=>{return res;}); 
                  track.push(r);
                }));
              response = {"pages": totalPages,"current_page": pageNo,"size": size, "error" : false,"message" : track};
          }
          res.json(response);
        });
    })

    // //Check if User email exist
    // const model = await PollingStation.find({polling_station_code:req.params.id});
    // res.json(model);
  } catch (error) {
    res.status(400).json({ message: error.message });
  }
};


const getPollingStationCounty = async (req, res) => {
  try {


    var pageNo = parseInt(req.query.pageNo)
    var size = parseInt(req.query.size)
    var query = {}
    if(pageNo < 0 || pageNo === 0 || pageNo == null || isNaN(pageNo) ) {
      response = {"error" : true,"message" : "invalid page number, should start with 1"};
          return res.json(response)
    }

    size=isNaN(size)?10:size;
    query.skip = size * (pageNo - 1);
    query.limit = size;

    var filter={county_code:req.params.id}

    PollingStation.countDocuments(filter,function(err,totalCount) {
      if(err) {
        response = {"error" : true,"message" : "Error fetching data"}
      }
 
      PollingStation.find(filter,{},query, async function(err,data) {
            // Mongo command to fetch all data from collection.
          if(err) {
              response = {"error" : true,"message" : "Error fetching data"};
          } else {

              var totalPages = Math.ceil(totalCount / size)
              var track = [];
               const c= await Promise.all(data.map(async (post, i) =>{ 
                  const r = await get_station_stats(post, i).then((res, err)=>{return res;}); 
                  track.push(r);
                }));
              response = {"pages": totalPages,"current_page": pageNo,"size": size, "error" : false,"message" : track};
          }
          res.json(response);
        });
    })
    // //Check if User email exist
    // const model = await PollingStation.find({county_code:req.params.id});
    // res.json(model);
  } catch (error) {
    res.status(400).json({ message: error.message });
  }
}; 
const getPollingStationContituency = async (req, res) => {
  try {

    var pageNo = parseInt(req.query.pageNo)
    var size = parseInt(req.query.size)
    var query = {}
    if(pageNo < 0 || pageNo === 0 || pageNo == null || isNaN(pageNo) ) {
      response = {"error" : true,"message" : "invalid page number, should start with 1"};
          return res.json(response)
    }

    size=isNaN(size)?10:size;
    query.skip = size * (pageNo - 1);
    query.limit = size;

    var filter={constituency_code:req.params.id}

    PollingStation.countDocuments(filter,function(err,totalCount) {
      if(err) {
        response = {"error" : true,"message" : "Error fetching data"}
      }
 
      PollingStation.find(filter,{},query, async function(err,data) {
            // Mongo command to fetch all data from collection.
          if(err) {
              response = {"error" : true,"message" : "Error fetching data"};
          } else {

              var totalPages = Math.ceil(totalCount / size)
              var track = [];
               const c= await Promise.all(data.map(async (post, i) =>{ 
                  const r = await get_station_stats(post, i).then((res, err)=>{return res;}); 
                  track.push(r);
                }));
              response = {"pages": totalPages,"current_page": pageNo,"size": size, "error" : false,"message" : track};
          }
          res.json(response);
        });
    })

    //Check if User email exist 
    // const model = await PollingStation.find({constituency_code:req.params.id});

    // res.json(model);
  } catch (error) {
    res.status(400).json({ message: error.message });
  }
};
const getPollingStationWard = async (req, res) => {
  try {

    var pageNo = parseInt(req.query.pageNo)
    var size = parseInt(req.query.size)
    var query = {}
    if(pageNo < 0 || pageNo === 0 || pageNo == null || isNaN(pageNo) ) {
      response = {"error" : true,"message" : "invalid page number, should start with 1"};
          return res.json(response)
    }

    size=isNaN(size)?10:size;
    query.skip = size * (pageNo - 1);
    query.limit = size;

    var filter={ward_code:req.params.id}

    PollingStation.countDocuments(filter,function(err,totalCount) {
      if(err) {
        response = {"error" : true,"message" : "Error fetching data"}
      }
 
      PollingStation.find(filter,{},query, async function(err,data) {
            // Mongo command to fetch all data from collection.
          if(err) {
              response = {"error" : true,"message" : "Error fetching data"};
          } else {

              var totalPages = Math.ceil(totalCount / size)
              var track = [];
               const c= await Promise.all(data.map(async (post, i) =>{ 
                  const r = await get_station_stats(post, i).then((res, err)=>{return res;}); 
                  track.push(r);
                }));
              response = {"pages": totalPages,"current_page": pageNo,"size": size, "error" : false,"message" : track};
          }
          res.json(response);
        });
    })
    //Check if User email exist 
    // const model = await PollingStation.find({ward_code:req.params.id});

    // res.json(model);
  } catch (error) {
    res.status(400).json({ message: error.message });
  }
};



const createPollingStation = async (req, res) => {
  try{
    const data = new PollingStation({
      county_code: p.COUNTY_CODE,
      county_name: p.COUNTY_NAME,
      constituency_code: p.CONSTITUENCY_CODE,
      constituency_name: p.CONSTITUENCY_NAME,
      ward_code: p.CAW_CODE,
      ward_name: p.CAW_NAME,
      reg_center_code: p.REGISTRATION_CENTRE_CODE,
      reg_center_name: p.REGISTRATION_CENTRE_NAME,
      polling_station_code: p.POLLING_STATION_CODE,
      polling_station_name: p.POLLING_STATION_NAME,
      voter_per_center: p.VOTERS_PER_REGISTRATION_CENTRE,
      voter_per_polling_station: p.VOTERS_PER_POLLING_STATION,
    });
    const dataToSave = await data.save();
    res.json(dataToSave);
  } catch (error) {
    res.status(400).json({ message: error.message });
  }
};




const updatePollingStation = async (req, res) => {
  try { 

    const ps=findById(req.params.id)
    if(!ps) 
      res.status(400).json({ message: "Polling Station Not Found" }); 


    const data =  {
      county_code: req.body.county_code,
      county_name: req.body.county_name,
      constituency_code: req.body.constituency_code,
      constituency_name: req.body.constituency_name,
      ward_code: req.body.ward_code,
      ward_name: req.body.ward_name,
      reg_center_code: req.body.reg_center_code,
      reg_center_name: req.body.reg_center_name,
      polling_station_code: req.body.polling_station_code,
      polling_station_name: req.body.polling_station_name,
      voter_per_center: req.body.voter_per_center,
      voter_per_polling_station: req.body.voter_per_polling_station, 
    };

    const updateBranch = await PollingStation.findOneAndUpdate({ _id: req.params.id }, data); 
    res.status(200).json({ _id: req.params.id, message:"Successfully Updated" });
  } catch (error) {
    res.status(400).json({ message: error.message });
  }
};


const deletePollingStation = async (req, res) => {
  let _id = req.params.id; 
  try {

    const ps=findById(_id)
    if(!ps) 
      res.status(400).json({ message: "Polling Station Not Found" }); 

    await ps.delete();
    res.json({id:_id, message:"Deleted Successfully"});
  } catch (error) {
    res.status(400).json({ message: error.message });
  }
};

const get_counties = (data) => {
  return data.filter((value, index, self) =>index === self.findIndex((t) => t.COUNTY_CODE === value.COUNTY_CODE));
};

const get_constituency = (json, county_code) => {
  return json.filter((value, index, self) =>index === self.findIndex((t) => t.CONSTITUENCY_CODE===value.CONSTITUENCY_CODE && t.COUNTY_CODE===county_code));
  // return json.filter((value) => value.COUNTY_CODE === county_code);
};


const get_ward = (data, constituency_code, county_code) => {
  return data.filter((value, index, self) =>index === self.findIndex((t) => t.CAW_CODE===value.CAW_CODE && t.CONSTITUENCY_CODE===constituency_code));
  // return data.filter((value) => value.CONSTITUENCY_CODE == constituency_code && value.COUNTY_CODE == county_code);
};

const get_polling_station_ward = (data, ward_code, constituency_code, county_code) => {
  return data.filter((value, index, self) =>index === self.findIndex((t) => t.POLLING_STATION_CODE===value.POLLING_STATION_CODE && t.CAW_CODE===ward_code));
  // return data.filter((value) => value.CAW_CODE == ward_code  && value.CONSTITUENCY_CODE == constituency_code && value.COUNTY_CODE == county_code);

};





const process_constituencies=(json, county)=>{
  let constitituencies = get_constituency(json, county.COUNTY_CODE);
  let j=0;
  constitituencies.map(constitituency=>
  {
    j++

    process_wards(json, constitituency, county);     
  });
}


const process_wards=(json, constitituency, county)=>{
  
    let wards=  get_ward(json, constitituency.CONSTITUENCY_CODE, county.COUNTY_CODE);
    let k=0;
    wards.map(ward=>
    {
      k++
      process_polling_station(json, ward, constitituency, county);
    }); 
}



const process_polling_station=(json, ward, constitituency, county)=>{
      let polling_stations= get_polling_station_ward(json, ward.CAW_CODE, constitituency.CONSTITUENCY_CODE, county.COUNTY_CODE);
      let l=0;
      polling_stations.map(polling_station=>
      {
          l++;
      });
    };


const getIEBCData= async (req, res) => {
  fs.readFile("./data/iebc_data2.json", "utf8", async (err, jsonString) => {
    if (err) {
      return;
    }

    try {
      let json=JSON.parse(jsonString);
      let counties= await get_counties(json);
      let i=0;
        counties.map(county=>
        {
          i++;
          process_constituencies(json, county);
        })
      res.status(200).json(json);
    } catch (err) {
      res.status(400).json({ message: err.message });
    }
  });
};




const pollingStationController = {
  getPollingStation,
  getPollingStationOne,
  getPollingStationCounty, 
  getPollingStationContituency, 
  getPollingStationWard,
  createPollingStation,
  updatePollingStation,
  deletePollingStation,
  getIEBCData
  
};
module.exports = pollingStationController;
