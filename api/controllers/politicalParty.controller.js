const db = require("../models");
const PoliticalParty = db.politicalParty;

const fs = require("fs");
const { RegisterValidate } = require("../validate");


const updatePoliticalParty = async (req, res) => {
  try {
    //Check if User email exist
    const user = await PoliticalParty.findById(req.params.id);
    if (!user) return res.status(400).send({ message: "User Not Found" });

    const data = {
      party_name: req.body.party_name,
      color: req.body.color,
      logo: req.body.logo,
    };

    await PoliticalParty.findOneAndUpdate({ _id: req.params.id }, data);
    res.status(200).json({ _id: req.params.id, message: "Üpdate Successful" });
  } catch (error) {
    res.status(400).json({ message: error.message });
  }
};
 

const getPoliticalParty = async (req, res) => {
  try {


    var pageNo = parseInt(req.query.pageNo)
    var size = parseInt(req.query.size)
    var query = {}
    if(pageNo < 0 || pageNo === 0 || pageNo == null || isNaN(pageNo) ) {
      response = {"error" : true,"message" : "invalid page number, should start with 1"};
          return res.json(response)
    }

    size=isNaN(size)?10:size;
    query.skip = size * (pageNo - 1);
    query.limit = size;

    var filter={}

    PoliticalParty.countDocuments(filter,function(err,totalCount) {
      if(err) {
        response = {"error" : true,"message" : "Error fetching data"}
      }
 
      PoliticalParty.find(filter,{},query, async function(err,data) {
            // Mongo command to fetch all data from collection.
          if(err) {
              response = {"error" : true,"message" : "Error fetching data"};
          } else {

              var totalPages = Math.ceil(totalCount / size)
              var track = [];
               const c= await Promise.all(data.map(async (post, i) =>{ 
                  const r = await get_station_stats(post, i).then((res, err)=>{return res;}); 
                  track.push(r);
                }));
              response = {"pages": totalPages,"current_page": pageNo,"size": size, "error" : false,"message" : track};
          }
          res.json(response);
        });
    })
    // //Check if User email exist
    // const votes = await PoliticalParty.find();
    // res.status(200).json(votes);
  } catch (error) {
    res.status(400).json({ message: error.message });
  }
};

const deletePoliticalParty = async (req, res) => {
  // const { error } = RegisterValidate(req.body);
  // if (error) {
  //   return res.status(400).send(error.details.map((msg) => msg));
  // }

  let _id = req.params.id;
  try {
    const updateBranch = await PoliticalParty.deleteOne({ _id: _id });
    res.status(200).json(updateBranch);
  } catch (error) {
    res.status(400).json({ message: error.message });
  }
};
 
const createPoliticalParty = async (req, res, next) => {
  var d={
    party_name: req.body.party_name,
    color: req.body.color,
    logo: req.body.logo,
  };
  var image = new PoliticalParty(d);  
  await image.save(function (err) {
    if (err) {
      return next(err);
    }
    res.status(200).json({ message: "OK" });
  });
};

const getOnePoliticalParty = async (req, res) => {
  try {
    //Check if User email exist
    const model = await PoliticalParty.findById(req.params.id);
    if (model) {
      res.status(200).json(model);

    }
    else
    {
      response = {"error" : false, "data" : [], "message" : "Not Found"};
      res.status(200).json(response); 
    } 

    } catch (error) {
    res.status(400).json({ message: error.message });
  }
};

const votesController = {
  updatePoliticalParty,
  getPoliticalParty,
  getOnePoliticalParty,
  deletePoliticalParty,
  createPoliticalParty,
};
module.exports = votesController;
