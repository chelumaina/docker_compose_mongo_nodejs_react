const db = require("../models");
const User = db.user;
const Role = db.role;

const { RegisterValidate, LoginValidate } = require("../validate");
   

  const getRoles = async (req, res) => {
      
    try {
      //Check if User email exist
      const roles = await Role.find();
      res.json(roles);
    } catch (error) {
      res.status(400).json({ message: error.message });
    }
  }; 


  const getRole = async (req, res) => {
    
    let _id = req.params.id; 
    try { 
      const roles = await Role.findById(_id);
      res.json(roles);  
    } catch (error) {
      res.status(400).json({ message: error.message });
    }
  }; 
  const roleController = { 
    getRole,
    getRoles
  };
  module.exports = roleController;

