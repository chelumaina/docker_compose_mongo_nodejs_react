const db = require("../models");
const User = db.user;
const Role = db.role;
const Stations = db.pollingStation;

const { RegisterValidate, LoginValidate } = require("../validate");

// router.post("/getPollingStation", [authJwt.verifyToken, authJwt.isModerator], getPollingStation);
// router.post("/createPollingStation", [authJwt.verifyToken, authJwt.isModerator], createPollingStation);
// router.post("/updatePollingStation", [authJwt.verifyToken, authJwt.isModerator], updatePollingStation);
// router.post("/deletePollingStation", [authJwt.verifyToken, authJwt.isModerator], deletePollingStation);

const getPollingStation = async (req, res) => {
  try {


    var pageNo = parseInt(req.query.pageNo)
    var size = parseInt(req.query.size)
    var query = {}
    if(pageNo < 0 || pageNo === 0 || pageNo == null || isNaN(pageNo) ) {
      response = {"error" : true,"message" : "invalid page number, should start with 1"};
          return res.json(response)
    }

    size=isNaN(size)?10:size;
    query.skip = size * (pageNo - 1);
    query.limit = size;

    var filter={}

    Stations.countDocuments(filter,function(err,totalCount) {
      if(err) {
        response = {"error" : true,"message" : "Error fetching data"}
      }
 
      Stations.find(filter,{},query, async function(err,data) {
            // Mongo command to fetch all data from collection.
          if(err) {
              response = {"error" : true,"message" : "Error fetching data"};
          } else {

              var totalPages = Math.ceil(totalCount / size)
              var track = [];
               const c= await Promise.all(data.map(async (post, i) =>{ 
                  const r = await get_station_stats(post, i).then((res, err)=>{return res;}); 
                  track.push(r);
                }));
              response = {"pages": totalPages,"current_page": pageNo,"size": size, "error" : false,"message" : track};
          }
          res.json(response);
        });
    })

    // //Check if User email exist
    // const roles = await Role.find();
    // res.json(roles);
  } catch (error) {
    res.status(400).json({ message: error.message });
  }
};

const createPollingStation = async (req, res) => {
  let _id = req.params.id;
  try {
    const roles = await Role.findOne({ _id: ObjectId(_id) });
    res.json(roles);
  } catch (error) {
    res.status(400).json({ message: error.message });
  }
};


const updatePollingStation = async (req, res) => {
  try {
    //Check if User email exist
    const roles = await Role.find();
    res.json(roles);
  } catch (error) {
    res.status(400).json({ message: error.message });
  }
};

const deletePollingStation = async (req, res) => {
  let _id = req.params.id;
  try {
    const roles = await Role.findOne({ _id: ObjectId(_id) });
    res.json(roles);
  } catch (error) {
    res.status(400).json({ message: error.message });
  }
};

const pollingStationController = {
  getPollingStation,
  createPollingStation,
  updatePollingStation,
  deletePollingStation,
};
module.exports = pollingStationController;
