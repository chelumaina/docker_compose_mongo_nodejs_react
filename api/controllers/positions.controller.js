const db = require("../models");
const Positions = db.position;

const fs = require("fs");
const { RegisterValidate } = require("../validate");




const updatePositions = async (req, res) => {
  try {
    //Check if User email exist
    const user = await Positions.findById(req.params.id);
    if (!user) return res.status(400).send({ message: "User Not Found" });

    const data = {name: req.body.name};

    await Positions.findOneAndUpdate({ _id: req.params.id }, data);
    res.status(200).json({ _id: req.params.id, message: "Üpdate Successful" });
  } catch (error) {
    res.status(400).json({ message: error.message });
  }
};
 

const getPositions = async (req, res) => {
  try {
    //Check if User email exist
    const cnt = await Positions.find();
    response = {"pages": 1,"current_page": 1,"size": 1, "error" : false,"message" : cnt};
    res.status(200).json(response); 
  } catch (error) {
    res.status(400).json({ message: error.message });
  }
};

const deletePositions = async (req, res) => {
  // const { error } = RegisterValidate(req.body);
  // if (error) {
  //   return res.status(400).send(error.details.map((msg) => msg));
  // }

  let _id = req.params.id;
  try {
    const updateBranch = await Positions.deleteOne({ _id: _id });
    response = {"pages": 1,"current_page": 1,"size": 1, "error" : false,"message" : updateBranch};
    res.status(200).json(response); 
  } catch (error) {
    res.status(400).json({ message: error.message });
  }
};
 
const createPositions = async (req, res, next) => {
  var d={name: req.body.name};
  var image = new Positions(d);  
  await image.save(function (err) {
    if (err) {
      return next(err);
    }
    res.status(200).json({ message: "OK" });
  });
};

const getOnePositions = async (req, res) => {
  try {
    //Check if User email exist
    const model = await Positions.findById(req.params.id);
    if (model) {
      response = {"pages": 1,"current_page": 1,"size": 1, "error" : false,"message" : model};
      res.status(200).json(response); 
    }
    else
    {
      response = {"error" : false, "data" : [], "message" : "Not Found"};
      res.status(200).json(response); 
    } 
    
  } catch (error) {
    res.status(400).json({ message: error.message });
  }
};


const votesController = {
  updatePositions,
  getPositions,
  deletePositions,
  createPositions,
  getOnePositions
};
module.exports = votesController;
