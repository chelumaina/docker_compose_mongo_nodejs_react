const db = require("../models");
const Constituency = db.constituency;
const { ObjectId } = require("mongodb");
const PollingStations = db.pollingStation;


const get_station_stats= async (p, i)=>{

  const pipeline= [
            {"$match" : { "constituency_id" : ObjectId(p._id)}}, 
            {"$group" : { "_id" : {"constituency_id" : "$constituency_id"}, "total" : { "$sum" : "$polling_station_registered_voters"}, "total_polling_station" : { "$sum" : 1}}}, 
        ]; 
  const aggCursor = await PollingStations.aggregate(pipeline);
  
  if(aggCursor)
  {
    return {
      id: i,
      _id: p._id,
      constituency_code: p.constituency_code,
      constituency_name: p.constituency_name,
      constituency_images:[],
      voter_registered:aggCursor[0].total,
      total_polling_stations:aggCursor[0].total_polling_station
    }
  }
  else 
  {
    return {
      id: i,
      _id: p._id,
      constituency_code: p.constituency_code,
      constituency_name: p.constituency_name,
      constituency_images:[],
      voter_registered:0,
      total_polling_stations:0
    }
  }
  
}

const updateConstituency = async (req, res) => {
  try {
    //Check if User email exist
    const user = await Constituency.findById(req.params.id);
    if (!user) return res.status(400).send({ message: "User Not Found" });

    const data = {
      constituency_name: req.body.constituency_name,
      constituency_code: req.body.constituency_code,
      county_id:req.body.county_id
    };

    await Counties.findOneAndUpdate({ _id: req.params.id }, data);
    res.status(200).json({ _id: req.params.id, message: "Üpdate Successful" });
  } catch (error) {
    res.status(400).json({ message: error.message });
  }
};
 


const getConstituencies =  async(req, res) => {
  try {
    var pageNo = parseInt(req.query.pageNo)
    var size = parseInt(req.query.size)
    var query = {}
    if(pageNo < 0 || pageNo === 0 || pageNo == null || isNaN(pageNo) ) {
      response = {"error" : true,"message" : "invalid page number, should start with 1"};
          return res.json(response)
    }

    size=isNaN(size)?10:size;
    query.skip = size * (pageNo - 1);
    query.limit = size;
    var filter={};

    Constituency.countDocuments(filter,function(err,totalCount) {
      if(err) {
        response = {"error" : true,"message" : "Error fetching data"}
      }

      Constituency.find(filter,{},query, async function(err,data) {
            // Mongo command to fetch all data from collection.
          if(err) {
              response = {"error" : true,"message" : "Error fetching data"};
          } else {

              var totalPages = Math.ceil(totalCount / size)
              var track = [];
               const c= await Promise.all(data.map(async (post, i) =>{ 
                  const r = await get_station_stats(post, i).then((res, err)=>{return res;}); 
                  track.push(r);
                }));
              response = {"pages": totalPages,"current_page": pageNo,"size": size, "error" : false,"message" : track};
          }
          res.json(response);
        });
    })
 
  } catch (error) {
    res.status(400).json({ message: error.message });
  }
};


const deleteConstituency = async (req, res) => {
  // const { error } = RegisterValidate(req.body);
  // if (error) {
  //   return res.status(400).send(error.details.map((msg) => msg));
  // }

  let _id = req.params.id;
  try {
    const updateBranch = await Constituency.deleteOne({ _id: _id });
    response = {"pages": 1,"current_page": 1,"size": 1, "error" : false,"message" : updateBranch};
    res.status(200).json(response); 
  } catch (error) {
    res.status(400).json({ message: error.message });
  }
};
 
const createConstituency = async (req, res, next) => {
  var d={
    constituency_name: req.body.constituency_name,
    constituency_code: req.body.constituency_code,
    county_id:req.body.county_id
  };
  var image = new Constituency(d);  
  await image.save(function (err) {
    if (err) {
      return next(err);
    }
    res.status(200).json({ message: "OK" });
  });
};

 

const getOneConstituency = async (req, res) => {
  try {
    const model = await Constituency.findById(req.params.id );
    if (model) {
      const cnt=await get_station_stats(model, 0);
      response = {"pages": 1,"current_page": 1,"size": 1, "error" : false,"message" : cnt};
    res.status(200).json(response);  
    }
    else
    {
      response = {"error" : false, "data" : [], "message" : "Not Found"};
      res.status(200).json(response); 
    }

 
  } 
  catch (error) {
    res.status(400).json({ message: error.message });
  }
};

const getCountyConstituencyById =  async(req, res) => {
  try {

    var pageNo = parseInt(req.query.pageNo)
    var size = parseInt(req.query.size)
    var query = {}
    if(pageNo < 0 || pageNo === 0 || pageNo == null || isNaN(pageNo) ) {
      response = {"error" : true,"message" : "invalid page number, should start with 1"};
          return res.json(response)
    }

    size=isNaN(size)?10:size;
    query.skip = size * (pageNo - 1);
    query.limit = size;

    var filter={county_id: req.params.county_id }

    Constituency.countDocuments(filter,function(err,totalCount) {
      if(err) {
        response = {"error" : true,"message" : "Error fetching data"}
      }
      var filter={county_id: req.params.county_id }

      Constituency.find(filter,{},query, async function(err,data) {
            // Mongo command to fetch all data from collection.
          if(err) {
              response = {"error" : true,"message" : "Error fetching data"};
          } else {

              var totalPages = Math.ceil(totalCount / size)
              var track = [];
               const c= await Promise.all(data.map(async (post, i) =>{ 
                  const r = await get_station_stats(post, i).then((res, err)=>{return res;}); 
                  track.push(r);
                }));
              response = {"pages": totalPages,"current_page": pageNo,"size": size, "error" : false,"message" : track};
          }
          res.json(response);
        });
    })
  
  } catch (error) {
    res.status(400).json({ message: error.message });
  }
};
 



 
const ConstituencyController = {
  getConstituencies,
  getOneConstituency,
  createConstituency,
  updateConstituency,
  deleteConstituency,
  getCountyConstituencyById,
};
module.exports = ConstituencyController;
