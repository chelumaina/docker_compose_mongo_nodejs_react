const db = require("../models");
const User = db.user;
const Role = db.role;
var jwt = require("jsonwebtoken");
var bcrypt = require("bcryptjs");

exports.signup = async (req, res) => {
  //Check if User email exist
  const email_exist = await User.findOne({ email: req.body.email });
  if (email_exist)
    return res.status(400).send({ email: "Email address is already in Use " });

  const phone = await User.findOne({ phone: req.body.phone });
  if (phone)
    return res.status(400).send({ phone: "Phone Number is already in Use " });
  //CHECK if password Exist

  const user = new User({
    name: req.body.name,
    email: req.body.email,
    role: req.body.roles,
    phone: req.body.phone,
    county_id: req.body.county_id,
    constituency_id: req.body.constituency_id,
    ward_id: req.body.ward_id,
    polling_station_id: req.body.polling_station_id,
    img: req.body.img,
    password: bcrypt.hashSync(req.body.password, 8),
  });
  user.save((err, user) => {
    if (err) {
      res.status(500).send({ message: err });
      return;
    }
    if (req.body.roles) {
      Role.find(
        {
          name: { $in: req.body.roles },
        },
        (err, roles) => {
          if (err) {
            res.status(500).send({ message: err });
            return;
          }
          user.role = roles.map((role) => role._id);
          user.save((err) => {
            if (err) {
              res.status(500).send({ message: err });
              return;
            }
            res.send({ message: "User was registered successfully!" });
          });
        }
      );
    } else {
      Role.findOne({ name: "user" }, (err, role) => {
        if (err) {
          res.status(500).send({ message: err });
          return;
        }
        user.role = [role._id];
        user.save((err) => {
          if (err) {
            res.status(500).send({ message: err });
            return;
          }
          res.send({ message: "User was registered successfully!" });
        });
      });
    }
  });
};

exports.signin = (req, res) => {
  User.findOne({
    email: req.body.email,
  })
    .populate("role")
    .exec((err, user) => {
      if (err) {
        res.status(500).send({ data: err });
      } else {

        const error = {
          email: user ? null : "Check your username/email",
          password:
            user && user.password === req.body.password
              ? null
              : "Check your password",
        };

        if (!user) {
          //return [200, { error }];
          return res.status(200).send({ error: error });
        }
        var passwordIsValid = bcrypt.compareSync(
          req.body.password,
          user.password
        );
        if (!passwordIsValid) {
          return res.status(200).send({ error: error });
        }

        var token = jwt.sign({ id: user.id }, process.env.TOKEN_SECRET, {
          expiresIn: 86400,
        });
        var authorities = [];
        var role='';

        for (let i = 0; i < user.role.length; i++) {
          authorities.push("ROLE_" + user.role[i].name.toUpperCase());
          role=user.role[i].name;
        } 
        var user2 = {
          id: user._id,
          from: "custom-db",
          username: user.username,
          email: user.email,
          roles: authorities,
          role: role,
          access_token: token,
          data: {
            displayName: user.data.displayName,
            photoURL: "assets/images/avatars/Abbott.jpg",
            email: user.email,
            settings: {
              layout: {
                style: "layout1",
                config: {
                  scroll: "content",
                  navbar: {
                    display: true,
                    folded: false,
                    position: "left",
                  },
                  toolbar: {
                    display: true,
                    style: "fixed",
                    position: "below",
                  },
                  footer: {
                    display: true,
                    style: "fixed",
                    position: "below",
                  },
                  mode: "fullwidth",
                },
              },
              customScrollbars: true,
              theme: {
                main: "light2", //"defaultDark",
                navbar: "light2", //"defaultDark",
                toolbar: "light2", //"defaultDark",
                footer: "light2", //"defaultDark"
              },
            },
            shortcuts: ["calendar", "mail", "contacts"],
          },
        };

        const response = {
          user: user2,
          access_token: token,
        };
        res.status(200).send(response);
      }
    });
};

exports.signin_with_token = async (req, res, next) => {
  try {
    let token = req.headers["x-access-token"];
    if (!token) {
      return res.status(403).send({ data: { err: "No token provided!" } });
    }

    jwt.verify(token, process.env.TOKEN_SECRET, async (err, decoded) => {
      if (err) {
        // return res.status(401).send({ message: "Unauthorized!" });
        res.status(400).json({ data: err.message });
      }
      req.userId = decoded.id;
      const updatedAccessToken = jwt.sign(
        { id: decoded.id },
        process.env.TOKEN_SECRET,
        { expiresIn: "7h" }
      );
      // const user = User.findById(decoded.id);
      const user = await User.findOne({
        _id: decoded.id,
      }) .populate("role");

      if (!user) return [];
      delete user.password;

      var authorities = [];
      var role=user.role[0].name;

        // for (let i = 0; i < user.role.length; i++) {
        //   authorities.push("ROLE_" + user.role[i].name.toUpperCase());
        // }


      var user2 = {
        id: user._id,
        from: "custom-db",
        username: user.username,
        email: user.email,
        roles: authorities,
        role: role,
        access_token: token,
        data: {
          displayName: user.data.displayName,
          photoURL: "assets/images/avatars/Abbott.jpg",
          email: user.email,
          settings: {
            layout: {
              style: "layout1",
              config: {
                scroll: "content",
                navbar: {
                  display: true,
                  folded: false,
                  position: "left",
                },
                toolbar: {
                  display: true,
                  style: "fixed",
                  position: "below",
                },
                footer: {
                  display: true,
                  style: "fixed",
                  position: "below",
                },
                mode: "fullwidth",
              },
            },
            customScrollbars: true,
            theme: {
              main: "light2", //"defaultDark",
              navbar: "light2", //"defaultDark",
              toolbar: "light2", //"defaultDark",
              footer: "light2", //"defaultDark"
            },
          },
          shortcuts: ["calendar", "mail", "contacts"],
        },
      };

      const response = {
        user: user2,
        access_token: token,
      };
      res.status(200).send(response);

      next();
    });
  } catch (error) {
    const error2 = "Invalid access token detected";
    return [401, { data: error2 }];
  }
};
