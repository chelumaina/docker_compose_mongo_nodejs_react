const db = require("../models"); 
const Vote = db.vote;

const fs = require("fs");
const { RegisterValidate } = require("../validate");

const updateVote = async (req, res) => {
  const { error } = RegisterValidate(req.body);
  if (error) {
    return res.status(400).send(error.details.map((msg) => msg));
  }

  var d={    
    form_name:req.body.form_name,
    county_id:req.body.county_id,
    constituency_id: req.body.constituency_id,
    ward_id: req.body.ward_id,
    polling_station_id:req.body.polling_station_id,
    position_id: req.body.position_id,
    results: json2array(JSON.parse(req.body.results)),
  };
  let _id = req.params.id;
  try {
    var image = new Votes({_id:_id}, d);
    //   res.status(400).json({ message: 'Test' });
    image.img.data = fs.readFileSync(req.file.path);
    image.img.contentType = "image/jpg";
    await image.save(function (err) {
      if (err) {
        return next(err);
      }
      res.status(200).json({ message: "OK" });
    });
  } catch (error) {
    res.status(400).json({ message: error.message });
  }
};



const uploadImage = async (req, res) => {
  const { error } = RegisterValidate(req.body);
  if (error) {
    return res.status(400).send(error.details.map((msg) => msg));
  } 
  let _id = req.params.id;
  try {
    var image = new Votes.findById(_id);
    //   res.status(400).json({ message: 'Test' });
    image.img.data = fs.readFileSync(req.file.path);
    image.img.contentType = "image/jpg";
    await image.save(function (err) {
      if (err) {
        return next(err);
      }
      res.status(200).json({ message: "OK" });
    });
  } catch (error) {
    res.status(400).json({ message: error.message });
  }
};


const getVotes2 = async (req, res) => {
  try {

    var pageNo = parseInt(req.query.pageNo)
    var size = parseInt(req.query.size)
    var query = {}
    if(pageNo < 0 || pageNo === 0 || pageNo == null || isNaN(pageNo) ) {
      response = {"error" : true,"message" : "invalid page number, should start with 1"};
          return res.json(response)
    }

    size=isNaN(size)?10:size;
    query.skip = size * (pageNo - 1);
    query.limit = size;

    var filter={}

    Votes.countDocuments(filter,function(err,totalCount) {
      if(err) {
        response = {"error" : true,"message" : "Error fetching data"}
      }
 
      Votes.find(filter,{},query, async function(err,data) {
            // Mongo command to fetch all data from collection.
          if(err) {
              response = {"error" : true,"message" : "Error fetching data"};
          } else {

              var totalPages = Math.ceil(totalCount / size)
              var track = [];
               const c= await Promise.all(data.map(async (post, i) =>{ 
                  // const r = await get_station_stats(post, i).then((res, err)=>{return res;}); 
                  track.push(post);
                }));
              response = {"pages": totalPages,"current_page": pageNo,"size": size, "error" : false,"message" : track};
          }
          res.json(response);
        });
    })

    // //Check if User email exist
    // const votes = await Votes.find();
    // res.status(200).json(votes);
  } catch (error) {
    res.status(400).json({ message: error.message });
  }
};

const getVotes =  async(req, res) => {
  try {

    var pageNo = parseInt(req.query.pageNo)
    var size = parseInt(req.query.size)
    var query = {}
    if(pageNo < 0 || pageNo === 0 || pageNo == null || isNaN(pageNo) ) {
      response = {"error" : true,"message" : "invalid page number, should start with 1"};
          return res.json(response)
    }

    size=isNaN(size)?10:size;
    query.skip = size * (pageNo - 1);
    query.limit = size;

  
    var filter={}
    Vote.countDocuments(filter,function(err,totalCount) {
      if(err) {
        response = {"error" : true,"message" : "Error fetching data"}
      }


      Vote.find(filter, {}, query)
      // .populate("role")
      .populate("polling_station_id")
      // .populate("ward_id")
      // .populate("constituency_id")
      // .populate("county_id")
      .populate("position_id")
 
      .exec((err, user) => {
          if (err) 
          {
            response = {"error" : true,"message" : "Error fetching data"};
            res.status(500).send({ message: response });
          } 
          else 
          {
            var totalPages = Math.ceil(totalCount / size)
            response = {"pages": totalPages,"current_page": pageNo,"size": size, "error" : false,"message" : user};
            res.status(200).json(response);
          }
        });

      

      // Vote.find(filter,{},query, async function(err,data) {
      //       // Mongo command to fetch all data from collection.
      //     if(err) {
      //         response = {"error" : true,"message" : "Error fetching data"};
      //     } else {

      //         var totalPages = Math.ceil(totalCount / size)
      //         var track = [];
      //         const c= await Promise.all(data.map(async (post, i) =>{ 
      //           // const r = await get_station_stats(post, i).then((res, err)=>{return res;}); 
      //           track.push(post);
      //         }));
      //         response = {"pages": totalPages,"current_page": pageNo,"size": size, "error" : false,"message" : track};
      //     }
      //     res.json(response);
      //   });
 
    })
 
  } catch (error) {
    res.status(400).json({ message: error.message });
  }
};

const deleteVote = async (req, res) => {
  const { error } = RegisterValidate(req.body);
  if (error) {
    return res.status(400).send(error.details.map((msg) => msg));
  }

  let _id = req.params.id;
  try {
    const updateBranch = await Votes.deleteOne({ _id: _id }, data);
    res.status(200).json(updateBranch);
  } catch (error) {
    res.status(400).json({ message: error.message });
  }
};


const json2array=(json)=>{
  var result = [];
  var keys = Object.keys(json);
  keys.forEach(function(key){
      result.push(json[key]);
  });
  return result;
}

const createVote = async (req, res, next) => {

 
  var d={
    form_name:req.body.form_name,
    county_id:req.body.county_id,
    constituency_id: req.body.constituency_id,
    ward_id: req.body.ward_id,
    polling_station_id:req.body.polling_station_id,
    position_id: req.body.position_id,
    results: json2array(JSON.parse(req.body.results)),
  };
  var image = new Votes(d); 
  image.img.data = fs.readFileSync(req.file.path);
  image.img.contentType = "image/jpg"; 
  await image.save(function (err) {
    if (err) {
      return next(err);
    }
    res.status(200).json({ message: "OK" });
  });
};

const getOneVote = async (req, res) => {
  try {
    //Check if User email exist
    const model = await Votes.findById(req.params.id);
    if (model) {
      res.status(200).json(model);

    }
    else
    {
      response = {"error" : false, "data" : [], "message" : "Not Found"};
      res.status(200).json(response); 
    } 
    
    
  } catch (error) {
    res.status(400).json({ message: error.message });
  }
};


const getWardVotes = async (req, res) => {
  try {

    var pageNo = parseInt(req.query.pageNo)
    var size = parseInt(req.query.size)
    var query = {}
    if(pageNo < 0 || pageNo === 0 || pageNo == null || isNaN(pageNo) ) {
      response = {"error" : true,"message" : "invalid page number, should start with 1"};
          return res.json(response)
    }

    size=isNaN(size)?10:size;
    query.skip = size * (pageNo - 1);
    query.limit = size;

    var filter={ward_id: req.params.ward_id}

    Votes.countDocuments(filter,function(err,totalCount) {
      if(err) {
        response = {"error" : true,"message" : "Error fetching data"}
      }
 
      Votes.find(filter,{},query, async function(err,data) {
            // Mongo command to fetch all data from collection.
          if(err) {
              response = {"error" : true,"message" : "Error fetching data"};
          } else {

              var totalPages = Math.ceil(totalCount / size)
              var track = [];
               const c= await Promise.all(data.map(async (post, i) =>{ 
                  // const r = await get_station_stats(post, i).then((res, err)=>{return res;}); 
                  track.push(post);
                }));
              response = {"pages": totalPages,"current_page": pageNo,"size": size, "error" : false,"message" : track};
          }
          res.json(response);
        });
    })

    // //Check if User email exist
    // const model = await Votes.find({ward_id: req.params.ward_id });
    // res.json(model);
  } catch (error) {
    res.status(400).json({ message: error.message });
  }
};

const getConstituencyVotes = async (req, res) => {
  try {

    var pageNo = parseInt(req.query.pageNo)
    var size = parseInt(req.query.size)
    var query = {}
    if(pageNo < 0 || pageNo === 0 || pageNo == null || isNaN(pageNo) ) {
      response = {"error" : true,"message" : "invalid page number, should start with 1"};
          return res.json(response)
    }

    size=isNaN(size)?10:size;
    query.skip = size * (pageNo - 1);
    query.limit = size;

    var filter={constituency_id: req.params.constituency_id}

    Votes.countDocuments(filter,function(err,totalCount) {
      if(err) {
        response = {"error" : true,"message" : "Error fetching data"}
      }
 
      Votes.find(filter,{},query, async function(err,data) {
            // Mongo command to fetch all data from collection.
          if(err) {
              response = {"error" : true,"message" : "Error fetching data"};
          } else {

              var totalPages = Math.ceil(totalCount / size)
              var track = [];
               const c= await Promise.all(data.map(async (post, i) =>{ 
                  // const r = await get_station_stats(post, i).then((res, err)=>{return res;}); 
                  track.push(post);
                }));
              response = {"pages": totalPages,"current_page": pageNo,"size": size, "error" : false,"message" : track};
          }
          res.json(response);
        });
    })

    // //Check if User email exist
    // const model = await Votes.find({
    //   constituency_id: req.params.constituency_id,
    // });
    // res.json(model);
  } catch (error) {
    res.status(400).json({ message: error.message });
  }
};

const getCountyVotes = async (req, res) => {
  try {

    var pageNo = parseInt(req.query.pageNo)
    var size = parseInt(req.query.size)
    var query = {}
    if(pageNo < 0 || pageNo === 0 || pageNo == null || isNaN(pageNo) ) {
      response = {"error" : true,"message" : "invalid page number, should start with 1"};
          return res.json(response)
    }

    size=isNaN(size)?10:size;
    query.skip = size * (pageNo - 1);
    query.limit = size;

    var filter={county_id: req.params.county_id}

    Votes.countDocuments(filter,function(err,totalCount) {
      if(err) {
        response = {"error" : true,"message" : "Error fetching data"}
      }
 
      Votes.find(filter,{},query, async function(err,data) {
            // Mongo command to fetch all data from collection.
          if(err) {
              response = {"error" : true,"message" : "Error fetching data"};
          } else {

              var totalPages = Math.ceil(totalCount / size)
              var track = [];
               const c= await Promise.all(data.map(async (post, i) =>{ 
                  // const r = await get_station_stats(post, i).then((res, err)=>{return res;}); 
                  track.push(post);
                }));
              response = {"pages": totalPages,"current_page": pageNo,"size": size, "error" : false,"message" : track};
          }
          res.json(response);
        });
    })

    // //Check if User email exist
    // const model = await Votes.find({county_id: req.params.county_id });
    // res.json(model);
  } catch (error) {
    res.status(400).json({ message: error.message });
  }
};





const votesController = {
  getVotes, 
  createVote, 
  updateVote,  
  deleteVote, 
  getOneVote, 
  getCountyVotes, 
  getConstituencyVotes, 
  getWardVotes,
  uploadImage
};
module.exports = votesController;
