const db = require("../../models");
const Agent = db.user;


const getAgents = async (req, res) => {
  try {

    var pageNo = parseInt(req.query.pageNo)
    var size = parseInt(req.query.size)
    var query = {}
    if(pageNo < 0 || pageNo === 0 || pageNo == null || isNaN(pageNo) ) {
      response = {"error" : true,"message" : "invalid page number, should start with 1"};
          return res.json(response)
    }

    size=isNaN(size)?10:size;
    query.skip = size * (pageNo - 1);
    query.limit = size;

    var filter={}

    Agent.countDocuments(filter,function(err,totalCount) {
      if(err) {
        response = {"error" : true,"message" : "Error fetching data"}
      }
 
      Agent.find(filter,{},query, async function(err,data) {
            // Mongo command to fetch all data from collection.
          if(err) {
              response = {"error" : true,"message" : "Error fetching data"};
          } else {

              var totalPages = Math.ceil(totalCount / size)
              var track = [];
               const c= await Promise.all(data.map(async (post, i) =>{ 
                  // const r = await get_station_stats(post, i).then((res, err)=>{return res;}); 
                  track.push(post);
                }));
              response = {"pages": totalPages,"current_page": pageNo,"size": size, "error" : false,"message" : track};
          }
          res.json(response);
        });
    })
    // //Check if User email exist
    // const model = await Agent.find();

    // res.json(model);
  } catch (error) {
    res.status(400).json({ message: error.message });
  }
};

const getOneAgent = async (req, res) => {
  try {
    //Check if User email exist
    const model = await Candidate.findById(req.params.id);
    if (model) {
      res.status(200).json(model);
    }
    else
    {
      response = {"error" : false, "data" : [], "message" : "Not Found"};
      res.status(200).json(response); 
    } 
    
    
  } catch (error) {
    res.status(400).json({ message: error.message });
  }
};
const getAgentByCounty = async (req, res) => {
  try {
    var pageNo = parseInt(req.query.pageNo)
    var size = parseInt(req.query.size)
    var query = {}
    if(pageNo < 0 || pageNo === 0 || pageNo == null || isNaN(pageNo) ) {
      response = {"error" : true,"message" : "invalid page number, should start with 1"};
          return res.json(response)
    }

    size=isNaN(size)?10:size;
    query.skip = size * (pageNo - 1);
    query.limit = size;

    var filter={county_id:req.params.county_id}

    Agent.countDocuments(filter,function(err,totalCount) {
      if(err) {
        response = {"error" : true,"message" : "Error fetching data"}
      }
 
      Agent.find(filter,{},query, async function(err,data) {
            // Mongo command to fetch all data from collection.
          if(err) {
              response = {"error" : true,"message" : "Error fetching data"};
          } else {

              var totalPages = Math.ceil(totalCount / size)
              var track = [];
               const c= await Promise.all(data.map(async (post, i) =>{ 
                  // const r = await get_station_stats(post, i).then((res, err)=>{return res;}); 
                  track.push(post);
                }));
              response = {"pages": totalPages,"current_page": pageNo,"size": size, "error" : false,"message" : track};
          }
          res.json(response);
        });
    })

    // //Check if User email exist
    // const model = await Candidate.find({county_id:req.params.county_id});
    // res.json(model);
  } catch (error) {
    res.status(400).json({ message: error.message });
  }
};

const getAgentByConstituency = async (req, res) => {
  try {

    var pageNo = parseInt(req.query.pageNo)
    var size = parseInt(req.query.size)
    var query = {}
    if(pageNo < 0 || pageNo === 0 || pageNo == null || isNaN(pageNo) ) {
      response = {"error" : true,"message" : "invalid page number, should start with 1"};
          return res.json(response)
    }

    size=isNaN(size)?10:size;
    query.skip = size * (pageNo - 1);
    query.limit = size;

    var filter={constituency_id:req.params.constituency_id}

    Agent.countDocuments(filter,function(err,totalCount) {
      if(err) {
        response = {"error" : true,"message" : "Error fetching data"}
      }
 
      Agent.find(filter,{},query, async function(err,data) {
            // Mongo command to fetch all data from collection.
          if(err) {
              response = {"error" : true,"message" : "Error fetching data"};
          } else {

              var totalPages = Math.ceil(totalCount / size)
              var track = [];
               const c= await Promise.all(data.map(async (post, i) =>{ 
                  // const r = await get_station_stats(post, i).then((res, err)=>{return res;}); 
                  track.push(post);
                }));
              response = {"pages": totalPages,"current_page": pageNo,"size": size, "error" : false,"message" : track};
          }
          res.json(response);
        });
    })

    // //Check if User email exist
    // const model = await Candidate.find({constituency_id:req.params.constituency_id});
    // res.json(model);
  } catch (error) {
    res.status(400).json({ message: error.message });
  }
};
const getAgentByWard = async (req, res) => {
  try {
    var pageNo = parseInt(req.query.pageNo)
    var size = parseInt(req.query.size)
    var query = {}
    if(pageNo < 0 || pageNo === 0 || pageNo == null || isNaN(pageNo) ) {
      response = {"error" : true,"message" : "invalid page number, should start with 1"};
          return res.json(response)
    }

    size=isNaN(size)?10:size;
    query.skip = size * (pageNo - 1);
    query.limit = size;

    var filter={ward_id:req.params.ward_id}

    Agent.countDocuments(filter,function(err,totalCount) {
      if(err) {
        response = {"error" : true,"message" : "Error fetching data"}
      }
 
      Agent.find(filter,{},query, async function(err,data) {
            // Mongo command to fetch all data from collection.
          if(err) {
              response = {"error" : true,"message" : "Error fetching data"};
          } else {

              var totalPages = Math.ceil(totalCount / size)
              var track = [];
               const c= await Promise.all(data.map(async (post, i) =>{ 
                  // const r = await get_station_stats(post, i).then((res, err)=>{return res;}); 
                  track.push(post); 
                }));
              response = {"pages": totalPages,"current_page": pageNo,"size": size, "error" : false,"message" : track};
          }
          res.json(response);
        });
    })

    // //Check if User email exist
    // const model = await Candidate.find({ward_id:req.params.ward_id});
    // res.json(model);
  } catch (error) {
    res.status(400).json({ message: error.message });
  }
}; 

const getAgentByStation = async (req, res) => {
  try {
    var pageNo = parseInt(req.query.pageNo)
    var size = parseInt(req.query.size)
    var query = {}
    if(pageNo < 0 || pageNo === 0 || pageNo == null || isNaN(pageNo) ) {
      response = {"error" : true,"message" : "invalid page number, should start with 1"};
          return res.json(response)
    }

    size=isNaN(size)?10:size;
    query.skip = size * (pageNo - 1);
    query.limit = size;

    var filter={ward_id:req.params.ward_id}

    Agent.countDocuments(filter,function(err,totalCount) {
      if(err) {
        response = {"error" : true,"message" : "Error fetching data"}
      }
 
      Agent.find(filter,{},query, async function(err,data) {
            // Mongo command to fetch all data from collection.
          if(err) {
              response = {"error" : true,"message" : "Error fetching data"};
          } else {

              var totalPages = Math.ceil(totalCount / size)
              var track = [];
               const c= await Promise.all(data.map(async (post, i) =>{ 
                  // const r = await get_station_stats(post, i).then((res, err)=>{return res;}); 
                  track.push(post);
                }));
              response = {"pages": totalPages,"current_page": pageNo,"size": size, "error" : false,"message" : track};
          }
          res.json(response);
        });
    })
    // //Check if User email exist
    // const model = await Candidate.find({station_id:req.params.station_id});
    // res.json(model);
  } catch (error) {
    res.status(400).json({ message: error.message });
  }
}; 
 
 
const createAgent = async (req, res) => {
  try { 


    const data = new Agent({
      name: req.body.name,
      email: req.body.email,
      password: req.body.password,
      phone:req.body.phone,
      role_id:req.body.role_id,
      county_id: req.body.county_id,
      constituency_id: req.body.constituency_id,
      Ward_id: req.body.Ward_id,
      polling_station_id: req.body.polling_station_id,
    });
    const dataToSave = await data.save();
    res.json(dataToSave);
  } catch (error) {
    res.status(400).json({ message: error.message });
  }
};

const updateAgent = async (req, res) => {
  try {
    //Check if User email exist
    const user = await Agent.findById(req.params.id);
    if (!user) return res.status(400).send({ message: "User Not Found" });

    const data = {
      name: req.body.name,
      email: req.body.email,
      password: req.body.password,
      phone:req.body.phone,
      role_id:req.body.role_id,
      county_id: req.body.county_id,
      constituency_id: req.body.constituency_id,
      Ward_id: req.body.Ward_id,
      polling_station_id: req.body.polling_station_id,
    };

    await Agent.findOneAndUpdate({ _id: req.params.id }, data);
    res.status(200).json({ _id: req.params.id, message: "Üpdate Successful" });
  } catch (error) {
    res.status(400).json({ message: error.message });
  }
};

const deleteAgent = async (req, res) => {
  try {
    //Check if User email exist
    const user = await Agent.findById(req.params.id);
    if (!user) return res.status(400).send({ message: "Candidate Not Found " });

    await user.delete();
    res.status(200).json({ _id: req.params.id, message: "Deleted Successful" });
  } catch (error) {
    res.status(400).json({ message: error.message });
  }
};

const AgentController = {
  getAgents,
  getOneAgent,
  getAgentByCounty,
  getAgentByConstituency, 
  getAgentByWard,
  getAgentByStation,
  createAgent,
  updateAgent,
  deleteAgent,
};
module.exports = AgentController;
