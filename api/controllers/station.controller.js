const { ObjectID } = require("bson");
const db = require("../models");
const Station = db.pollingStation;
const Vote = db.vote;


// const get_station_stats=async (position_id,position_id_value, station_id, station_id_value)=>{
  const get_station_stats=async (req, res)=>{

    const station_id=req.params.station_id;
    const station_id_value=req.params.station_id_value;

  const pipeline= [
            {"$unwind" : "$results"}, 
            {"$match" : { "polling_station_id" : ObjectID(station_id_value)}}, 
            // {"$match" : { "polling_station_id" : ObjectID("62971f327820a761470ddf78")}},
            {"$group" : { "_id" : {"form_name" : "$form_name", "position_name" : "$position_name", station_id : "$"+station_id, "results" : "$results"}, "total" : { "$sum" : "$results.votes"}, "count1" : { "$sum" : 1}}}, 
            {"$project" : { "form_name" : "$_id.form_name", "position_name" : "$_id.position_name", station_id : "$_id."+station_id,  "results" : "$_id.results", "total" : "$total", "count1" : "$count1", "_id" : 0}}, 
            {"$sort" : { "total" : -1}}
        ];
  // const pipeline = [
  //   { $match: { categories: "Bakery" } },
  //   { $group: { _id: "$stars", count: { $sum: 1 } } }
  // ];

  const aggCursor = await Vote.aggregate(pipeline);

  for await (const doc of aggCursor) {
  }
}
const updateStation = async (req, res) => {
  try {
    //Check if User email exist
    const user = await Station.findById(req.params.id);
    if (!user) return res.status(400).send({ message: "User Not Found" });

    const data = {
      polling_station_name: req.body.polling_station_name,
      polling_station_code: req.body.polling_station_code,
      polling_station_registered_voters:req.body.polling_station_registered_voters,
      ward_id: req.body.ward_id,
      constituency_id: req.body.constituency_id,
      county_id: req.body.county_id,
    };

    await Station.findOneAndUpdate({ _id: req.params.id }, data);
    res.status(200).json({ _id: req.params.id, message: "Üpdate Successful" });
  } catch (error) {
    res.status(400).json({ message: error.message });
  }
};

const getStations = async (req, res) => {
  try {

    var pageNo = parseInt(req.query.pageNo)
    var size = parseInt(req.query.size)
    var query = {}
    if(pageNo < 0 || pageNo === 0 || pageNo == null || isNaN(pageNo) ) {
      response = {"error" : true,"message" : "invalid page number, should start with 1"};
          return res.json(response)
    }

    size=isNaN(size)?10:size;
    query.skip = size * (pageNo - 1);
    query.limit = size;

    var filter={ward_code:req.params.id}

    Station.countDocuments(filter,function(err,totalCount) {
      if(err) {
        response = {"error" : true,"message" : "Error fetching data"}
      }
 
      Station.find(filter,{},query, async function(err,data) {
            // Mongo command to fetch all data from collection.
          if(err) {
              response = {"error" : true,"message" : "Error fetching data"};
          } else {

              var totalPages = Math.ceil(totalCount / size)
              var track = [];
               const c= await Promise.all(data.map(async (post, i) =>{ 
                // const r = await get_station_stats(post, i).then((res, err)=>{return res;}); 
                track.push(post);
                }));
              response = {"pages": totalPages,"current_page": pageNo,"size": size, "error" : false,"message" : track};
          }
          res.json(response);
        });
    })

  } catch (error) {
    res.status(400).json({ message: error.message });
  }
};

const deleteStation = async (req, res) => {
  // const { error } = RegisterValidate(req.body);
  // if (error) {
  //   return res.status(400).send(error.details.map((msg) => msg));
  // }

  let _id = req.params.id;
  try {
    const cnt = await Station.deleteOne({ _id: _id });
    response = {"pages": 1,"current_page": 1,"size": 1, "error" : false,"message" : cnt};
    res.status(200).json(response); 
  } catch (error) {
    res.status(400).json({ message: error.message });
  }
};

const createStation = async (req, res, next) => {
  var d = {
    polling_station_name: req.body.polling_station_name,
    polling_station_code: req.body.polling_station_code,
    polling_station_registered_voters:req.body.polling_station_registered_voters,
    ward_id: req.body.ward_id,
    constituency_id: req.body.constituency_id,
    county_id: req.body.county_id,
  };
  var image = new Station(d);
  await image.save(function (err) {
    if (err) {
      return next(err);
    }
    res.status(200).json({ message: "OK" });
  });
};

const getOneStation = async (req, res) => {
  try {
    //Check if User email exist
    const model = await Station.findById(req.params.id); 
    response = {"pages": 1,"current_page": 1,"size": 1, "error" : false,"message" : model};
    res.status(200).json(response);  
  } catch (error) {
    res.status(400).json({ message: error.message });
  }
};

const getWardStations = async (req, res) => {
  try {


    var pageNo = parseInt(req.query.pageNo)
    var size = parseInt(req.query.size)
    var query = {}
    if(pageNo < 0 || pageNo === 0 || pageNo == null || isNaN(pageNo) ) {
      response = {"error" : true,"message" : "invalid page number, should start with 1"};
          return res.json(response)
    }

    size=isNaN(size)?10:size;
    query.skip = size * (pageNo - 1);
    query.limit = size;

    var filter={ward_id: req.params.ward_id }

    Station.countDocuments(filter,function(err,totalCount) {
      if(err) {
        response = {"error" : true,"message" : "Error fetching data"}
      }
 
      Station.find(filter,{},query, async function(err,data) {
            // Mongo command to fetch all data from collection.
          if(err) {
              response = {"error" : true,"message" : "Error fetching data"};
          } else {

              var totalPages = Math.ceil(totalCount / size)
              var track = [];
               const c= await Promise.all(data.map(async (post, i) =>{ 
                // const r = await get_station_stats(post, i).then((res, err)=>{return res;}); 
                track.push(post);
                }));
                track.push();

              response = {"pages": totalPages,"current_page": pageNo,"size": size, "error" : false,"message" : track};
          }
          res.json(response);
        });
    })


  } catch (error) {
    res.status(400).json({ message: error.message });
  }
};

const getConstituencyStations = async (req, res) => {
  try {

    var pageNo = parseInt(req.query.pageNo)
    var size = parseInt(req.query.size)
    var query = {}
    if(pageNo < 0 || pageNo === 0 || pageNo == null || isNaN(pageNo) ) {
      response = {"error" : true,"message" : "invalid page number, should start with 1"};
          return res.json(response)
    }

    size=isNaN(size)?10:size;
    query.skip = size * (pageNo - 1);
    query.limit = size;

    var filter={constituency_id: req.params.constituency_id }

    Station.countDocuments(filter,function(err,totalCount) {
      if(err) {
        response = {"error" : true,"message" : "Error fetching data"}
      }
 
      Station.find(filter,{},query, async function(err,data) {
            // Mongo command to fetch all data from collection.
          if(err) {
              response = {"error" : true,"message" : "Error fetching data"};
          } else {

              var totalPages = Math.ceil(totalCount / size)
              var track = [];
               const c= await Promise.all(data.map(async (post, i) =>{ 
                // const r = await get_station_stats(post, i).then((res, err)=>{return res;}); 
                track.push(post);
                }));
              response = {"pages": totalPages,"current_page": pageNo,"size": size, "error" : false,"message" : track};
          }
          res.json(response);
        });
    })

  
  } catch (error) {
    res.status(400).json({ message: error.message });
  }
};

const getCountyStations = async (req, res) => {
  try {

    var pageNo = parseInt(req.query.pageNo)
    var size = parseInt(req.query.size)
    var query = {}
    if(pageNo < 0 || pageNo === 0 || pageNo == null || isNaN(pageNo) ) {
      response = {"error" : true,"message" : "invalid page number, should start with 1"};
          return res.json(response)
    }

    size=isNaN(size)?10:size;
    query.skip = size * (pageNo - 1);
    query.limit = size;

    var filter={county_id: req.params.county_id }

    Station.countDocuments(filter,function(err,totalCount) {
      if(err) {
        response = {"error" : true,"message" : "Error fetching data"}
      }
 
      Station.find(filter,{},query, async function(err,data) {
            // Mongo command to fetch all data from collection.
          if(err) {
              response = {"error" : true,"message" : "Error fetching data"};
          } else {

              var totalPages = Math.ceil(totalCount / size)
              var track = [];
               const c= await Promise.all(data.map(async (post, i) =>{ 
                  // const r = await get_station_stats(post, i).then((res, err)=>{return res;}); 
                  track.push(post);
                }));
              response = {"pages": totalPages,"current_page": pageNo,"size": size, "error" : false,"message" : track};
          }
          res.json(response);
        });
    })
 
  } catch (error) {
    res.status(400).json({ message: error.message });
  }
};

const StationController = {
  getStations,
  getConstituencyStations,
  getWardStations,
  getOneStation,
  createStation,
  updateStation,
  deleteStation,
  getCountyStations,
  get_station_stats
};
module.exports = StationController;
