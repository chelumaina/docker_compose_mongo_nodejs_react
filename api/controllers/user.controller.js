const db = require("../models");
const User = db.user;
const Roles = db.role;
var bcrypt = require("bcryptjs");

const { RegisterValidate, LoginValidate } = require("../validate");
const { getPollingStation } = require("./pollingStation.controller");

const getAgents = async (req, res) => {
  try {
    //Check if User email exist

    var pageNo = parseInt(req.query.pageNo);
    var size = parseInt(req.query.size);
    var query = {};
    if (pageNo < 0 || pageNo === 0 || pageNo == null || isNaN(pageNo)) {
      response = {
        error: true,
        message: "invalid page number, should start with 1",
      };
      return res.json(response);
    }

    size = isNaN(size) ? 10 : size;
    query.skip = size * (pageNo - 1);
    query.limit = size;

    var filter = {};

    User.countDocuments(filter, function (err, totalCount) {
      if (err) {
        response = { error: true, message: "Error fetching data" };
      }

      var filter = {};

      User.find(filter, {}, query, async function (err, data) {
        // Mongo command to fetch all data from collection.
        if (err) {
          response = { error: true, message: "Error fetching data" };

          res.status(500).json(response);
        } else {
          var totalPages = Math.ceil(totalCount / size);
          var track = [];
          const c = await Promise.all(
            data.map(async (post) => {
              // const r = await get_station_stats(post, i).then((res, err)=>{return res;});

              track.push(post);
            })
          );
          response = {
            pages: totalCount,
            totalCount: totalCount,
            totalCount: totalCount,
            current_page: pageNo,
            size: size,
            error: false,
            message: track,
          };
          res.status(200).json(response);
        }
      });
    });

    // const model = await Candidate.find();
    // res.json(model);
  } catch (error) {
    res.status(400).json({ message: error.message });
  }
};

const getAgents2 = async (req, res) => {
  try {
    //Check if User email exist
    const model = await User.find();
    const agents = model.map((d) => ({
      id: d._id,
      name: d.name,
      email: d.email,
      phone: d.phone,
      role: d.role,
      polling_station_id: d.polling_station_id,
      ward_id: d.ward_id,
      constituency_id: d.constituency_id,
      county_id: d.county_id,
    }));

    res.json(agents);
  } catch (error) {
    res.status(400).json({ message: error.message });
  }
};

const getOneAgent = async (req, res) => {
  try {
    //Check if User email exist
    const model = await User.find({ email: req.params.email });
    if (model) {
      res.status(200).json({ message: model });
    } else {
      response = { error: false, data: [], message: "Not Found" };
      res.status(200).json({ message: response });
    }
  } catch (error) {
    res.status(400).json({ message: error.message });
  }
};
const getAgentByCounty = async (req, res) => {
  try {
    //Check if User email exist
    const model = await User.find({ county_id: req.params.county_id });
    res.json(model);
  } catch (error) {
    res.status(400).json({ message: error.message });
  }
};

const getAgentByConstituency = async (req, res) => {
  try {
    //Check if User email exist
    const model = await User.find({
      constituency_id: req.params.constituency_id,
    });
    res.json(model);
  } catch (error) {
    res.status(400).json({ message: error.message });
  }
};
const getAgentByWard = async (req, res) => {
  try {
    //Check if User email exist
    const model = await User.find({ ward_id: req.params.ward_id });
    res.json(model);
  } catch (error) {
    res.status(400).json({ message: error.message });
  }
};

const getAgentByStation = async (req, res) => {
  try {
    //Check if User email exist
    const model = await User.find({
      polling_station_id: req.params.station_id,
    });
    res.json(model);
  } catch (error) {
    res.status(400).json({ message: error.message });
  }
};

const createUser = async (req, res) => {
  try {
    const data = new User({
      name: req.body.name,
      email: req.body.email,
      password: req.body.password,
      phone: req.body.phone,
      role_id: req.body.role_id,
      county_id: req.body.county_id,
      constituency_id: req.body.constituency_id,
      Ward_id: req.body.Ward_id,
      polling_station_id: req.body.polling_station_id,
    });
    const dataToSave = await data.save();
    res.json(dataToSave);
  } catch (error) {
    res.status(400).json({ message: error.message });
  }
};

const updateUser = async (req, res) => {
  // const { error } = RegisterValidate(req.body);
  // if (error) {
  //   return res.status(400).send(error.details.map((msg) => msg));
  // }

  try {
    //Check if User email exist
    const user = await User.find({ email: req.params.email });
    if (!user) return res.status(400).send({ message: "User Not Found" });

    const salt = await bcrypt.genSalt(10);
    const hash_password = await bcrypt.hash(req.body.password, salt);
    const data = {
      name: req.body.name,
      email: req.body.email,
      password: hash_password,
      phone: req.body.phone,
      role: req.body.role,
      polling_station_id: req.body.polling_station_id,
      ward_id: req.body.ward_id,
      constituency_id: req.body.constituency_id,
      county_id: req.body.county_id,
    };

    await User.updateOne({ _id: user._id }, data);
    res.status(200).json({ _id: req.params.id, message: "Üpdate Successful" });
  } catch (error) {
    res.status(400).json({ message: error.message });
  }
};

const getUser = async (req, res) => {
  const _user = await User.find({ email: req.params.email });

  try {
    //Check if User email exist
    if (!_user) return res.status(400).send({ message: "User Not Found" });

    res.status(200).json({ message: _user });
  } catch (error) {
    res.status(400).json({ message: error.message });
  }
};

const getRoles = async (roles) => {
  if (roles.length == 0) return [];
  return await roles.map(async (r) => {
    await Roles.findById(r);
  });
};

const getAllUsers2 = async (req, res) => {
  try {
    //Check if User email exist

    var pageNo = parseInt(req.query.pageNo);
    var size = parseInt(req.query.size);
    var query = {};
    if (pageNo < 0 || pageNo === 0 || pageNo == null || isNaN(pageNo)) {
      response = {
        error: true,
        message: "invalid page number, should start with 1",
      };
      return res.json(response);
    }

    size = isNaN(size) ? 10 : size;
    query.skip = size * (pageNo - 1);
    query.limit = size;

    var filter = {};

    User.countDocuments(filter, function (err, totalCount) {
      if (err) {
        response = { error: true, message: "Error fetching data" };
      }

      var filter = {};

      User.find(filter, {}, query, async function (err, data) {
        // Mongo command to fetch all data from collection.
        if (err) {
          response = { error: true, message: "Error fetching data" };

          res.status(500).json(response);
        } else {
          var totalPages = Math.ceil(totalCount / size);
          var track = [];
          const c = await Promise.all(
            data.map(async (post) => {
              // const r = await get_station_stats(post, i).then((res, err)=>{return res;});

              track.push(post);
            })
          );
          response = {
            pages: totalCount,
            totalCount: totalCount,
            totalCount: totalCount,
            current_page: pageNo,
            size: size,
            error: false,
            message: track,
          };
          res.status(200).json(response);
        }
      });
    });

    // const model = await Candidate.find();
    // res.json(model);
  } catch (error) {
    res.status(400).json({ message: error.message });
  }
};

const getAllUsers = async (req, res) => {
  try {
    //Check if User email exist

    var pageNo = parseInt(req.query.pageNo);
    var size = parseInt(req.query.size);
    var query = {};
    if (pageNo < 0 || pageNo === 0 || pageNo == null || isNaN(pageNo)) {
      response = {
        error: true,
        message: "invalid page number, should start with 1",
      };
      return res.json(response);
    }

    size = isNaN(size) ? 10 : size;
    query.skip = size * (pageNo - 1);
    query.limit = size;

    var filter = {};

    User.countDocuments(filter, function (err, totalCount) {
      if (err) {
        response = { error: true, message: "Error fetching data" };
      }


      User.find(filter, {}, query)
      .populate("role")
      .populate("polling_station_id")
      .populate("ward_id")
      .populate("constituency_id")
      .populate("county_id")
      .exec((err, user) => {
          if (err) {
            res.status(500).send({ message: err });
          } else {
            res.status(200).send({ message: user });
          }
        });
    });

    // const model = await Candidate.find();
    // res.json(model);
  } catch (error) {
    res.status(400).json({ message: error.message });
  }
};
const getAllUsersw = async (req, res) => {
  const _user = await User.find();

  try {
    //Check if User email exist

    if (!_user) return res.status(400).send({ message: "User Not Found" });
    else {
      const agents = _user.map((d) => ({
        id: d._id,
        name: d.name,
        email: d.email,
        phone: d.phone,
        role: d.role, //await getRoles(d.role),
        polling_station_id: d.polling_station_id,
        ward_id: d.ward_id,
        constituency_id: d.constituency_id,
        county_id: d.county_id,
      }));

      // res.json(agents);
      res.status(200).json({ message: agents });
    }
  } catch (error) {
    res.status(400).json({ message: error.message });
  }
};

const deleteUser = async (req, res) => {
  try {
    //Check if User email exist
    const user = await User.find({ email: req.params.email });
    if (!user) return res.status(400).send({ email: "User Not Found " });

    const updateBranch = await User.deleteOne({ email: req.params.email });
    res.status(200).json({ message: updateBranch });
  } catch (error) {
    res.status(400).json({ message: error.message });
  }
};

const userController = {
  getAgents,
  getOneAgent,
  getAgentByCounty,
  getAgentByConstituency,
  getAgentByWard,
  getAgentByStation,
  createUser,
  updateUser,
  getUser,
  deleteUser,
  getAllUsers,
};
module.exports = userController;
