const db = require("../models");
const Candidate = db.candidate;
const Position = db.position;
const PoliticalParty = db.politicalParty;

const get_station_stats = async (p, i) => {
  const pipeline = [
    { $match: { constituency_id: ObjectId(p._id) } },
    {
      $group: {
        _id: { constituency_id: "$constituency_id" },
        total: { $sum: "$polling_station_registered_voters" },
        total_polling_station: { $sum: 1 },
      },
    },
  ];
  const aggCursor = await PollingStations.aggregate(pipeline);

  if (aggCursor) {
    return {
      id: i,
      _id: p._id,
      constituency_code: p.constituency_code,
      constituency_name: p.constituency_name,
      constituency_images: [],
      voter_registered: aggCursor[0].total,
      total_polling_stations: aggCursor[0].total_polling_station,
    };
  } else {
    return {
      id: i,
      _id: p._id,
      constituency_code: p.constituency_code,
      constituency_name: p.constituency_name,
      constituency_images: [],
      voter_registered: 0,
      total_polling_stations: 0,
    };
  }
};

const get_position = async (id) => {
  const pos = await Position.findById(id);
  if (!pos) return "Undefined";
  return pos.name;
};

const get_party = async (id) => {
  const party = await PoliticalParty.findById(id);
  if (!party) return "Undefined";
  return party.party_name;
};

const getCandidateByParty = async (req, res) => {
  try {
    var pageNo = parseInt(req.query.pageNo);
    var size = parseInt(req.query.size);
    var query = {};
    if (pageNo < 0 || pageNo === 0 || pageNo == null || isNaN(pageNo)) {
      response = {
        error: true,
        message: "invalid page number, should start with 1",
      };
      return res.json(response);
    }

    size = isNaN(size) ? 10 : size;
    query.skip = size * (pageNo - 1);
    query.limit = size;

    var filter = { party_id: req.params.party_id };

    Candidate.countDocuments(filter, function (err, totalCount) {
      if (err) {
        response = { error: true, message: "Error fetching data" };
      }

      Candidate.find(filter, {}, query, async function (err, data) {
        // Mongo command to fetch all data from collection.
        if (err) {
          response = { error: true, message: "Error fetching data" };

          res.status(500).json(response);
        } else {
          var totalPages = Math.ceil(totalCount / size);
          var track = [];
          //  const c= await Promise.all(data.map(async (post, i) =>{
          //     const r = await get_station_stats(post, i).then((res, err)=>{return res;});
          //     track.push(r);
          //   }));
          response = {
            pages: totalPages,
            totalCount:totalCount,
            current_page: pageNo,
            size: size,
            error: false,
            message: data,
          };
          res.status(200).json(response);
        }
      });
    });
    // //Check if User email exist
    // const model = await Candidate.find({ party_id: req.params.party_id });
    // res.json(model);
  } catch (error) {
    res.status(400).json({ message: error.message });
  }
};

const getCandidateByPosistion = async (req, res) => {
  try {
    var pageNo = parseInt(req.query.pageNo);
    var size = parseInt(req.query.size);
    var query = {};
    if (pageNo < 0 || pageNo === 0 || pageNo == null || isNaN(pageNo)) {
      response = {
        error: true,
        message: "invalid page number, should start with 1",
      };
      return res.json(response);
    }

    size = isNaN(size) ? 10 : size;
    query.skip = size * (pageNo - 1);
    query.limit = size;

    var filter = { position_id: req.params.position_id };

    Candidate.countDocuments(filter, function (err, totalCount) {
      if (err) {
        response = { error: true, message: "Error fetching data" };
      }

      Candidate.find(filter, {}, query, async function (err, data) {
        // Mongo command to fetch all data from collection.
        if (err) {
          response = { error: true, message: "Error fetching data" };

          res.status(500).json(response);
        } else {
          var totalPages = Math.ceil(totalCount / size);
          var track = [];
          //  const c= await Promise.all(data.map(async (post, i) =>{
          //     const r = await get_station_stats(post, i).then((res, err)=>{return res;});
          //     track.push(r);
          //   }));
          response = {
            pages: totalPages,
            totalCount:totalCount,
            current_page: pageNo,
            size: size,
            error: false,
            message: data,
          };
          res.status(200).json(response);
        }
      });
    });

    // //Check if User email exist
    // const model = await Candidate.find({position_id: req.params.position_id});
    // res.json(model);
  } catch (error) {
    res.status(400).json({ message: error.message });
  }
};

const getCandidate = async (req, res) => {
  try {
    //Check if User email exist

    var pageNo = parseInt(req.query.pageNo);
    var size = parseInt(req.query.size);
    var query = {};
    if (pageNo < 0 || pageNo === 0 || pageNo == null || isNaN(pageNo)) {
      response = {
        error: true,
        message: "invalid page number, should start with 1",
      };
      return res.json(response);
    }

    size = isNaN(size) ? 10 : size;
    query.skip = size * (pageNo - 1);
    query.limit = size;

    var filter = {};

    Candidate.countDocuments(filter, function (err, totalCount) {
      if (err) {
        response = { error: true, message: "Error fetching data" };
      }

      var filter = {};

      Candidate.find(filter, {}, query, async function (err, data) {
        // Mongo command to fetch all data from collection.
        if (err) {
          response = { error: true, message: "Error fetching data" };

          res.status(500).json(response);
        } else {
          var totalPages = Math.ceil(totalCount / size);
          var track = [];
          const c = await Promise.all(
            data.map(async (post) => {
              // const r = await get_station_stats(post, i).then((res, err)=>{return res;});
             
              var d = {
                _id: post._id,
                candidate_name: post.candidate_name,
                county_id: post.county_id,
                constituency_id: post.constituency_id,
                ward_id: post.ward_id,
                polling_station_id: post.polling_station_id,
                position_id: post.position_id,
                party_id: post.party_id,
                party_name:await get_party(post.party_id),
                position_name:await get_position(post.position_id)
              };
              track.push(d);

            })
          );
          response = {
            pages: totalCount,
            totalCount:totalCount,
            totalCount:totalCount,
            current_page: pageNo,
            size: size,
            error: false,
            message: track,
          };
          res.status(200).json(response);
        }
      });
    });

    // const model = await Candidate.find();
    // res.json(model);
  } catch (error) {
    res.status(400).json({ message: error.message });
  }
};

const getOneCandidate = async (req, res) => {
  try {
    //Check if User email exist
    const model = await Candidate.findById(req.params.id);
    if (model) {
      res.status(200).json(model);
    } else {
      response = { error: false, data: [], message: "Not Found" };
      res.status(200).json(response);
    }
  } catch (error) {
    res.status(400).json({ message: error.message });
  }
};

const getCandidateByCounty = async (req, res) => {
  try {
    var pageNo = parseInt(req.query.pageNo);
    var size = parseInt(req.query.size);
    var query = {};
    if (pageNo < 0 || pageNo === 0 || pageNo == null || isNaN(pageNo)) {
      response = {
        error: true,
        message: "invalid page number, should start with 1",
      };
      return res.json(response);
    }

    size = isNaN(size) ? 10 : size;
    query.skip = size * (pageNo - 1);
    query.limit = size;

    var filter = { county_id: req.params.county_id };

    Candidate.countDocuments(filter, function (err, totalCount) {
      if (err) {
        response = { error: true, message: "Error fetching data" };
      }

      Candidate.find(filter, {}, query, async function (err, data) {
        // Mongo command to fetch all data from collection.
        if (err) {
          response = { error: true, message: "Error fetching data" };
          res.status(500).json(response);
        } else {
          var totalPages = Math.ceil(totalCount / size);
          var track = [];
          //  const c= await Promise.all(data.map(async (post, i) =>{
          //     const r = await get_station_stats(post, i).then((res, err)=>{return res;});
          //     track.push(r);
          //   }));
          response = {
            pages: totalPages,
            totalCount:totalCount,
            current_page: pageNo,
            size: size,
            error: false,
            message: data,
          };
          res.status(200).json(response);
        }
      });
    });

    // //Check if User email exist
    // const model = await Candidate.find({ county_id: req.params.county_id });
    // res.json(model);
  } catch (error) {
    res.status(400).json({ message: error.message });
  }
};

const getCandidateByConstituency = async (req, res) => {
  try {
    var pageNo = parseInt(req.query.pageNo);
    var size = parseInt(req.query.size);
    var query = {};
    if (pageNo < 0 || pageNo === 0 || pageNo == null || isNaN(pageNo)) {
      response = {
        error: true,
        message: "invalid page number, should start with 1",
      };
      return res.json(response);
    }

    size = isNaN(size) ? 10 : size;
    query.skip = size * (pageNo - 1);
    query.limit = size;

    var filter = { constituency_id: req.params.constituency_id };

    Candidate.countDocuments(filter, function (err, totalCount) {
      if (err) {
        response = { error: true, message: "Error fetching data" };
      }

      Candidate.find(filter, {}, query, async function (err, data) {
        // Mongo command to fetch all data from collection.
        if (err) {
          response = { error: true, message: "Error fetching data" };
          res.status(500).json(response);
        } else {
          var totalPages = Math.ceil(totalCount / size);
          var track = [];
          //  const c= await Promise.all(data.map(async (post, i) =>{
          //     const r = await get_station_stats(post, i).then((res, err)=>{return res;});
          //     track.push(r);
          //   }));
          response = {
            pages: totalPages,
            totalCount:totalCount,
            current_page: pageNo,
            size: size,
            error: false,
            message: data,
          };
          res.status(200).json(response);
        }
      });
    });

    // //Check if User email exist
    // const model = await Candidate.find({ constituency_id: req.params.constituency_id });
    // res.json(model);
  } catch (error) {
    res.status(400).json({ message: error.message });
  }
};

const getCandidateByWard = async (req, res) => {
  try {
    var pageNo = parseInt(req.query.pageNo);
    var size = parseInt(req.query.size);
    var query = {};
    if (pageNo < 0 || pageNo === 0 || pageNo == null || isNaN(pageNo)) {
      response = {
        error: true,
        message: "invalid page number, should start with 1",
      };
      return res.json(response);
    }

    size = isNaN(size) ? 10 : size;
    query.skip = size * (pageNo - 1);
    query.limit = size;

    var filter = { ward_id: req.params.ward_id };

    Candidate.countDocuments(filter, function (err, totalCount) {
      if (err) {
        response = { error: true, message: "Error fetching data" };
      }

      Candidate.find(filter, {}, query, async function (err, data) {
        // Mongo command to fetch all data from collection.
        if (err) {
          response = { error: true, message: "Error fetching data" };
        } else {
          var totalPages = Math.ceil(totalCount / size);
          var track = [];
          //  const c= await Promise.all(data.map(async (post, i) =>{
          //     const r = await get_station_stats(post, i).then((res, err)=>{return res;});
          //     track.push(r);
          //   }));

          
          response = {
            pages: totalPages,
            totalCount:totalCount,
            current_page: pageNo,
            size: size,
            error: false,
            message: data,
          };
          res.status(200).json(response);
        }
      });
    });

    // //Check if User email exist
    // const model = await Candidate.find({ ward_id: req.params.ward_id });
    // res.json(model);
  } catch (error) {
    res.status(400).json({ message: error.message });
  }
};

const createCandidate = async (req, res) => {
  try {
    const data = new Candidate({
      candidate_name: req.body.candidate_name,
      party_id: req.body.party_id,
      position_id: req.body.position_id,
      county_id: req.body.county_id,
      constituency_id: req.body.constituency_id,
      ward_id: req.body.ward_id,
    });
    const dataToSave = await data.save();
    res.json(dataToSave);
  } catch (error) {
    res.status(400).json({ message: error.message });
  }
};

const updateCandidate = async (req, res) => {
  try {
    //Check if User email exist
    const user = await Candidate.findById(req.params.id);
    if (!user) return res.status(400).send({ message: "User Not Found" });

    const data = {
      candidate_name: req.body.candidate_name,
      party_id: req.body.party_id,
      position_id: req.body.position_id,
      county_id: req.body.county_id,
      constituency_id: req.body.constituency_id,
      ward_id: req.body.ward_id,
    };

    await Candidate.findOneAndUpdate({ _id: req.params.id }, data);
    res.status(200).json({ _id: req.params.id, message: "Üpdate Successful" });
  } catch (error) {
    res.status(400).json({ message: error.message });
  }
};

const deleteCandidate = async (req, res) => {
  try {
    //Check if User email exist
    const user = await Candidate.findById(req.params.id);
    if (!user) return res.status(400).send({ message: "Candidate Not Found " });

    await user.delete();
    res.status(200).json({ _id: req.params.id, message: "Deleted Successful" });
  } catch (error) {
    res.status(400).json({ message: error.message });
  }
};

const CandidateController = {
  getCandidate,
  getCandidateByParty,
  getCandidateByPosistion,

  createCandidate,
  updateCandidate,
  deleteCandidate,
  getOneCandidate,
  getCandidateByCounty,
  getCandidateByConstituency,
  getCandidateByWard,
};
module.exports = CandidateController;
