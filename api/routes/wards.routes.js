const express = require("express");
const router = express.Router();
const { authJwt } = require("../middleware");

const {
  getWards,
  getOneWard,
  getCountyWards,
  getConstituencyWards,

  createWard,
  updateWard,
  deleteWard,
} = require("../controllers/ward.controller");

router.post("/", [authJwt.verifyToken/*, authJwt.isAdmin*/], createWard);
router.get("/", [/*authJwt.verifyToken, authJwt.isAdmin*/], getWards);

router.delete("/:id", [authJwt.verifyToken/*, authJwt.isAdmin*/], deleteWard);
router.put("/:id", [authJwt.verifyToken/*, authJwt.isAdmin*/], updateWard);
router.get("/:id", [/*authJwt.verifyToken, authJwt.isAdmin*/], getOneWard);
router.get("/county_wards/:county_id",[/*authJwt.verifyToken*/],getCountyWards);
router.get("/constituencyWards/:constituency_id",[/*authJwt.verifyToken*/],getConstituencyWards);

module.exports = router;
