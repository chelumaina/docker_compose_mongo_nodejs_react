const express = require("express");
var fs = require("fs");

const User = require("../models/user.model");
const PollingStation = require("../models/pollingStation.model");
const router = express.Router();
const { RegisterValidate, LoginValidate } = require("../validate");
const bcrypt = require("bcryptjs");

var multer = require("multer");
var bodyParser = require("body-parser");
 
var upload = multer({ dest: 'uploads/' })
var ResultForm = require('../models/results.model');
const { ObjectId } = require("mongodb");

/*
*
*USERS MANAGEMENT
*
*/

//1. Create User end-point

router.post("/create_user", async (req, res) => {
  const { error } = RegisterValidate(req.body);
  if (error) {
    return res.status(400).send(error.details.map((msg) => msg));
  }

  try {
    //Check if User email exist
    const email_exist = await User.findOne({ email: req.body.email });
    if (email_exist)
      return res
        .status(400)
        .send({ email: "Email address is already in Use " });

    const phone = await User.findOne({ phone: req.body.phone });
    if (phone)
      return res.status(400).send({ phone: "Phone Number is already in Use " });
    //CHECK if password Exist

    const salt = await bcrypt.genSalt(10);
    const hash_password = await bcrypt.hash(req.body.password, salt);
    const data = new User({
      name: req.body.name,
      email: req.body.email,
      password: hash_password,
      phone: req.body.phone,
      role: req.body.role,
      // user_polling_station: req.body.user_polling_station,
    });

    const dataToSave = await data.save();
    res.status(200).json(dataToSave);
  } catch (error) {
    res.status(400).json({ message: error.message });
  }
});

//2. Update User

router.put("/update_user", async (req, res) => {

    const { error } = RegisterValidate(req.body);
  
    if (error) {
        return res.status(400).send(error.details);
    }
    try { 

        const salt = await bcrypt.genSalt(10);
        const hash_password = await bcrypt.hash(req.body.password, salt);


        const data = {
          name: req.body.name,
          email: req.body.email,
          password: hash_password,
          phone: req.body.phone,
          role: req.body.role,
          // user_polling_station: req.body.user_polling_station,
        };

    
        const updateBranch = await User.findOneAndUpdate({ email: req.body.email }, data);
        //updateBranch.user_polling_station
        //updateBranch.save()
        res.json({ result: "success", message: "Update Brach data successfully" });
      
    } catch (err) {
      res.json({ result: "error", message: err.msg });
    }
  });

router.put("/update_user/:id", async (req, res) => {
    const { error } = RegisterValidate(req.body);
    if (error) {
      return res.status(400).send(error.details.map((msg) => msg));
    }
  
    let _id = req.params.id;
    try {
      //Check if User email exist
      const user = await User.findOne({_id:ObjectId(_id)});
      if (!user)
        return res.status(400).send({ email: "User Not Found " });
  
      const salt = await bcrypt.genSalt(10);
      const hash_password = await bcrypt.hash(req.body.password, salt);
      const data = new User({
        name: req.body.name,
        email: req.body.email,
        password: hash_password,
        phone: req.body.phone,
        role: req.body.role,
      });
  
      const updateBranch = await User.findOneAndUpdate({ _id: _id }, data); 
      res.status(200).json(updateBranch);
    } catch (error) {
      res.status(400).json({ message: error.message });
    }
  }); 

router.get("/create_polling_stations", async (req, res) => {
  fs.readFile("iebc_data2.json", "utf8", async (err, jsonString) => {
    if (err) {
      return;
    }
    try {
      const polling_station = JSON.parse(jsonString);
      polling_station.map(async (p) => {
        const polling_station_code = await PollingStation.findOne({
          polling_station_code: p.POLLING_STATION_CODE,
        });

        if (!polling_station_code) {
          const data = new PollingStation({
            county_code: p.COUNTY_CODE,
            county_name: p.COUNTY_NAME,
            constituency_code: p.CONSTITUENCY_CODE,
            constituency_name: p.CONSTITUENCY_NAME,
            ward_code: p.CAW_CODE,
            ward_name: p.CAW_NAME,
            reg_center_code: p.REGISTRATION_CENTRE_CODE,
            reg_center_name: p.REGISTRATION_CENTRE_NAME,
            polling_station_code: p.POLLING_STATION_CODE,
            polling_station_name: p.POLLING_STATION_NAME,
            voter_per_center: p.VOTERS_PER_REGISTRATION_CENTRE,
            voter_per_polling_station: p.VOTERS_PER_POLLING_STATION,
          });
          const dataToSave = await data.save();
        } else {
        }
      });

      res.status(200).json({ size: polling_station.length });
    } catch (err) {
      res.status(400).json({ message: error.message });
    }
  });
});



router.get("/get_iebc_data", async (req, res) => {
  fs.readFile("iebc_data2.json", "utf8", async (err, jsonString) => {
    if (err) {
      return;
    }
    try {
    } catch (err) {
      res.status(400).json({ message: error.message });
    }
  });
});

router.post("/image/upload", upload.single("image"),  async (req, res, next)=> {
  var image = new ResultForm({
    name: req.body.image_name,
    results:[
        {candidate_id:1, candidate_name:"Raila Odinga", votes:200},
        {candidate_id:2, candidate_name:"Ruto", votes:400}
    ]
  });
//   res.status(400).json({ message: 'Test' });
  image.img.data = fs.readFileSync(req.file.path);
  image.img.contentType = "image/jpg";
  await image.save(function (err) {
    if (err) {
      return next(err);
    }
    res.status(200).json({ message: 'OK' })
  });
});

//Post Method
router.post("/post", async (req, res) => {
  const data = new User({
    name: req.body.name,
    age: req.body.age,
  });

  try {
    const dataToSave = await data.save();
    res.status(200).json(dataToSave);
  } catch (error) {
    res.status(400).json({ message: error.message });
  }
});

//Get all Method
router.get("/getAll", async (req, res) => {
  try {
    const data = await User.find();
    // data.map(user=>{
    //     let updateBranch = await user.findByIdAndUpdate({ _id: fields.id }, { name: fields.name, tel: fields.tel, address: fields.address });
    //     let pos_arr = fields.pos_machines.split(',')
    //     const pos = await posmachine.find().where('_id').in(pos_arr).exec();
    //     updateBranch.pos_machines = pos
    //     await updateBranch.save()
    // });
    res.json(data);
  } catch (error) {
    res.status(500).json({ message: error.message });
  }
});

//Get by ID Method
router.get("/getOne/:id", async (req, res) => {
  try {
    const data = await User.findById(req.params.id);
    res.json(data);
  } catch (error) {
    res.status(500).json({ message: error.message });
  }
});

//Update by ID Method
router.patch("/update/:id", async (req, res) => {
  try {
    const id = req.params.id;
    const updatedData = req.body;
    const options = { new: true };

    const result = await User.findByIdAndUpdate(id, updatedData, options);

    res.send(result);
  } catch (error) {
    res.status(500).json({ message: error.message });
  }
});

//Delete by ID Method
router.delete("/delete/:id", async (req, res) => {
  try {
    const id = req.params.id;
    const data = await User.findByIdAndDelete(id);
    res.send(`Document with ${data.name} has been deleted..`);
  } catch (error) {
    res.status(400).json({ message: error.message });
  }
});

module.exports = router;
