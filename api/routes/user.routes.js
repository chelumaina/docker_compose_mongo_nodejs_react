const express = require("express"); 
const router = express.Router();
const {authJwt} = require('../middleware');

const {
    getAgents,
    getOneAgent,
    getAgentByCounty,
    getAgentByConstituency, 
    getAgentByWard,
    getAgentByStation,
    updateUser,
    createUser,
    getUser,
    deleteUser,
    getAllUsers} = require('../controllers/user.controller')

//Create User end-point
// router.post("/", [authJwt.verifyToken, authJwt.isAdmin], createUser);
// router.put("/update/:id", [authJwt.verifyToken, authJwt.isAdmin], updateUser);
// router.delete("/DELETE/:id", [authJwt.verifyToken, authJwt.isAdmin], deleteUser);
// router.get("/get/:id", [authJwt.verifyToken, authJwt.isAdmin], getUser);

router.post("/", [authJwt.verifyToken, authJwt.isAdmin], createUser); 
router.get("/", [authJwt.verifyToken, authJwt.isAdmin], getAllUsers); 

router.delete("/:email", [authJwt.verifyToken, authJwt.isAdmin], deleteUser); 
router.put("/:email", [authJwt.verifyToken, authJwt.isAdmin], updateUser);
router.get("/:email", [authJwt.verifyToken, authJwt.isAdmin], getOneAgent); 

router.get("/getUserByCounty/:county_id",[authJwt.verifyToken],getAgentByCounty);
router.get("/getUserByConstituency/:constituency_id",[authJwt.verifyToken],getAgentByConstituency);
router.get("/getUserByWard/:ward_id", [authJwt.verifyToken], getAgentByWard);
router.get("/getUserByPollingStation/:station_id",[authJwt.verifyToken],getAgentByStation);
 

 

module.exports = router;
