const authRoute = require("./auth.routes");
const userRoute = require("./user.routes");
const roleRoute = require("./roles.routes");
const pollingStationRoute = require("./polling_station.routes");
const candidateRoute = require("./candidates.routes");
const votesRoute = require("./votes.routes");

const CountyRoute = require("./county.routes");
const ConstituencyRoute = require("./constituency.routes");
const WardsRoute = require("./wards.routes");
const StationRoute = require("./station.routes");
const PositionRoute = require("./positions.routes");
const PoliticalParty = require("./politicalParty.routes");
// const AgentRoute = require("./agents.routes");

// const Route = require("./routes");
module.exports = {
    authRoute,
    userRoute,
    // Route,
    roleRoute,
    pollingStationRoute,
    candidateRoute,
    votesRoute,
    CountyRoute,
    ConstituencyRoute,
    WardsRoute,
    StationRoute,
    PositionRoute,
    PoliticalParty
    // AgentRoute,
};