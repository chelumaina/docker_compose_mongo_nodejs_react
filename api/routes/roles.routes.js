const express = require("express");
// const Role = require("../models/role.model");
// const bcrypt = require("bcryptjs");
// const { RegisterValidate, LoginValidate } = require("../validate");
const router = express.Router();
const {authJwt} = require('../middleware');

const {getRoles, getRole} = require('../controllers/role.controller')


router.get("/getAll", [authJwt.verifyToken, authJwt.isAdmin], getRoles); 
router.get("/getOne/:id", [authJwt.verifyToken, authJwt.isAdmin], getRole); 
 

module.exports = router;
