const express = require("express"); 
const router = express.Router();
const {authJwt} = require('../middleware');

const {
    getStations,
    getConstituencyStations, 
    getWardStations,
    getOneStation,
    createStation,
    updateStation,
    deleteStation,
    getCountyStations,
    get_station_stats
} = require('../controllers/station.controller')
 
router.post("/", [authJwt.verifyToken/*, authJwt.isAdmin*/], createStation);
router.get("/", [/*authJwt.verifyToken, authJwt.isAdmin*/], getStations);

router.delete("/:id", [authJwt.verifyToken/*, authJwt.isAdmin*/], deleteStation);
router.put("/:id", [authJwt.verifyToken/*, authJwt.isAdmin*/], updateStation);
router.get("/:id", [/*authJwt.verifyToken, authJwt.isAdmin*/], getOneStation);
router.get("/county_stations/:county_id",[/*authJwt.verifyToken*/],getCountyStations);
router.get("/constituency_stations/:constituency_id",[/*authJwt.verifyToken*/],getConstituencyStations);
router.get("/ward_stations/:ward_id",[/*authJwt.verifyToken*/],getWardStations);
router.get("/get_station_stats/:station_id/:station_id_value",[/*authJwt.verifyToken*/],get_station_stats);



 
module.exports = router;
