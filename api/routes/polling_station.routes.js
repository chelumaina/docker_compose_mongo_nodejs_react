const express = require("express");
const pollingStation = require("../models/pollingStation.model");
const bcrypt = require("bcryptjs");
const { RegisterValidate, LoginValidate } = require("../validate");
const router = express.Router();
const {authJwt} = require('../middleware');

const {getPollingStation, createPollingStation, updatePollingStation,  deletePollingStation,
    getPollingStationOne,getPollingStationCounty, getPollingStationContituency, getPollingStationWard, getIEBCData } = require('../controllers/pollingStation.controller')
 
// router.get("/get", [authJwt.verifyToken, authJwt.isModerator], getPollingStation); 
// router.post("/create", [authJwt.verifyToken, authJwt.isModerator], createPollingStation); 
// router.post("/update/:id", [authJwt.verifyToken, authJwt.isModerator], updatePollingStation); 
// router.post("/delete/:id", [authJwt.verifyToken, authJwt.isModerator], deletePollingStation);  
 
router.get("/get", [authJwt.verifyToken], getPollingStation); 
router.get("/getOne/:id", [authJwt.verifyToken], getPollingStationOne);
router.get("/filterByCounty/:id", [authJwt.verifyToken], getPollingStationCounty); 
router.get("/filterByConstituency/:id", [authJwt.verifyToken], getPollingStationContituency); 
router.get("/filterByWard/:id", [authJwt.verifyToken], getPollingStationWard);  


router.post("/create", [authJwt.verifyToken, authJwt.isModerator], createPollingStation); 
router.post("/update/:id", [authJwt.verifyToken, authJwt.isModerator], updatePollingStation); 
router.post("/delete/:id", [authJwt.verifyToken, authJwt.isModerator], deletePollingStation);
router.get("/iebc",  getIEBCData);  
 
module.exports = router;
