
const express = require("express"); 
const router = express.Router();
const {authJwt} = require('../middleware'); 

const {getPoliticalParty, createPoliticalParty, updatePoliticalParty,  deletePoliticalParty, getOnePoliticalParty} = require('../controllers/politicalParty.controller')



router.post("/", [authJwt.verifyToken, authJwt.isAdmin], createPoliticalParty); 
router.get("/", [authJwt.verifyToken, authJwt.isAdmin], getPoliticalParty); 

router.delete("/:id", [authJwt.verifyToken, authJwt.isAdmin], deletePoliticalParty); 
router.put("/:id", [authJwt.verifyToken, authJwt.isAdmin], updatePoliticalParty);
router.get("/:id", [authJwt.verifyToken, authJwt.isAdmin], getOnePoliticalParty); 


module.exports = router;
