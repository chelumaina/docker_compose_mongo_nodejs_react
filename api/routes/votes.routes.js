
const express = require("express"); 
const router = express.Router();
const {authJwt} = require('../middleware'); 

var multer = require("multer");
var bodyParser = require("body-parser");
 
var upload = multer({ dest: 'uploads/' })

// const {getVotes, createVotes, updateVotes,  deleteVotes} = require("../co")
const {getVotes, createVote, updateVote,  deleteVote, getOneVote, getCountyVotes, getConstituencyVotes, getWardVotes, uploadImage} = require('../controllers/votes.controller')

 
router.post("/", upload.single("image"),[authJwt.verifyToken, authJwt.isAdmin], createVote);
router.get("/", [/*authJwt.verifyToken, authJwt.isAdmin*/], getVotes);

router.delete("/:id", [authJwt.verifyToken, authJwt.isAdmin], deleteVote);
router.put("/:id", upload.single("image"),[authJwt.verifyToken, authJwt.isAdmin], updateVote);
router.get("/:id", [/*authJwt.verifyToken, authJwt.isAdmin*/], getOneVote);
router.post("/:id/uploadImage", upload.single("image"), [authJwt.verifyToken, authJwt.isAdmin], uploadImage);

router.get("/county_stations/:county_id",[/*authJwt.verifyToken*/],getCountyVotes);
router.get("/constituency_stations/:constituency_id",[/*authJwt.verifyToken*/],getConstituencyVotes);
router.get("/ward_stations/:ward_id",[/*authJwt.verifyToken*/],getWardVotes);


// router.post("/get", [authJwt.verifyToken], getVotes); 
// router.post("/create", upload.single("image"), [authJwt.verifyToken, authJwt.isAdmin], createVotes); 
// router.post("/update/:id", upload.single("image"), [authJwt.verifyToken, authJwt.isModerator], updateVotes); 
// router.post("/delete/:id", [authJwt.verifyToken, authJwt.isModerator], deleteVotes);  
 

module.exports = router;
