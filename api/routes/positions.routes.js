
const express = require("express"); 
const router = express.Router();
const {authJwt} = require('../middleware'); 

var multer = require("multer"); 
 
var upload = multer({ dest: 'uploads/' })

// const {getVotes, createVotes, updateVotes,  deleteVotes} = require("../co")
const {getPositions, createPositions, updatePositions,  deletePositions, getOnePositions} = require('../controllers/positions.controller')


router.post("/", [authJwt.verifyToken/*, authJwt.isAdmin*/], createPositions); 
router.get("/", [/*authJwt.verifyToken, authJwt.isAdmin*/], getPositions); 

router.delete("/:id", [authJwt.verifyToken/*, authJwt.isAdmin*/], deletePositions); 
router.put("/:id", [authJwt.verifyToken/*, authJwt.isAdmin*/], updatePositions);
router.get("/:id", [/*authJwt.verifyToken, authJwt.isAdmin*/], getOnePositions); 


 
module.exports = router;
