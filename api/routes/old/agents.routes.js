const express = require("express");
const router = express.Router();
const { authJwt } = require("../middleware");

const {
    getAgents,
    getOneAgent,
    getAgentByCounty,
    getAgentByConstituency, 
    getAgentByWard,
    getAgentByStation,
    createAgent,
    updateAgent,
    deleteAgent,
} = require("../controllers/agent.controller");

router.get("/get", /*[authJwt.verifyToken],*/ getAgents);
router.get("/getOneAgent/:id", [authJwt.verifyToken], getOneAgent);
router.get("/getAgentByCounty/:county_id",[authJwt.verifyToken],getAgentByCounty);
router.get("/getAgentByConstituency/:constituency_id",[authJwt.verifyToken],getAgentByConstituency);
router.get("/getAgentByWard/:ward_id", [authJwt.verifyToken], getAgentByWard);
router.get("/getAgentByStation/:station_id",[authJwt.verifyToken],getAgentByStation);

router.post("/create", [authJwt.verifyToken, authJwt.isAdmin], createAgent);
router.post("/update/:id", [authJwt.verifyToken, authJwt.isAdmin], updateAgent);
router.post("/delete/:id", [authJwt.verifyToken, authJwt.isAdmin], deleteAgent);

module.exports = router;
