const express = require("express"); 
const router = express.Router();
const {authJwt} = require('../middleware');

// const {getVotes, createVotes, updateVotes,  deleteVotes} = require("../co")
const {getVotes, createVotes, updateVotes,  deleteVotes} = require('../controllers/votes.controller')

 
router.post("/get", [authJwt.verifyToken, authJwt.isModerator], getVotes); 
router.post("/create", upload.single("image"), [authJwt.verifyToken, authJwt.isModerator], createVotes); 
router.post("/update/:id", upload.single("image"), [authJwt.verifyToken, authJwt.isModerator], updateVotes); 
router.post("/delete/:id", [authJwt.verifyToken, authJwt.isModerator], deleteVotes);  
 

module.exports = router;
