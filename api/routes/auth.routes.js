const express = require("express");

const router = express.Router();
const authController = require("../controllers/auth.controller");
const {authJwt} = require('../middleware');

router.post("/signup", authController.signup);
router.post("/signin", authController.signin);
router.post("/access-token", [authJwt.verifyToken], authController.signin_with_token);

module.exports = router;
