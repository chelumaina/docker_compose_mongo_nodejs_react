const express = require("express"); 
const router = express.Router();
const {authJwt} = require('../middleware');

const {getCandidate,getCandidateByParty, getCandidateByPosistion, createCandidate, updateCandidate,  deleteCandidate, getOneCandidate, getCandidateByCounty, getCandidateByConstituency, getCandidateByWard} = require('../controllers/candidate.controller')
 

router.post("/", [authJwt.verifyToken/*, authJwt.isAdmin*/], createCandidate); 
router.get("/", [authJwt.verifyToken/*, authJwt.isAdmin*/], getCandidate); 

router.delete("/:id", [authJwt.verifyToken/*, authJwt.isAdmin*/], deleteCandidate); 
router.put("/:id", [authJwt.verifyToken/*, authJwt.isAdmin*/], updateCandidate);
router.get("/:id", [authJwt.verifyToken/*, authJwt.isAdmin*/], getOneCandidate); 


router.get("/getCandidateByParty/:party_id", [authJwt.verifyToken], getCandidateByParty); 
router.get("/getCandidateByConstituency/:constituency_id", [authJwt.verifyToken], getCandidateByConstituency); 
router.get("/getCandidateByCounty/:county_id", [authJwt.verifyToken], getCandidateByCounty); 
router.get("/getCandidateByWard/:ward_id", [authJwt.verifyToken], getCandidateByWard); 
router.get("/getCandidateByPosistion/:position_id", [authJwt.verifyToken], getCandidateByPosistion); 
 

module.exports = router;
