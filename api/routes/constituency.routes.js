const express = require("express"); 
const router = express.Router();
const {authJwt} = require('../middleware');

const {
    getConstituencies,
    createConstituency,
    updateConstituency,
    deleteConstituency,
    getOneConstituency,
    getCountyConstituencyById
} = require('../controllers/constituency.controller')
 

router.post("/", [authJwt.verifyToken/*, authJwt.isAdmin*/], createConstituency); 
router.get("/", [/*authJwt.verifyToken, authJwt.isAdmin*/], getConstituencies); 

router.delete("/:id", [authJwt.verifyToken/*, authJwt.isAdmin*/], deleteConstituency); 
router.put("/:id", [authJwt.verifyToken/*, authJwt.isAdmin*/], updateConstituency);
router.get("/:id", [/*authJwt.verifyToken, authJwt.isAdmin*/], getOneConstituency); 
router.get("/get_county_constituencies/:county_id", [/*authJwt.verifyToken*/], getCountyConstituencyById);
 
module.exports = router;
