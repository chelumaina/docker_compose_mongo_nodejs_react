const express = require("express"); 
const router = express.Router();
const {authJwt} = require('../middleware');

const {
    getCounties,
    createCounty,
    updateCounty,
    deleteCounty,
    getOneCounty
} = require('../controllers/county.controller')
 

router.post("/", [authJwt.verifyToken/*, authJwt.isAdmin*/], createCounty); 
router.get("/", [/*authJwt.verifyToken, authJwt.isAdmin*/], getCounties); 

router.delete("/:id", [authJwt.verifyToken/*, authJwt.isAdmin*/], deleteCounty); 
router.put("/:id", [authJwt.verifyToken/*, authJwt.isAdmin*/], updateCounty);
router.get("/:id", [/*authJwt.verifyToken, authJwt.isAdmin*/], getOneCounty); 
 
module.exports = router;
