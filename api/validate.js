const Joi = require('joi'); 


//Register validation

const RegisterValidate = (data) => {

  
  const schema = Joi.object().keys({ 
    name: Joi.string().min(3).max(30).required(),
    email: Joi.string().min(6).required().email(),
    password: Joi.string().min(6).required(), 
    phone: Joi.string().min(6).required(), 
    role: Joi.array().min(1).max(30).required(),
    // user_polling_station :Joi.array().min(1).max(30).required(),
  });  
  return Joi.validate(data, schema);   
};

const LoginValidate = (data) => {
  const schema = {
    email: Joi.string().min(6).required().email(),
    password: Joi.string().min(6).required(),
  };
  return Joi.Validate(data, schema);
};

module.exports.RegisterValidate=RegisterValidate;
module.exports.LoginValidate=LoginValidate;