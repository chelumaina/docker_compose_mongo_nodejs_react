const mongoose = require("mongoose");
const Positions = mongoose.model(
  "Positions",
  new mongoose.Schema({
    name: {required:true, type:String}
  })
);
module.exports = Positions;