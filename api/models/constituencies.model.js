const mongoose = require("mongoose");
const Constituency = mongoose.model(
  "Constituencies",
  new mongoose.Schema({
    constituency_code: {required: true,type: String},
    constituency_name: {required: true,type: String},
    county_id: {
        required: true,
        type: mongoose.Schema.Types.ObjectId,
        ref: "Counties"
      } 
  })
);
module.exports = Constituency;