const mongoose = require("mongoose");
const Wards = mongoose.model(
  "Wards",
  new mongoose.Schema({
    ward_code: {required: true,type: String},
    ward_name: {required: true,type: String},
    county_id: {
        required: true,
        type: mongoose.Schema.Types.ObjectId,
        ref: "Counties"
      } ,
    constituency_id: {
        required: true,
        type: mongoose.Schema.Types.ObjectId,
        ref: "Constituencies"
      } 
  })
);
module.exports = Wards;