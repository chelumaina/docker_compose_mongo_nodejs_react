const mongoose = require("mongoose");
const User = mongoose.model(
  "Users",
  new mongoose.Schema({
    name: {required: true,type: String},
    email: {required: true,type: String},
    password: {required: true,type: String},
    phone: {required: true,type: String},
    data: {required: true,type: Object},
    role: [{required: true,type: mongoose.Schema.Types.ObjectId,ref: "Roles"}],
    polling_station_id: [{required: true,type: mongoose.Schema.Types.ObjectId,ref: "PollingStations"}],
    ward_id: [{required: true,type: mongoose.Schema.Types.ObjectId,ref: "Wards"}],
    constituency_id: [{required: true,type: mongoose.Schema.Types.ObjectId,ref: "Constituencies"}],
    county_id: [{required: true,type: mongoose.Schema.Types.ObjectId,ref: "Counties"}],
    img: {data: Buffer, contentType: String}
  })
);
module.exports = User;