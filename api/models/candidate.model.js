const mongoose = require("mongoose");
const Candidates = mongoose.model(
  "Candidates",
  new mongoose.Schema({
    candidate_name: {required: true,type: String},

    county_id: {
      // required: true,
      type: mongoose.Schema.Types.ObjectId,
      ref: "Counties",
    },
    constituency_id: {
      // required: true,
      type: mongoose.Schema.Types.ObjectId,
      ref: "Constituencies",
    },
    ward_id: {
      // required: true,
      type: mongoose.Schema.Types.ObjectId,
      ref: "Wards",
    },
    polling_station_id: {
      // required: true,
      type: mongoose.Schema.Types.ObjectId,
      ref: "PollingStations",
    },
    position_id: {
      required: true,
      type: mongoose.Schema.Types.ObjectId,
      ref: "Positions",
    },
    party_id: {
      required: true,
      type: mongoose.Schema.Types.ObjectId,
      ref: "PoliticalParty",
    },

    img: { data: Buffer, contentType: String }
  })
);
module.exports = Candidates;

 