const { sequelize, Sequelize } = require("sequelize");

module.exports = (sequelize, Sequelize) => {
  const Station = sequelize.define("stations", { 
  COUNTY_CODE: { type: Sequelize.STRING },
  COUNTY_NAME: { type: Sequelize.STRING },	
  CONSTITUENCY_CODE: { type: Sequelize.STRING },	
  CONSTITUENCYNAME: { type: Sequelize.STRING },	
  CAW_CODE: { type: Sequelize.STRING },	
  CAW_NAME: { type: Sequelize.STRING },	
  REGISTRATION_CENTRE_CODE: { type: Sequelize.STRING },	
  REGISTRATION_CENTRE_NAME: { type: Sequelize.STRING },	
  VOTERS_PER_REGISTRATION_CENTRE: { type: Sequelize.STRING },	
  POLLING_STATION_CODE: { type: Sequelize.STRING },
  POLLING_STATION_NAME: { type: Sequelize.STRING },	
  VOTERS_PER_POLLING_STATION: { type: Sequelize.STRING },
});
 
  return Station;
};
