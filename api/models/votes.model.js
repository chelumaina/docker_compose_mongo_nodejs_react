const { Int32 } = require("mongodb");
const mongoose = require("mongoose");

const userSchema = new mongoose.Schema({
  form_name: { required: true, type: String },
  vote_cast: { required: false, type: String },
  spoiled_votes: { required: false, type: String },
  polling_station_id: {required: true,type: mongoose.Schema.Types.ObjectId,ref: "PollingStations",},
  position_id: {required: true,type: mongoose.Schema.Types.ObjectId,ref: "Positions",},
  results: { required: false, type: Object },
  img: { data: Buffer, contentType: String },
});

module.exports = mongoose.model("Votes", userSchema);
