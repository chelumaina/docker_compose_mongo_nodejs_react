const mongoose = require('mongoose');
mongoose.Promise = global.Promise;
const db = {};
db.mongoose = mongoose;

db.user = require("./user.model");
db.role = require("./role.model");
db.candidate = require("./candidate.model");
db.vote = require("./votes.model");
db.pollingStation = require("./pollingStation.model");
db.politicalParty = require("./politicalParty.model");


// db.stations = require("./station.model");
db.county = require("./counties.model");
db.constituency = require("./constituencies.model");
db.ward = require("./wards.model");
db.position = require("./position.model");



db.ROLES = ["user", "admin", "moderator"];
module.exports = db;

