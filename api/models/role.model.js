const mongoose = require("mongoose");
const Role = mongoose.model(
  "Roles",
  new mongoose.Schema({
    name: {required:true, type:String}
  })
);
module.exports = Role;