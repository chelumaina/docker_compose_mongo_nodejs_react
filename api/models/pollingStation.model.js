const mongoose = require("mongoose");
const PollingStations = mongoose.model(
  "PollingStations",
  new mongoose.Schema({
    polling_station_code: {required: true,type: String},
    polling_station_name: {required: true,type: String},
    polling_station_registered_voters: Number,
    county_id: {
        required: true,
        type: mongoose.Schema.Types.ObjectId,
        ref: "Counties"
      } ,
    constituency_id: {
        required: true,
        type: mongoose.Schema.Types.ObjectId,
        ref: "Constituencies"
      } ,
    ward_id: {
        required: true,
        type: mongoose.Schema.Types.ObjectId,
        ref: "Wards"
      } 
  })
);
module.exports = PollingStations;