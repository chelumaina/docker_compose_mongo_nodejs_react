const mongoose = require("mongoose");
const Counties = mongoose.model(
  "Counties",
  new mongoose.Schema({
    county_code: {required: true,type: String},
    county_name: {required: true,type: String} 
  })
);
module.exports = Counties;