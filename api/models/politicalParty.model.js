const mongoose = require("mongoose");
const PoliticalParty = mongoose.model(
  "PoliticalParty",
  new mongoose.Schema({
    party_name: {required:true, type:String},
    color: {required:true, type:String}, 
    logo: { data: Buffer, contentType: String }
  })
);
module.exports = PoliticalParty;